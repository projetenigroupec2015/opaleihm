'use strict';

// Karma configuration
module.exports = function (config) {
	config.set({
		// Frameworks to use
		frameworks: ['jasmine'],

		// List of files / patterns to load in the browser
		files: [
			'src/bower_components/jquery/dist/jquery.js',
			'src/bower_components/es5-shim/es5-shim.js',
			'src/bower_components/json3/lib/json3.min.js',
			'src/bower_components/angular/angular.js',
			'src/bower_components/angular-aria/angular-aria.js',
			'src/bower_components/angular-resource/angular-resource.js',
			'src/bower_components/angular-mocks/angular-mocks.js',
			'src/bower_components/angular-cookies/angular-cookies.js',
			'src/bower_components/angular-sanitize/angular-sanitize.js',
			'src/bower_components/angular-animate/angular-animate.js',
			'src/bower_components/angular-touch/angular-touch.js',
			'src/bower_components/angular-ui-router/release/angular-ui-router.js',
			'src/bower_components/angular-aria/angular-aria.js',
			'src/bower_components/angular-material/angular-material.js',
			'src/bower_components/angular-messages/angular-messages.js',
			'src/bower_components/angular-material-icons/angular-material-icons.js',
			'app/app.js',
			'app/modules/home/homeModule.js',
			'app/modules/home/homeCtrl.js',
			'app/modules/home/homeRoute.js',
			'app/modules/home/homeService.js',
			'app/modules/home/home-test.js',
			'app/modules/config/configModule.js',
			'app/modules/config/configCtrl.js',
			'app/modules/config/configRoute.js',
			'app/modules/config/configService.js',
			'app/modules/config/config-test.js',
			'app/modules/timesheet/timesheetModule.js',
			'app/modules/timesheet/timesheetCtrl.js',
			'app/modules/timesheet/timesheetRoute.js',
			'app/modules/timesheet/timesheetService.js',
			'app/modules/timesheet/timesheet-test.js',
			'app/modules/vacationrequest/vacationrequestModule.js',
			'app/modules/vacationrequest/vacationrequestCtrl.js',
			'app/modules/vacationrequest/vacationrequestRoute.js',
			'app/modules/vacationrequest/vacationrequestService.js',
			'app/modules/vacationrequest/vacationrequest-test.js',
			'app/modules/notifications/notificationsModule.js',
			'app/modules/notifications/notificationsCtrl.js',
			'app/modules/notifications/notificationsRoute.js',
			'app/modules/notifications/notificationsService.js',
			'app/modules/notifications/notifications-test.js',
			'app/modules/settings/settingsModule.js',
			'app/modules/settings/settingsCtrl.js',
			'app/modules/settings/settingsRoute.js',
			'app/modules/settings/settingsService.js',
			'app/modules/settings/settings-test.js',
			'app/modules/planning/planningModule.js',
			'app/modules/planning/planningCtrl.js',
			'app/modules/planning/planningRoute.js',
			'app/modules/planning/planningService.js',
			'app/modules/planning/planning-test.js',
			'app/modules/user/userModule.js',
			'app/modules/user/userCtrl.js',
			'app/modules/user/userRoute.js',
			'app/modules/user/userService.js',
			'app/modules/user/user-test.js',
			'app/modules/login/loginModule.js',
			'app/modules/login/loginCtrl.js',
			'app/modules/login/loginRoute.js',
			'app/modules/login/loginService.js',
			'app/modules/login/login-test.js',
		],

		// Test results reporter to use
		// Possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
		//reporters: ['progress'],
		reporters: ['spec'],

		plugins : [
			'karma-jasmine',
			'karma-coverage',
			'karma-chrome-launcher',
			'karma-firefox-launcher',
			'karma-phantomjs-launcher',
			'karma-spec-reporter'
		],

		// Web server port
		port: 9876,

		// Enable / disable colors in the output (reporters and logs)
		colors: true,

		// Level of logging
		// Possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_INFO,

		// Enable / disable watching file and executing tests whenever any file changes
		autoWatch: true,

		// Start these browsers, currently available:
		// - Chrome
		// - ChromeCanary
		// - Firefox
		// - Opera
		// - Safari (only Mac)
		// - PhantomJS
		// - IE (only Windows)
		browsers: ['PhantomJS'],

		// If browser does not capture in given timeout [ms], kill it
		captureTimeout: 60000,

		// Continuous Integration mode
		// If true, it capture browsers, run tests and exit
		singleRun: true
	});
};
