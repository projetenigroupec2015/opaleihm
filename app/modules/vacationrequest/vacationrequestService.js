(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:vacationrequestService
	* @description
	* # vacationrequestService
	* Service of the app
	*/

	angular
	.module('vacationrequest')
	.factory('VacationrequestService', Vacationrequest);
	// Inject your dependencies as .$inject = ['$http', 'someSevide'];
	// function Name ($http, someSevide) {...}

	Vacationrequest.$inject = ['$http', 'config', 'Dao'];

	function Vacationrequest ($http, config, Dao) {

		return {
			getTimeOff: getTimeOff,
			getJSONTimeOff: getJSONTimeOff,
			createTimeOff: createTimeOff,
			updateTimeOff: updateTimeOff
		};

		function getTimeOff (dateDebut, dateFin, formateurId, etat) {
			var url = config.apiUrl + "/conges/" + dateDebut + "/" + dateFin;
			if (formateurId === null) {
				if (etat === null) {
					return Dao.crudData('get', url);
				} else {
					return $http.get(url, {cache: true, params:{'etat': etat}})
						.then(Dao.success)
						.catch(Dao.error);
				}
			} else {
				if (etat === null) {
					return $http.get(url, {cache: true, params:{'formateurId': formateurId}})
						.then(Dao.success)
						.catch(Dao.error);
				} else {
					return $http.get(url, {cache: true, params:{'formateurId': formateurId, 'etat': etat}})
						.then(Dao.success)
						.catch(Dao.error);
				}
			}
		}
		function getJSONTimeOff (
			idConge,
			dateDebutConge,
			dateEndConge,
			motif,
			etatConge,
			dateDemandeConge,
			formateur
		) {
			return {
				idConge: idConge == null ? 0:idConge,
				dateDebutConge: dateDebutConge,
				dateFinConge: dateEndConge,
				motif: motif,
				etatConge: etatConge,
				dateDemandeConge: dateDemandeConge,
				formateur:
				{
					idFormateur: formateur.idFormateur,
					username: formateur.username,
					mail:formateur.mail,
					prenom:formateur.prenom,
					nom:formateur.nom,
					externe: 0,
					competences:null,
					habilitation: null,
					experiences: null
				}
			};
		}
		function createTimeOff (conge) {
			return Dao.crudData(
				'post',
				config.apiUrl + "/conge",
				conge
			);
		}

		function updateTimeOff (conge) {
			return Dao.crudData(
				'put',
				config.apiUrl + "/conge/" + conge.idConge,
				conge
			);
		}
	}

})();
