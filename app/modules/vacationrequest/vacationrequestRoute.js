'use strict';

/**
* @ngdoc function
* @name app.route:vacationrequestRoute
* @description
* # vacationrequestRoute
* Route of the app
*/

angular.module('vacationrequest')
.config(['$stateProvider', function ($stateProvider) {

	$stateProvider
	.state('home.vacationrequest', {
		url:'/vacationrequest',
		templateUrl: 'app/modules/vacationrequest/vacationrequest.html',
		controller: 'VacationrequestCtrl',
		controllerAs: 'vm'
	});


}]);
