(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:vacationrequestCtrl
	 * @description
	 * # vacationrequestCtrl
	 * Controller of the app
	 */

	angular
		.module('vacationrequest')
		.controller('AddDialogController', AddDialogController)
		.controller('ValidationDialogController', ValidationDialogController)
		.controller('VacationrequestCtrl', Vacationrequest);

	Vacationrequest.$inject = ['VacationrequestService', 'homeService', 'Utils', '_', '$mdDialog', '$scope', '$rootScope'];
	AddDialogController.$inject = ['VacationrequestService', 'homeService', 'Utils', '$mdDialog', '$scope'];
	ValidationDialogController.$inject = ['VacationrequestService', 'homeService', 'Utils', '$mdDialog', '$scope'];

	/*
	 * recommend
	 * Using function declarations
	 * and bindable members up top.
	 */

	function Vacationrequest(VacationrequestService, homeService, Utils, _, $mdDialog, $scope, $rootScope) {
		/*jshint validthis: true */
		var vm = this;

		vm.user = $rootScope.account;

		vm.query = {
			order: 'dateDebutConge',
			reverse: false,
			limit: 10,
			page: 1
		};

		vm.etatEnum = {
			ALL: {intituleUrl: 'all', description: 'tout', etatConge: false, valeurEnum: 'ALL'},
			ACTIF: {intituleUrl: 'actif', description: 'actif', etatConge: false, valeurEnum: 'ACTIF'},
			VALIDE: {intituleUrl: 'valide', description: 'Validé', etatConge: true, valeurEnum: 'VALIDE'},
			EN_ATTENTE: {intituleUrl: 'attente', description: 'En attente', etatConge: true, valeurEnum: 'EN_ATTENTE'},
			REFUSE: {intituleUrl: 'refuse', description: 'Refusé', etatConge: true, valeurEnum: 'REFUSE'}
		};
		vm.getDescriptionEtat = function (etat) {
			switch (etat) {
				case vm.etatEnum.ALL.valeurEnum:
					return vm.etatEnum.ALL.description;
				case vm.etatEnum.ACTIF.valeurEnum:
					return vm.etatEnum.ACTIF.description;
				case vm.etatEnum.VALIDE.valeurEnum:
					return vm.etatEnum.VALIDE.description;
				case vm.etatEnum.EN_ATTENTE.valeurEnum:
					return vm.etatEnum.EN_ATTENTE.description;
				case vm.etatEnum.REFUSE.valeurEnum:
					return vm.etatEnum.REFUSE.description;
				default:
					return 'Etat inconnu'
			}
		};


		vm.dateDuJour = new Date();
		vm.filtreDateDebut = new Date((new Date()).setHours(0, 0, 0));
		vm.filtreDateFin = new Date(new Date((new Date()).setYear((new Date()).getFullYear() + 1)).setHours(23, 59, 59));
		vm.getFiltreDateDebut = function () {
			return vm.filtreDateDebut;
		};
		vm.getFiltreDateFin = function () {
			return vm.filtreDateFin;
		};

		vm.refreshHolidays = function () {
			if (vm.user.isAdmin) {
				VacationrequestService.getTimeOff(
					vm.getFiltreDateDebut().toISOString(),
					vm.getFiltreDateFin().toISOString(),
					null,
					null
				).then(function (response) {
					if (response.status == 200) {
						vm.holidays = response.data;
					} else {
						vm.holidays = [];
						if (response.status == 409) {
							Utils.viewToast("warn", response.data);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				}).catch(function (error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			} else {
				VacationrequestService.getTimeOff(
					vm.getFiltreDateDebut().toISOString(),
					vm.getFiltreDateFin().toISOString(),
					$rootScope.account.id,
					null
				).then(function (response) {
					if (response.status == 200) {
						vm.holidays = response.data;
					} else {
						vm.holidays = [];
						if (response.status == 409) {
							Utils.viewToast("warn", response.data);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				}).catch(function (error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			}
		};
		vm.refreshHolidays();


		vm.getHolidays = function () {
			$scope.promise = vm.holidays.get($scope.query, success).$promise;
		};

		vm.hasHolidays = function () {
			return vm.holidays.length > 0;
		};

		function success(holidays) {
			vm.holidays = holidays;
		}

		vm.newTimeOff = {
			dateStart: new Date(),
			hourStart: 9,
			minuteStart: 30,
			getDateTimeStart: function () {
				if (vm.newTimeOff.hasOwnProperty('dateStart')
					&& vm.newTimeOff.dateStart != null
					&& vm.newTimeOff.dateStart != ''
				) {
					if (vm.newTimeOff.hasOwnProperty('hourStart')
						&& vm.newTimeOff.hasOwnProperty('minuteStart')
						&& Number.isInteger(vm.newTimeOff.hourStart)
						&& Number.isInteger(vm.newTimeOff.hourStart) > -1
						&& Number.isInteger(vm.newTimeOff.hourStart) < 24
						&& Number.isInteger(vm.newTimeOff.minuteStart)
						&& Number.isInteger(vm.newTimeOff.minuteStart) > -1
						&& Number.isInteger(vm.newTimeOff.minuteStart) < 60
					) {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateStart).year(),
								moment(vm.newTimeOff.dateStart).month(),
								moment(vm.newTimeOff.dateStart).date(),
								vm.newTimeOff.hourStart,
								vm.newTimeOff.minuteStart,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					} else {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateStart).year(),
								moment(vm.newTimeOff.dateStart).month(),
								moment(vm.newTimeOff.dateStart).date(),
								0,
								0,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					}
				} else {
					return null;
				}
			},
			dateEnd: new Date(),
			hourEnd: 17,
			minuteEnd: 30,
			getDateTimeEnd: function () {
				if (vm.newTimeOff.hasOwnProperty('dateEnd')
					&& vm.newTimeOff.dateEnd != null
					&& vm.newTimeOff.dateEnd != '') {
					if (vm.newTimeOff.hasOwnProperty('hourEnd')
						&& vm.newTimeOff.hasOwnProperty('minuteEnd')
						&& Number.isInteger(vm.newTimeOff.hourEnd)
						&& Number.isInteger(vm.newTimeOff.hourEnd) > -1
						&& Number.isInteger(vm.newTimeOff.hourEnd) < 24
						&& Number.isInteger(vm.newTimeOff.minuteEnd)
						&& Number.isInteger(vm.newTimeOff.minuteEnd) > -1
						&& Number.isInteger(vm.newTimeOff.minuteEnd) < 60
					) {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateEnd).year(),
								moment(vm.newTimeOff.dateEnd).month(),
								moment(vm.newTimeOff.dateEnd).date(),
								vm.newTimeOff.hourEnd,
								vm.newTimeOff.minuteEnd,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					} else {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateEnd).year(),
								moment(vm.newTimeOff.dateEnd).month(),
								moment(vm.newTimeOff.dateEnd).date(),
								0,
								0,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					}
				} else {
					return null;
				}
			},
			motif: '',
			formateur: {
				idFormateur: $rootScope.account.id,
				nom: $rootScope.account.lastName,
				prenom: $rootScope.account.firstName,
				mail: $rootScope.account.email,
				username: $rootScope.account.login
			}
		};

		vm.resetNewTimeOff = function () {
			vm.newTimeOff.motif = '';
			vm.newTimeOff.dateStart = new Date();
			vm.newTimeOff.dateEnd = new Date();
			vm.newTimeOff.hourStart = 9;
			vm.newTimeOff.minuteStart = 17;
			vm.newTimeOff.hourEnd = 17;
			vm.newTimeOff.minuteEnd = 17;
		};

		// affichage de l'écran de dialogue d'ajout d'une demande de congé
		vm.showAddVacationrequestDialog = function (event) {
			$mdDialog.show({
				clickOutsideToClose: true,
				escapeToClose: true,
				preserveScope: true,
				scope: $scope,
				templateUrl: 'app/modules/vacationrequest/addVacationrequestDialog.html',
				controller: AddDialogController(VacationrequestService, homeService, Utils, $mdDialog, $scope, vm)
			});
		};

		vm.showValidationVacationrequestDialog = function (conge, event) {
			vm.inValidationConge = conge;
			$mdDialog.show({
				clickOutsideToClose: true,
				escapeToClose: true,
				preserveScope: true,
				scope: $scope,
				templateUrl: 'app/modules/vacationrequest/validationVacationrequestDialog.html',
				controller: ValidationDialogController(VacationrequestService, homeService, Utils, $mdDialog, $scope, vm)
			});
		};

		vm.searchText = '';
		vm.filterByFormateur = function (holiday) {
			if (holiday != null && holiday.formateur != null) {
				return ((holiday.formateur.nom.toLowerCase().indexOf(vm.searchText.toLowerCase()) !== -1)
				|| (holiday.formateur.prenom.toLowerCase().indexOf(vm.searchText.toLowerCase()) !== -1));
			} else {
				return false;
			}
		};

		vm.getCongesFiltres = function () {
			if (vm.user.isAdmin) {
				var conges = [];
				_.forEach(vm.holidays, function (value) {
					if (vm.filterByFormateur(value)) {
						conges.push(value);
					}
				});
				return conges;
			} else {
				return vm.holidays;
			}
		}
	}

	function getNotifications(homeService, Utils, $scope) {
		homeService.getNotifications()
			.then(function (response) {
				if (response.status === 200) {
					$scope.$emit("notificationsUpdate", {notifs: response.data, nbNotif:response.data.length});
				} else {
					if (response.err === -1) {
						Utils.viewToast("error", response.data);
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				}
			});
	}

	function AddDialogController(VacationrequestService, homeService, Utils, $mdDialog, $scope, vm) {
		$scope.closeDialog = function () {
			$mdDialog.hide();
		};

		$scope.cancelDialog = function () {
			$mdDialog.cancel();
		};

		$scope.validDialog = function () {
			if ((moment(vm.newTimeOff.getDateTimeStart()).diff(vm.newTimeOff.getDateTimeEnd())) < 0) {
				if (vm.newTimeOff.formateur != null) {
					VacationrequestService.createTimeOff(
						VacationrequestService.getJSONTimeOff(
							null,
							vm.newTimeOff.getDateTimeStart(),
							vm.newTimeOff.getDateTimeEnd(),
							vm.newTimeOff.motif,
							null,
							null,
							vm.newTimeOff.formateur
						)
					).then(function (response) {
						if (response.status == 200 || response.status == 201) {
							Utils.viewToast("success", "Succès : création du congé réussie!");
							getNotifications(homeService, Utils, $scope);
							vm.refreshHolidays();
							vm.resetNewTimeOff();
						} else {
							if (response.status == 409) {
								Utils.viewToast("warn", response.data);
							} else {
								Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
							}
						}
					}).catch(function (error) {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					});
				} else {
					Utils.viewToast("error", "Erreur dans la récupération du formateur sélectionné.");
				}
			} else {
				Utils.viewToast("warn", "La date de début doit précéder la date de fin de congé.");
			}

			// vm.holidays.push(conge);
			$scope.closeDialog();
		};
	}

	function ValidationDialogController(VacationrequestService, homeService, Utils, $mdDialog, $scope, vm) {
		$scope.closeDialog = function () {
			$mdDialog.hide();
		};

		$scope.refuseCongeDialog = function () {
			$scope.validation(vm.etatEnum.REFUSE.valeurEnum);
			$scope.closeDialog();
		};

		$scope.validCongeDialog = function () {
			$scope.validation(vm.etatEnum.VALIDE.valeurEnum);
			$scope.closeDialog();
		};

		$scope.validation = function (newEtat) {
			if ((moment(vm.inValidationConge.dateDebutConge).diff(moment(vm.inValidationConge.dateFinConge)) < 0)) {
				VacationrequestService.updateTimeOff(
					VacationrequestService.getJSONTimeOff(
						vm.inValidationConge.idConge,
						vm.inValidationConge.dateDebutConge,
						vm.inValidationConge.dateFinConge,
						vm.inValidationConge.motif,
						newEtat,
						vm.inValidationConge.dateDemandeConge,
						vm.inValidationConge.formateur
					)).then(function (response) {
					if (response.status == 200 || response.status == 201) {
						Utils.viewToast("success", "Succès : traitement de la demande de congé réussi!");
						getNotifications(homeService, Utils, $scope);
						vm.refreshHolidays();
					} else {
						if (response.status == 409) {
							Utils.viewToast("warn", response.data);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				}).catch(function (error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			} else {
				Utils.viewToast("warn", "La date de début doit précéder la date de fin de congé.");
			}

		};
	}
})();
