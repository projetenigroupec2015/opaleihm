(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:vacationrequestModule
   * @description
   * # vacationrequestModule
   * Module of the app
   */

   angular.module('vacationrequest', [])
	   .config(function($mdDateLocaleProvider) {

		   // Example of a French localization.
		   $mdDateLocaleProvider.months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
		   $mdDateLocaleProvider.shortMonths = ['janv', 'févr', 'mars', 'avr', 'mai', 'juin', 'juil', 'août', 'sept', 'oct', 'nov', 'déc'];
		   $mdDateLocaleProvider.days = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
		   $mdDateLocaleProvider.shortDays = ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'];

		   // Can change week display to start on Monday.
		   $mdDateLocaleProvider.firstDayOfWeek = 1;

		   $mdDateLocaleProvider.msgCalendar = 'Calendrier';
		   $mdDateLocaleProvider.msgOpenCalendar = 'Ouvrir le calendrier';

		   // Optional.
		   // $mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6];

		   // Example uses moment.js to parse and format dates.
		   $mdDateLocaleProvider.parseDate = function(dateString) {
			   var m = moment(dateString, 'L', true);
			   m.tz('Europe/Paris');
			   return m.isValid() ? m.toDate() : new Date(NaN);
		   };

		   $mdDateLocaleProvider.formatDate = function(date) {
			   var m = moment(date);
			   return m.isValid() ? m.format('DD-MM-YYYY') : '';
		   };

		   // $mdDateLocaleProvider.monthHeaderFormatter = function(date) {
			//    return myShortMonths[date.getMonth()] + ' ' + date.getFullYear();
		   // };

		   // In addition to date display, date components also need localized messages
		   // for aria-labels for screen-reader users.

		   $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
			   return 'Semaine ' + weekNumber;
		   };

		   // // You can also set when your calendar begins and ends.
		   // $mdDateLocaleProvider.firstRenderableDate = new Date(1776, 6, 4);
		   // $mdDateLocaleProvider.lastRenderableDate = new Date(2030, 11, 21);
	   });
})();
