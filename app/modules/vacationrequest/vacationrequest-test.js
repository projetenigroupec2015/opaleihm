(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:vacationrequestTest
	 * @description
	 * # vacationrequestTest
	 * Test of the app
	 */

	describe('vacationrequest test', function () {
		var controller = null, $scope = null;

		beforeEach(function () {
			module('opale');
		});

		beforeEach(inject(function ($controller, $rootScope) {
			$scope = $rootScope.$new();
			controller = $controller('VacationrequestCtrl', {
				$scope: $scope
			});
		}));

		it('Should controller must be defined', function () {
			expect(controller).toBeDefined();
		});

	});
})();
