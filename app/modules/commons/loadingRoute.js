'use strict';

	/**
	* @ngdoc function
	* @name app.route:HomeRoute
	* @description
	* # HomeRoute
	* Route of the app
	*/

angular.module('loading')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {
		$stateProvider
		.state('loading', {
			url:'/loading',
			templateUrl: 'app/modules/commons/loading.html',
			access : {
				loginRequired: false,
				authorizedRoles: [USER_ROLES.all]
			}
		});
	}]);
