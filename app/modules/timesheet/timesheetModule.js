(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:timesheetModule
   * @description
   * # timesheetModule
   * Module of the app
   */

   angular.module('timesheet', []);

})();
