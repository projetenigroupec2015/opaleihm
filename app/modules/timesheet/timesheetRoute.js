'use strict';

/**
* @ngdoc function
* @name app.route:timesheetRoute
* @description
* # timesheetRoute
* Route of the app
*/

angular.module('timesheet')
.config(['$stateProvider', function ($stateProvider) {

	$stateProvider
	.state('home.timesheet', {
		url:'/timesheet',
		templateUrl: 'app/modules/timesheet/timesheet.html',
		controller: 'TimesheetCtrl',
		controllerAs: 'vm'
	});


}]);
