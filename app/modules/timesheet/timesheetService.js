(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:timesheetService
	* @description
	* # timesheetService
	* Service of the app
	*/

	angular
	.module('timesheet')
	.factory('TimesheetService', Timesheet);
	// Inject your dependencies as .$inject = ['$http', 'someSevide'];
	// function Name ($http, someSevide) {...}

	Timesheet.$inject = ['$http', '_', 'config', 'Dao'];

	function Timesheet ($http, _, config, Dao) {
		var vm = this;

		vm.getJsonActivites = function (activites) {
			var activitesJson = {};
			_.forEach(activites, function (value) {
				var activite = {
					idActivite: value.idActivite == null ? 0 : value.idActivite,
					dateActivite: value.dateActivite,
					periodeActivite: value.periodeActivite,
					typeActivite: value.typeActivite,
					ficheTemps: value.ficheTemps
				};
				activitesJson.push(activite);
			});
		};

		return {
			getTimesheet: getTimesheet,
			getJSONTimesheet: getJSONTimesheet,
			createTimesheet: createTimesheet,
			updateTimesheet: updateTimesheet
		};

		function getTimesheet (dateDebut, dateFin, formateurId, etat) {
			var url = config.apiUrl + "/ficheTemps/" + dateDebut + "/" + dateFin;
			if (formateurId === null) {
				return Dao.crudData('get', url);
			} else {
				return $http.get(url, {cache: true, params:{'formateurId': formateurId}})
					.then(Dao.success)
					.catch(Dao.error);
			}
		}

		function getJSONTimesheet (
			idFicheTemps,
			moisFicheTemps,
			anneeFicheTemps,
			valide,
			archive,
			formateur,
			activites
		) {
			return {
				idFicheTemps: idFicheTemps == null ? 0:idFicheTemps,
				moisFicheTemps: moisFicheTemps,
				anneeFicheTemps: anneeFicheTemps,
				valide: valide,
				archive: archive,
				formateur:
				{
					idFormateur: formateur.formateurId,
					username: formateur.username,
					mail:formateur.mail,
					prenom:formateur.prenom,
					nom:formateur.nom,
					externe: 0,
					competences:null,
					habilitation: null,
					experiences: null
				},
				activites: vm.getJsonActivites(activites)
			};
		}

		function createTimesheet (timesheet) {
			return Dao.crudData(
				'post',
				config.apiUrl + "/ficheTemps",
				timesheet
			);
		}

		function updateTimesheet (timesheet) {
			return Dao.crudData(
				'put',
				config.apiUrl + "/ficheTemps/" + conge.idConge,
				timesheet
			);
		}
	}
})();
