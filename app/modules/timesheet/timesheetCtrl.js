(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:timesheetCtrl
	* @description
	* # timesheetCtrl
	* Controller of the app
	*/

	angular
	.module('timesheet')
	.controller('TimesheetCtrl', Timesheet);

	Timesheet.$inject = ['TimesheetService', 'Utils', '_', '$rootScope'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Timesheet(TimesheetService, Utils, _, $rootScope) {
		/*jshint validthis: true */
		var vm = this;

		vm.role = $rootScope.account.userRoles;

		vm.isAdmin = function () {
			return (vm.role.libelle === 'admin');
		};

		vm.query = {
			order: 'dateDebutConge',
			reverse: false,
			limit: 10,
			page: 1
		};

		vm.periodeActiviteEnum = {
			MATIN: {description: 'Matin', valeurEnum:'MATIN'},
			APRES_MIDI: {description: 'Après-midi', valeurEnum:'APRES_MIDI'}
		};

		vm.refreshTimesheet = function () {
			if (vm.isAdmin()) {
				TimesheetService.getTimesheet(

				).then(function(response) {
					if (response.status == 200) {
						vm.timesheets = response.data;
					} else {
						vm.timesheets = [];
						if (response.status == 409) {
							Utils.viewToast("warn", response.data);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				}).catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			} else {
				TimesheetService.getTimesheet(

					$rootScope.account.id
				).then(function(response) {
					if (response.status == 200) {
						vm.timesheets = response.data;
					} else {
						vm.timesheets = [];
						if (response.status == 409) {
							Utils.viewToast("warn", response.data);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				}).catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			}
		};
		vm.refreshTimesheet();

		vm.searchText = '';
		vm.filterByFormateur = function(timesheet) {
			if (timesheet != null && timesheet.formateur != null) {
				return ((timesheet.formateur.nom.toLowerCase().indexOf(vm.searchText.toLowerCase()) !== -1)
				|| (timesheet.formateur.prenom.toLowerCase().indexOf(vm.searchText.toLowerCase()) !== -1));
			} else {
				return false;
			}
		};

		vm.getFicheTempsFiltres = function() {
			if (vm.isAdmin()) {
				var timesheets = [];
				_.forEach(vm.timesheets, function (value) {
					if (vm.filterByFormateur(value)) {
						timesheets.push(value);
					}
				});
				return timesheets;
			} else {
				return vm.timesheets;
			}
		}
	}

})();
