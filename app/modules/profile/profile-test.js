(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:notificationsTest
	 * @description
	 * # notificationsTest
	 * Test of the app
	 */

	describe('notifications test', function () {
		var controller = null, $scope = null;

		beforeEach(function () {
			module('opale');
		});

		beforeEach(inject(function ($controller, $rootScope) {
			$scope = $rootScope.$new();
			controller = $controller('NotificationsCtrl', {
				$scope: $scope
			});
		}));

		it('Should controller must be defined', function () {
			expect(controller).toBeDefined();
		});

	});
})();
