(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:notificationsModule
   * @description
   * # notificationsModule
   * Module of the app
   */

   angular.module('profile', []);

})();
