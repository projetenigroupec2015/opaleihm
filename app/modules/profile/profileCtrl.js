(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:notificationsCtrl
	* @description
	* # notificationsCtrl
	* Controller of the app
	*/

	angular
	.module('profile')
	.controller('ProfileCtrl', Profile);

	Profile.$inject = ['$rootScope'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Profile($rootScope) {
		var vm = this;
		vm.user = $rootScope.account;
		vm.getHumanRole = function (habil) {
			switch (habil) {
				case "admin":
				return "administrateur";
				case "resp_form":
				return "reponsable formation";
				case "form":
				return "formateur";
			}
		};

	}

})();
