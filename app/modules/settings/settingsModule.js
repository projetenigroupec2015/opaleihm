(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:settingsModule
   * @description
   * # settingsModule
   * Module of the app
   */

   angular.module('settings', []);

})();
