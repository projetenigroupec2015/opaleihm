(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:settingsTest
	 * @description
	 * # settingsTest
	 * Test of the app
	 */

	describe('settings test', function () {
		var controller = null, $scope = null;

		beforeEach(function () {
			module('opale');
		});

		beforeEach(inject(function ($controller, $rootScope) {
			$scope = $rootScope.$new();
			controller = $controller('SettingsCtrl', {
				$scope: $scope
			});
		}));

		it('Should controller must be defined', function () {
			expect(controller).toBeDefined();
		});

	});
})();
