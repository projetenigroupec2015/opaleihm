(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('SettingsCtrl', Settings);

	Settings.$inject = ['SettingsService', '$mdSidenav', 'Utils'];

	function Settings(SettingsService, $mdSidenav, Utils) {
		/*jshint validthis: true */
		var vm = this;

		vm.init = {
			pageTitle: "Navigation",
		};

		vm.toggleSidenav = function(){
			$mdSidenav('left').toggle();
		};

		vm.isOpenLeft = function(){
			return $mdSidenav('left').isOpen();
		};

	}

})();
