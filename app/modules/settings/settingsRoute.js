'use strict';

/**
 * @ngdoc function
 * @name app.route:settingsRoute
 * @description
 * # settingsRoute
 * Route of the app
 */

angular.module('settings')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {

		$stateProvider
			.state('home.settings', {
				url:'/settings',
				templateUrl: 'app/modules/settings/settings.html',
				controller: "SettingsCtrl",
				controllerAs: "vm",
				ncyBreadcrumb: {
					label: 'Paramétrage'
				},
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.alertes', {
				url:'/alertes',
				templateUrl: 'app/modules/settings/partial/alertes.part.html',
				ncyBreadcrumb: {
			    label: 'Alertes'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.params', {
				url:'/params',
				templateUrl: 'app/modules/settings/partial/params.part.html',
				ncyBreadcrumb: {
			    label: 'Paramètres'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.habilitations', {
				url:'/habilitations',
				templateUrl: 'app/modules/settings/partial/habilitations.part.html',
				ncyBreadcrumb: {
			    label: 'Habilitations'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.pisteaudit', {
				url:'/pisteaudit',
				templateUrl: 'app/modules/settings/partial/pisteAudit.part.html',
				ncyBreadcrumb: {
			    label: 'Traces Applicatives'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.formateurs', {
				url:'/formateurs',
				templateUrl: 'app/modules/settings/partial/formateurs.part.html',
				ncyBreadcrumb: {
			    label: 'Formateurs'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.promotions', {
				url:'/promotions',
				templateUrl: 'app/modules/settings/partial/promotions.part.html',
				ncyBreadcrumb: {
			    label: 'Promotions'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.modules', {
				url:'/modules',
				templateUrl: 'app/modules/settings/partial/modules.part.html',
				ncyBreadcrumb: {
			    label: 'Modules'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.salles', {
				url:'/salles',
				templateUrl: 'app/modules/settings/partial/salles.part.html',
				ncyBreadcrumb: {
			    label: 'Salles'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			});
	}]);
