(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('SidenavCtrl', Sidenav);


	Sidenav.$inject = ['SettingsService', '$state', '$mdSidenav'];

	function Sidenav(SettingsService, $state, $mdSidenav) {
		var vm = this;

		vm.navigateTo = function(to) {
			$state.go('home.settings.' + to);
		};

		vm.tabMenu = SettingsService.getMenu();
	}

})();
