(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('PromotionsCtrl', Promotions);

	Promotions.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope'];

	function Promotions(SettingsService, Utils, $timeout, $q, $scope) {
		var vm = this;

		vm.promotionSelected = null;

		vm.promotionSelected = null;
		$scope.$watch(vm.searchPromotion, vm.searchPromotionChange);

		vm.querySearch = function(query, searchType){
			var results;
			results = query ? vm.promotions.filter( createFilterPromotion(query) ) : vm.promotions;
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		/**
		* Filtre la liste de promotion par le nom et le prenom
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterPromotion(query) {
			return function filterFn(promotion) {
				if(promotion !== undefined){
					return (promotion.code.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		vm.selectPromotion = function (promotion) {
			vm.promotionSelected = promotion;
			console.log(vm.promotionSelected);
		};

		vm.setLieu = function (lieu) {
			if(vm.promotionSelected === null){
				Utils.viewToast("warn", "Veuillez selectionner une promotion.");
			} else {
				vm.promotionSelected.lieu = lieu;
				console.log(vm.promotionSelected);
				SettingsService.updatePromotion(vm.promotionSelected).then(function (response) {
					if(response.status === 200){
						Utils.viewToast("success", "La promotion a bien été mise à jour !");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}
		};

		/**
		* Met à jour la liste de promotion dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchpromotion [Le texte de la recherche]
		*/
		vm.searchPromotionChange = function(searchPromotion){
			if(vm.promotions.length !== vm.promotionsSave.length || vm.promotions.length === 1){
				vm.promotions = vm.promotionsSave;
			}
			vm.promotions = vm.promotions.filter( createFilterPromotion(searchPromotion) );
		};

		SettingsService.getPromotions().then(function(response) {
			if(response.status === 200){
				vm.promotions = response.data;
				vm.promotionsSave = vm.promotions;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		SettingsService.getLieux().then(function(response) {
			if(response.status === 200){
				vm.lieux = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});
	}

})();
