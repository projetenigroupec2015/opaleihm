(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('PisteAuditCtrl', PisteAudit);

	PisteAudit.$inject = ['SettingsService', 'Utils'];

	function PisteAudit(SettingsService, Utils) {
		var vm = this;

		vm.query = {
			order: 'date',
			reverse: false,
			limit: 10,
			page: 1
		};

		vm.hasPisteAudit = function () {
			return vm.holidays.length > 0;
		};

		SettingsService.getPisteAudit().then(function(response) {
			if(response.status === 200){
				console.log(response);
				vm.pisteAudit = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});
	}

})();
