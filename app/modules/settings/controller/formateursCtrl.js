(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('FormateursCtrl', Formateurs);

	Formateurs.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope', '$mdDialog'];

	function Formateurs(SettingsService, Utils, $timeout, $q, $scope, $mdDialog) {
		var vm = this;

		/**
		* Autocomplete pour recherche formateur et competence
		*/
		vm.selectedFormateur = null;
		$scope.$watch(vm.searchFormateur, vm.searchFormateurChange);
		$scope.$watch(vm.searchCompetence, vm.searchCompetenceChange);

		vm.querySearch = function(query, searchType){
			var results;
			if(searchType === "formateur"){
				results = query ? vm.formateurs.filter( createFilterFormateur(query) ) : vm.formateurs;
			} else if(searchType === "competence" ){
				results = query ? vm.competences.filter( createFilterCompetence(query) ) : vm.competences;
			}
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		/**
		* Filtre la liste de formateur par le nom et le prenom
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterFormateur(query) {
			return function filterFn(formateur) {
				if(formateur !== undefined){
					return (formateur.nom.toLowerCase().indexOf(query.toLowerCase()) === 0 || formateur.prenom.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		/**
		* Filtre la liste de competence par le libelle et le libelleCourt
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterCompetence(query) {
			return function filterFn(competence) {
				if(competence !== undefined){
					return (competence.code.toLowerCase().indexOf(query.toLowerCase()) === 0 || competence.libelle.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		/**
		* Affiche la liste de competence checked pour le formateur selectionne
		* @param  formateur [Le formateur selectionne]
		*/
		vm.selectFormateur = function(formateur){
			console.log(formateur);
			vm.selectedFormateur = formateur;
			if(vm.selectedFormateur.competences !== null){
				vm.competencesFormateur = vm.selectedFormateur.competences;
				console.log(vm.competences);
			}
			SettingsService.getAffectationByFormateur(vm.selectedFormateur.idFormateur).then(function(response) {
				if(response.status === 200){
					vm.selectedFormateur.affectationsLieux = response.data;
					vm.affectationsLieux = vm.selectedFormateur.affectationsLieux;
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		/**
		* Met à jour la liste de formateur dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchFormateur [Le texte de la recherche]
		*/
		vm.searchFormateurChange = function(searchFormateur){
			if(vm.formateurs.length !== vm.formateursSave.length || vm.formateurs.length === 1){
				vm.formateurs = vm.formateursSave;
			}
			vm.formateurs = vm.formateurs.filter( createFilterFormateur(searchFormateur) );
		};

		/**
		* Met à jour la liste de compétence dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String competence [Le texte de la recherche]
		*/
		vm.selectedCompetenceChange = function(competence){
			console.log(vm.selectedFormateur);
			if(vm.selectedFormateur !== null){
				if(!angular.isDefined(_.find(vm.competencesFormateur, competence))){
					vm.competencesFormateur.push(competence);
					updateCompetencesFormateur();
				} else {
					Utils.viewToast("error", "Cette compétence est déjà associée au module sélectionné.");
				}
			} else {
				Utils.viewToast("warn", "Veuillez sélectionner un formateur.");
			}
		};

		vm.removeCompetence = function (competence) {
			if(angular.isDefined(_.find(vm.competencesFormateur, competence))){
				var index = _.indexOf(vm.competencesFormateur, competence);
				vm.competencesFormateur.splice(index, 1);
				updateCompetencesFormateur();
			}
		};

		vm.removeAffectation = function (affectation) {
			if(angular.isDefined(_.find(vm.affectationsLieux, affectation))){
				var index = _.indexOf(vm.affectationsLieux, affectation);
				vm.affectationsLieux.splice(index, 1);
				updateAffectationFormateur();
			}
		};

		/**
		* Verifie si le lieu est le premier dans la liste
		* @param  Lieu  lieu [Le lieu]
		* @return {Boolean}      [Si premier, return true]
		*/
		vm.isDefault = function(affectation){
			if(affectation.default === true){
				return true;
			}
		};

		/**
		* Enregistre les informations dans le formateur selectionne
		*/
		function updateCompetencesFormateur(){
			if(vm.selectedFormateur !== null){
				SettingsService.updateCompetenceFormateur(vm.selectedFormateur).then(function(response) {
					Utils.viewToast("success", "Succès : modification réussie!");
				})
				.catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			}
		}

		function updateAffectationFormateur(){
			if(vm.selectedFormateur !== null){
				SettingsService.updateAffectationFormateur(vm.selectedFormateur).then(function(response) {
					Utils.viewToast("success", "Succès : modification réussie!");
				})
				.catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			}
		}

		/**
		* Récupère les formateurs via le service
		*/
		SettingsService.getFormateurs().then(function(response) {
			if(response.status === 200){
				vm.formateurs = response.data;
				vm.formateursSave = vm.formateurs;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les lieux via le service
		*/
		SettingsService.getLieux().then(function(response) {
			if(response.status === 200){
				vm.lieux = response.data;
				vm.lieuxSave = vm.lieux;
				$scope.lieux = vm.lieux;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les compétences via le service
		*/
		SettingsService.getCompetences().then(function(response) {
			if(response.status === 200){
				vm.competences = response.data;
				vm.competencesSave = vm.competences;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.newAffectation = function (affectation) {
			vm.selectedAffectation = affectation;
			if(vm.selectedFormateur !== null){
				$mdDialog.show({
					clickOutsideToClose: true,
					parent: angular.element(document.body),
					escapeToClose: true,
					preserveScope: true,
					scope: $scope,
					locals : {
						lieux : vm.lieux,
						formateur : vm.selectedFormateur,
						affectation : 	vm.selectedAffectation
					},
					templateUrl: 'app/modules/settings/partial/affectation.tmpl.html',
					controller: function DialogController(SettingsService, $mdDialog, $scope, lieux, formateur, affectation)	{
						$scope.lieux = lieux;
						$scope.formateur = formateur;

						console.log($scope.formateur);
						$scope.identite = formateur.nom + " " + formateur.prenom;
						if(angular.isDefined(affectation)){
								$scope.newAffectation = affectation;
						}

						$scope.closeDialog = function() {
							delete $scope.formateur.affectationsLieux;
							$scope.newAffectation.formateur = $scope.formateur;
							console.log($scope.newAffectation.formateur);

							$scope.newAffectation.lieu = _.find($scope.lieux, $scope.newAffectation.lieu.id);
							console.log($scope.newAffectation.lieu);
							if($scope.newAffectation.default === true){
								$scope.newAffectation.preference = 0;
							}

							if(!angular.isDefined(_.find($scope.formateur.affectationsLieux, $scope.newAffectation))){
									$scope.newAffectation.id = 0;
									$scope.formateur.affectationsLieux = [];
									$scope.formateur.affectationsLieux.push($scope.newAffectation);
							}

							console.log($scope.formateur);

							SettingsService.updateAffectationFormateur($scope.formateur).then(function(response) {
								// if(response.status === 200){
									Utils.viewToast("success", "L'affectation a bien été créée !");
									// newAffectation = {};
								// } else {
								// 	Utils.viewToast("error", "Erreur :" + response.err + ". " + response.data);
								// }
							});
							$mdDialog.hide();
						};
					}
				});
			} else {
				Utils.viewToast("warn", "Veuillez sélectionner un formateur.");
			}
		};

	}

})();
