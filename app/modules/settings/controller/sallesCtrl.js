(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('SallesCtrl', Salles);

	Salles.$inject = ['SettingsService', 'Utils'];

	function Salles(SettingsService, Utils) {
		var vm = this;

		var lieuSelected;
		vm.materielSelected = false;
		vm.contentTitle = "Equipements présents dans la salle";

		SettingsService.getLieux().then(function(response) {
			if(response.status === 200){
				vm.lieux = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.isDisponible = function (salle, disponible) {
			salle.disponibilite = !disponible;
			SettingsService.updateDispoSalle(salle).then(function(response) {
				if(response.status === 200){
					Utils.viewToast("success", "La salle a bien été mise à jour !");
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		vm.selectSalles = function(lieu){
			lieuSelected = lieu;
			SettingsService.getSallesByLieu(lieu).then(function(response) {
				if(response.status === 200){
					vm.salles = response.data;
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
			vm.materiels = [];
		};

		vm.selectMateriels = function(salle){
			vm.contentTitle = "Equipements présents dans la salle";
			vm.selectedSalle = salle;
			vm.materiels = salle.equipements;
			vm.isSalleSelected = true;
			vm.isMaterielSelected = false;
			vm.isNew = false;
			vm.materielSelected = {};
			showDivider(vm.materiels.length);
		};

		function showDivider(size){
			if(size > 1) {
				vm.showDivider = true;
			}
		}

		vm.selectMateriel = function(materiel){
			vm.contentTitle = "Modifier l'équipement";
			vm.isMaterielSelected = true;
			vm.materielSelected = materiel;
			vm.materielSelected.finGarantie = new Date(materiel.finGarantie);
			vm.materielSelected.dernierNettoyage = new Date(materiel.dernierNettoyage);
		};

		vm.addMateriel = function(){
			if(angular.isDefined(vm.selectedSalle)){
				vm.contentTitle = "Ajouter un équipement";
				vm.isMaterielSelected = true;
				vm.isNew = true;
				vm.materielSelected = {};
			} else {
				Utils.viewToast("warn", "Veuillez sélectionner une salle.");
			}
		};

		vm.deleteEquipement = function (equipement) {
			SettingsService.deleteEquipement(equipement).then(function (response) {
				if(response.status === 200){
					vm.selectedSalle.equipements.splice(equipement, 1);
					Utils.viewToast("success", "L'équipement a bien été supprimé !");
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		vm.save = function(toSave, isNew){
			if(isNew === true){
				if(_.indexOf(vm.selectedSalle.equipements, toSave) === -1){
						vm.selectedSalle.equipements.push(toSave);
				}
				SettingsService.updateSalle(vm.selectedSalle).then(function(response) {
					if(response.status === 200){
						vm.selectSalles(lieuSelected);
						Utils.viewToast("success", "L'équipement a bien été ajouté !");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			} else {
				SettingsService.updateSalle(vm.selectedSalle).then(function(response) {
					if(response.status === 200){
						Utils.viewToast("success", "L'équipement a bien été modifié !");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}
		};
	}

})();
