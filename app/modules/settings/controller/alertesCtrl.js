(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('AlertesCtrl', Alertes);

	Alertes.$inject = ['SettingsService', 'Utils'];

	function Alertes(SettingsService, Utils) {
		var vm = this;

		SettingsService.getAlerts().then(function(response) {
			if(response.status === 200){
				console.log(response);
				vm.alertes = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.save = function (alertes) {
			_.forEach(alertes, function (alerte) {
				SettingsService.updateParams(alerte).then(function (response) {
						if(response.status === 200){
							Utils.viewToast("success", "Les alertes ont bien été mises à jour !");
							SettingsService.getAlerts().then(function(response) {
								if(response.status === 200){
									console.log(response);
									vm.alertes = response.data;
								} else {
									Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
								}
							});
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					});
			});
		};

	}

})();
