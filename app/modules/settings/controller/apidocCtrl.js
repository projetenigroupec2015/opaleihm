(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('ApiDocCtrl', ApiDoc);

	ApiDoc.$inject = ['config'];

	function ApiDoc (config) {
		var vm = this;
		vm.isLoading = false;
		vm.url = config.apiUrl + '/v2/api-docs';
		// error management
		vm.myErrorHandler = function (data, status) {
			console.log('failed to load swagger: ' + status + '   ' + data);
		};

		vm.infos = false;
	}

})();
