(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('ModulesCtrl', Modules);

	Modules.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope', '$rootScope', '$mdDialog'];

	function Modules(SettingsService, Utils, $timeout, $q, $scope, $rootScope, $mdDialog) {
		var vm = this;

		$scope.$watch(vm.searchModule, vm.searchModuleChange);

		vm.querySearch = function(query, searchType){
			var results;
			if(searchType === "equipement"){
				results = query ? vm.equipements.filter( createFilterEquipement(query) ) : vm.equipements;
			} else if(searchType === "module" ){
				results = query ? vm.modules.filter( createFilterModule(query) ) : vm.modules;
			} else if(searchType === "competence" ){
				results = query ? vm.competences.filter( createFilterCompetence(query) ) : vm.competences;
			}
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		function createFilterModule(query) {
			return function filterFn(module) {
				if(module !== undefined){
					return (module.libelle.toLowerCase().indexOf(query.toLowerCase()) === 0 || module.libelleCourt.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		function createFilterEquipement(query) {
			return function filterFn(equipement) {
				if(equipement !== undefined){
					return (equipement.lot.toLowerCase().indexOf(query.toLowerCase()) === 0 || equipement.modele.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		function createFilterCompetence(query) {
			return function filterFn(competence) {
				if(competence !== undefined){
					return (competence.code.toLowerCase().indexOf(query.toLowerCase()) === 0 || competence.libelle.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		/**
		* Met à jour la liste de module dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchModule [Le texte de la recherche]
		*/
		vm.searchModuleChange = function(searchModule){
			if(vm.modules.length !== vm.modulesSave.length || vm.modules.length === 1){
				vm.modules = vm.modulesSave;
			}
			vm.modules = vm.modules.filter( createFilterModule(searchModule) );
		};

		/**
		* Met à jour la liste de compétence dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String competence [Le texte de la recherche]
		*/
		vm.selectedCompetenceChange = function(competence){
			if(!angular.isDefined(_.find(vm.competencesModule, competence))){
					vm.competencesModule.push(competence);
					updateModule();
			} else {
				Utils.viewToast("error", "Cette compétence est déjà associée au module sélectionné.");
			}
		};

		/**
		* Met à jour la liste d'équipement dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchEquipement [Le texte de la recherche]
		*/
		vm.selectedEquipementChange = function(equipement){
			if(!angular.isDefined(_.find(vm.equipementsModule, equipement))){
					vm.equipementsModule.push(equipement);
					updateModule();
			} else {
				Utils.viewToast("error", "Cet équipement est déjà associé au module sélectionné.");
			}
		};

		vm.removeCompetence = function (competence) {
			if(angular.isDefined(_.find(vm.competencesModule, competence))){
					var index = _.indexOf(vm.competencesModule, competence);
					vm.competencesModule.splice(index, 1);
					updateModule();
			}
		};

		vm.removeEquipement = function (equipement) {
			console.log("removeEquipement");
			if(angular.isDefined(_.find(vm.equipementsModule, equipement))){
					var index = _.indexOf(vm.equipementsModule, equipement);
					vm.equipementsModule.splice(index, 1);
					updateModule();
			}
		};

		vm.getModulesSpecs = function (module) {
			vm.selectedModule = module;
			vm.equipementsModule = module.equipements;
			vm.competencesModule = module.competences;
		};

		function updateModule() {
			SettingsService.updateModule(vm.selectedModule).then(function (response) {
				if(response.status === 200){
					Utils.viewToast("success", "Le module a bien été mis à jour !");
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		}

		/**
		* Récupère les modules via le service
		*/
		SettingsService.getModules().then(function(response) {
			if(response.status === 200){
				vm.modules = response.data;
				vm.modulesSave = vm.modules;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les équipements via le service
		*/
		SettingsService.getEquipements().then(function(response) {
			if(response.status === 200){
				vm.equipements = response.data;
				vm.equipementsSave = vm.equipements;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les compétences via le service
		*/
		SettingsService.getCompetences().then(function(response) {
			if(response.status === 200){
				vm.competences = response.data;
				vm.competencesSave = vm.competences;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.newCompetence = function (event) {
			$mdDialog.show({
				clickOutsideToClose: true,
				escapeToClose: true,
				preserveScope: true,
				scope: $scope,
				templateUrl: 'app/modules/settings/partial/competences.tmpl.html',
				controller: function DialogController(SettingsService, $mdDialog, $scope)	{
					$scope.closeDialog = function() {
						SettingsService.createCompetence($scope.newCompetence).then(function(response) {
							if(response.status === 201){
								Utils.viewToast("success", "La compétence a bien été créée !");
								SettingsService.getCompetences().then(function(response) {
									if(response.status === 200){
										vm.competences = response.data;
										vm.competencesSave = vm.competences;
									} else {
										Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
									}
								});
							} else {
								Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
							}
						});
						$mdDialog.hide();
					};
				}
			});
		};
	}

})();
