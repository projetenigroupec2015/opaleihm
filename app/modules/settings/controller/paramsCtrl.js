(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('ParametresCtrl', Parametres);


	Parametres.$inject = ['SettingsService', 'Utils'];

	function Parametres(SettingsService, Utils) {
		var vm = this;

		SettingsService.getParams().then(function(response) {
			if(response.status === 200){
				vm.params = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.save = function (params) {
			_.forEach(params, function (param) {
				SettingsService.updateParams(param).then(function (response) {
						if(response.status === 200){
							Utils.viewToast("success", "Le paramétrage a bien été mis à jour !");
							SettingsService.getParams().then(function(response) {
								if(response.status === 200){
									vm.params = response.data;
								} else {
									Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
								}
							});
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					});
			});
		};

	}

})();
