(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('HabilitationsCtrl', Habilitations);


	Habilitations.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope'];

	function Habilitations(SettingsService, Utils, $timeout, $q, $scope) {
		var vm = this;

		vm.formateurSelected = null;

		vm.formateurSelected = null;
		$scope.$watch(vm.searchFormateur, vm.searchFormateurChange);
		$scope.$watch(vm.searchModule, vm.searchModuleChange);

		vm.querySearch = function(query, searchType){
			var results;
			results = query ? vm.formateurs.filter( createFilterFormateur(query) ) : vm.formateurs;
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		/**
		* Filtre la liste de formateur par le nom et le prenom
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterFormateur(query) {
			return function filterFn(formateur) {
				if(formateur !== undefined){
					return (formateur.nom.toLowerCase().indexOf(query.toLowerCase()) === 0 || formateur.prenom.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}


		vm.getHumanRole = function (habil) {
			switch (habil) {
				case "admin":
				return "administrateur";
				case "resp_form":
				return "reponsable formation";
				case "form":
				return "formateur";
			}
		};

		vm.selectFormateur = function (formateur) {
			vm.formateurSelected = formateur;
			console.log(vm.formateurSelected);
		};

		vm.setHabilitation = function (habilitation) {
			if(vm.formateurSelected === null){
				Utils.viewToast("warn", "Veuillez selectionner un formateur.");
			} else {
				vm.formateurSelected.habilitation = habilitation;
				console.log(vm.formateurSelected);
				SettingsService.updateHabilitationFormateur(vm.formateurSelected).then(function (response) {
					if(response.status === 200){
						Utils.viewToast("success", "L'habilitation  a bien été affectée.");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}
		};

		/**
		* Met à jour la liste de formateur dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchFormateur [Le texte de la recherche]
		*/
		vm.searchFormateurChange = function(searchFormateur){
			if(vm.formateurs.length !== vm.formateursSave.length || vm.formateurs.length === 1){
				vm.formateurs = vm.formateursSave;
			}
			vm.formateurs = vm.formateurs.filter( createFilterFormateur(searchFormateur) );
		};

		SettingsService.getFormateurs().then(function(response) {
			if(response.status === 200){
				vm.formateurs = response.data;
				vm.formateursSave = vm.formateurs;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		SettingsService.getHabilitations().then(function(response) {
			if(response.status === 200){
				vm.habilitations = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});
	}

})();
