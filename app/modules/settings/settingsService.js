(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:settingsService
	* @description
	* # settingsService
	* Service of the app
	*/

	angular
	.module('settings')
	.factory('SettingsService', Settings);

	Settings.$inject = ['$http', 'config', 'Dao', 'cache'];

	function Settings ($http, config, Dao, cache) {

		var menu = [
			{
				name: "Application",
				items: [
					{
						name: "Alertes",
						url: "alertes"
					},
					{
						name: "Paramètres",
						url: "params"
					},
					{
						name: "Habilitations",
						url: "habilitations"
					},
					{
						name: "Traces applicatives",
						url: "pisteaudit"
					}
				]
			},
			{
				name: "Ressources",
				items: [
					{
						name: "Formateurs",
						url: "formateurs"
					},
					{
						name: "Modules",
						url: "modules"
					},
					{
						name: "Salles",
						url: "salles"
					},
					{
						name: "Promotion",
						url: "promotions"
					},
				]
			},
		];

		var settings = {
			getMenu : getMenu,
			getAlerts : getAlerts,
			getParams: getParams,
			getLieux : getLieux,
			getPisteAudit : getPisteAudit,
			getSallesByLieu : getSallesByLieu,
			getFormateurs : getFormateurs,
			getFormations : getFormations,
			getHabilitations : getHabilitations,
			getPromotions: getPromotions,
			updateHabilitationFormateur : updateHabilitationFormateur,
			getAffectationByFormateur : getAffectationByFormateur,
			getModules : getModules,
			updateModule : updateModule,
			getEquipements : getEquipements,
			getCompetences : getCompetences,
			deleteEquipement: deleteEquipement,
			updateSalle : updateSalle,
			updatePromotion : updatePromotion,
			updateDispoSalle : updateDispoSalle,
			updateParams: updateParams,
			updateCompetenceFormateur : updateCompetenceFormateur,
			updateAffectationFormateur : updateAffectationFormateur,
			createCompetence : createCompetence
		};

		return settings;

		function getMenu() {
			return menu;
		}

		function getAlerts(){
			return Dao.crudData("get", config.apiUrl + "/alertes");
		}

		function getParams(){
			return Dao.crudData("get", config.apiUrl + "/parametrages");
		}

		function getLieux(){
			return Dao.crudData("get", config.apiUrl + "/lieux");
		}

		function getPisteAudit(){
			return Dao.crudData("get", config.apiUrl + "/pisteaudit");
			// return Dao.crudData("get", "app/mock/traces.json");
		}

		function getSallesByLieu(lieu){
			return Dao.crudData("get", config.apiUrl + "/salles/" + lieu +"/");
		}

		function getFormateurs(){
			return Dao.crudData("get", config.apiUrl + '/formateurs');
		}

		function getFormations() {
			return Dao.crudData("get", config.apiUrl + '/formations');
		}

		function getHabilitations(){
			return Dao.crudData("get", config.apiUrl + "/habilitations");
		}

		function getModules(){
			return Dao.crudData("get", config.apiUrl + '/modules');
		}

		function getPromotions(){
			return Dao.crudData("get", config.apiUrl + '/promotions');
		}

		function getAffectationByFormateur(idFormateur){
			return Dao.crudData("get", config.apiUrl + '/formateurs/' + idFormateur + '/affectations');
		}

		function updateAffectationFormateur(formateur){
			return Dao.crudData("put", config.apiUrl + "/formateurs/" + formateur.idFormateur + '/affectation', formateur);
		}


		function updateModule(module){
			return Dao.crudData("put", config.apiUrl + "/modules/" + module.id, module);
		}

		function getEquipements(){
			return Dao.crudData("get", config.apiUrl + '/equipements');
		}

		function getCompetences(){
			return Dao.crudData("get", config.apiUrl + '/competences');
		}

		function deleteEquipement(equipement){
			return Dao.crudData("delete", config.apiUrl + "/equipements/" + equipement.id);
		}

		function updatePromotion(promotion){
			return Dao.crudData("put", config.apiUrl + "/promotion", promotion);
		}

		function updateSalle(salle){
			return Dao.crudData("put", config.apiUrl + "/salle", salle);
		}

		function updateDispoSalle(salle){
			return Dao.crudData("put", config.apiUrl + "/salle/indisponible", salle);
		}

		function updateParams(parametrage){
			return Dao.crudData("put", config.apiUrl + "/parametrage/" + parametrage.code, parametrage);
		}

		function updateHabilitationFormateur(formateur){
			return Dao.crudData("put", config.apiUrl + "/formateurs/" + formateur.idFormateur + "/habilitation", formateur);
		}

		function updateCompetenceFormateur(formateur){
			return Dao.crudData("put", config.apiUrl + "/formateurs/" + formateur.idFormateur + "/competences", formateur);
		}

		function createCompetence(competence){
			return Dao.crudData("post", config.apiUrl + "/competence", competence);
		}

	}

})();
