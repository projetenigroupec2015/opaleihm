(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:HomeCtrl
	* @description
	* # HomeCtrl
	* Controller of the app
	*/

	angular
	.module('opale')
	.controller('HomeCtrl', Home)
	.controller('DashboardCtrl', Dashboard);

	Home.$inject = ['$scope','$state', 'homeService', 'config','$rootScope', 'Utils', 'LoginService', 'USER_ROLES','planningService'];
	Dashboard.$inject = ['homeService', 'Utils', '$rootScope', '$scope'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Home($scope, $state, homeService, config, $rootScope, Utils, LoginService, USER_ROLES,planningService) {
		/*jshint validthis: true */
		var vm = this;
		vm.title = config.appName;
		vm.subtitle = config.description;
		vm.loading = false;
		vm.loadingText = "Veuillez patientez...";
		vm.version = "1.0.0";
		vm.months = getMonths();
		vm.years = getYears();

		vm.showSidePanel = false;

		vm.isAuthorized = function (authorizedRole) {
			if(LoginService.isAuthorized(authorizedRole)){
				return true;
			}
		};

		var userId = $rootScope.account.id;
		vm.notifications = getNotifications(homeService, vm, Utils, $rootScope, userId);

		$scope.$on('notificationsUpdate', function (event, data) {
			vm.notifications = data.notifs;
			vm.nbNotif = data.nbNotif;
		});

		function getMonths(){
			var months = [];
			for(var i = 0; i < 12; i++){
				var month = moment().month(i).format('MMMM');
				months.push(month);
			}
			return months;
		}

		function getYears(){
			var yearCurrent = moment().year();
			var year = parseInt(yearCurrent);
			var years = [];
			years.push(year);
			for (var i = 0; i < 3; i++) {
				year+= 1;
				years.push(year);
			}
			years.unshift(yearCurrent-1);
			years.unshift(yearCurrent-2);
			return years;
		}

		vm.menu = homeService.getMenuList();

		vm.syncLink = {
			link: 'refresh',
			name: 'Rafraîchissement',
			icon: 'sync',
			tooltip: 'Synchroniser les données',
			authorizedRoles: [USER_ROLES.admin]
		};

		vm.notification = 	{
			link: 'notifications',
			name: 'Notifications',
			icon: 'notifications',
			tooltip: 'Notifications',
			authorizedRoles: [USER_ROLES.all]
		};



		vm.userMenu = {
			link: 'user',
			name: 'User',
			icon: 'person',
		};

		/** Ouverture du menu */
		var originatorEv;
		vm.openMenuUser = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		vm.openNotif = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		vm.getNotifTheme = function (typeNotif) {
			if (typeNotif) {
				switch (typeNotif.toLowerCase()) {
					case 'conge':
					return 'notifications info';
					case 'fiche_temps':
					return 'notifications success';
					case 'alerte':
					return 'notifications error';
					default:
					return 'notifications warning';
				}
			}
			return 'notifications warning';
		};

		vm.getNotifIconName = function (typeNotif) {
			if (typeNotif) {
				switch (typeNotif.toLowerCase()) {
					case 'conge':
					return 'access_time';
					case 'fiche_temps':
					return 'assignment';
					case 'alerte':
					return 'warning';
					default:
					return 'notifications';
				}
			}
			return 'notifications';

		};

		vm.syncData = function syncData(){
			vm.loading = true;
			return homeService.syncData().then(function(response) {
				if(response.status === 200){
					vm.loading = false;
					Utils.viewToast("La synchronisation s'est correctement déroulée !");
				} else {
					vm.loading = false;
					Utils.viewToast("Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		vm.navigateTo = function (target) {
			if(target === 'home.planning'){
				var interval = planningService.getIntervalDate(moment().year(),moment().month());
				$rootScope.showSidePanel = true;
				$rootScope.data = undefined;
				$state.go(target, {dateStart:interval.dateStart,dateEnd:interval.dateEnd});
			}else {
				$rootScope.showSidePanel = false;
				if(vm.filterAccess){
					vm.setFilterAccess();
				}
				$state.go(target);
			}
		};
		/* Action sur le bouton logout du menu User */
		vm.logout = function(){
			homeService.logout();
		};

		/** FILTERS */
		vm.filterAccess = vm.filterAccess === undefined ? false : vm.filterAccess;
		vm.setFilterAccess = function(){
			if(vm.filterAccess){
				vm.filterAccess = false;
				$('.ng-pageslide').css({height:'0px'});
			}else {
				vm.filterAccess = true;
				$('.ng-pageslide').css({height:'200px'});
			}
		};

		vm.setFiltersSelected = function(){
			$scope.$broadcast('filters', {apply:true});
		};
	}

	function Dashboard (homeService, Utils, $rootScope, $scope) {
		var vm = this;
		var userId = $rootScope.account.id;
		getNotifications(homeService, vm, Utils, $rootScope, userId);
	}

	function getNotifications(homeService, vm, Utils, $rootScope, idFormateur) {
		if (idFormateur) {
			homeService.getNotificationsByFormateurId(idFormateur)
				.then(function (response) {
					if(response.status === 200){
						vm.notifications = response.data;
						vm.nbNotif = vm.notifications.length;
					} else {
						if(response.err === -1){
							Utils.viewToast("error", response.data);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				});
		}
	}

})();
