'use strict';

	/**
	* @ngdoc function
	* @name app.route:HomeRoute
	* @description
	* # HomeRoute
	* Route of the app
	*/

angular.module('opale')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {
		$stateProvider
			.state('home', {
				url: '',
				abstract: true,
				templateUrl: 'app/modules/home/main.html',
				controller: 'HomeCtrl',
				controllerAs: 'vm',
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.all]
				}
			})
			.state('home.dashboard', {
				url:'/dashboard',
				templateUrl: 'app/modules/home/dashboard.html',
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			});

	}]);
