(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:homeService
	* @description
	* # homeService
	* Service of the app
	*/

	angular.module('opale')
	.factory('homeService', homeService);

	homeService.$inject = ['$http','config', 'Dao','cache', 'LoginService', 'USER_ROLES'];

		function homeService($http, config, Dao, cache, LoginService, USER_ROLES) {
			var menu = [
				{
					link: 'dashboard',
					name: 'Dashboard',
					icon: 'apps',
					tooltip: 'Tableau de bord',
					authorizedRoles: [USER_ROLES.all]
				},

				// {
				// 	link: 'timesheet',
				// 	name: 'Timesheet',
				// 	icon: 'assignment',
				// 	tooltip: 'Fiche de temps',
				// 	authorizedRoles: [USER_ROLES.all]
				// },

				{
					link: 'vacationrequest',
					name: 'Vacationrequest',
					icon: 'access_time',
					tooltip: 'Congés',
					authorizedRoles: [USER_ROLES.all]
				},

				{
					link: 'planning',
					name: 'Planning',
					icon: 'event',
					tooltip: 'Planning',
					authorizedRoles: [USER_ROLES.all]
				},
				{
					link: 'settings',
					name: 'Settings',
					icon: 'settings',
					tooltip: 'Paramétrage',
					authorizedRoles: [USER_ROLES.admin]
				},

			];

			function getNotifications(){
				return Dao.crudData("get", config.apiUrl + "/notifications");
			}

			function getNotificationsByFormateurId(formateurId){
				return Dao.crudData("get", config.apiUrl + "/formateur/" + formateurId + "/notifications");
			}

			function getFormations(){
				return Dao.crudData("get", config.apiUrl + "/formations");
			}

			function getPromotions(dateStart,dateEnd){
				return Dao.crudData("get", config.apiUrl + "/promotions/"+ dateStart + "/" + dateEnd);
			}

			function getLieux(){
				return Dao.crudData("get", config.apiUrl + "/lieux");
			}

			function getParametres(){
				return Dao.crudData("get",config.apiUrl + "/parametrages");
			}

			function getAlertes(){
				return Dao.crudData("get",config.apiUrl + "/alertes");
			}

			function getFormateurs(){
				return Dao.crudData("get", config.apiUrl + "/formateurs");
			}

			function getSalles(){
				return Dao.crudData("get", config.apiUrl + "/salles");
			}


			function syncData(){
				return Dao.crudData("get", config.apiUrl + "/etl-load");
			}

			function logout() {
				LoginService.logout();
			}

			function getMenuList() {
				return menu;
			}

			return {
				getMenuList: getMenuList,
				getNotifications: getNotifications,
				getNotificationsByFormateurId: getNotificationsByFormateurId,
				getFormations: getFormations,
				getPromotions: getPromotions,
				getLieux : getLieux,
				getSalles : getSalles,
				getFormateurs : getFormateurs,
				getAlertes : getAlertes,
				getParametres : getParametres,
				syncData: syncData,
				logout: logout
			};
		}

})();
