'use strict';

/**
 * @ngdoc function
 * @name app.route:planningRoute
 * @description
 * # planningRoute
 * Route of the app
 */

angular.module('planning')
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			.state('home.planning', {
				url:'/planning/:dateStart/:dateEnd',
				templateUrl: 'app/modules/planning/planning.html',
				controller: 'PlanningCtrl',
				controllerAs: 'vm',
				resolve: {

					parametres: ["homeService",function(homeService){
						return homeService.getParametres();
					}],
					alertes: ["homeService",function (homeService) {
						return homeService.getAlertes();
					}],
					formateurs: ["homeService", function(homeService) {
						return homeService.getFormateurs();
					}],
					salles: ["homeService", function(homeService) {
						return homeService.getSalles();
					}],
					promotions: ['homeService','$stateParams',function(homeService,$stateParams){
						return homeService.getPromotions($stateParams.dateStart,$stateParams.dateEnd);
					}]
				}
			});
	}]);
