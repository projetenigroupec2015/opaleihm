(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:planningCtrl
	 * @description
	 * # planningCtrl
	 * Controller of the app
	 */

	angular
		.module('planning')
		.controller('PlanningCtrl', Planning);

	Planning.$inject = ['$rootScope','config','planningService','parametres','alertes','formateurs','salles','promotions','_','$scope','Utils','$stateParams','$q','$state','$mdSidenav'];

	function Planning($rootScope, config,planningService,parametres,alertes,formateurs,salles,promotions,_,$scope,Utils,$stateParams,$q,$state,$mdSidenav) {
		var vm = this;
		vm.weeks = [];
		vm.studyOfTheWeek = vm.studyOfTheWeek > 0 ? vm.studyOfTheWeek : 0;
		vm.contentMonths = [];
		vm.lessons = {};
		vm.alerts = [];
		$rootScope.displayFilters = {
			formations:[],
			promotions:[],
			formateurs:[],
			lieux:[],
			cities:[]
		};
		vm.displayPlanningImpossible = [];
		if($rootScope.data === undefined){
			$rootScope.data = {
				formations:[],
				formateurs:[],
				promotions:[],
				salles:[],
				lieux:[],
				apply: false,
				filtersSelected:{
					formations :[],
					promotions:[],
					lieux:[],
					salles:[],
					formateurs:[],
					viewPlanning:'promotion',
					month:moment().format('MMMM'),
					year:moment().format('YYYY')
				}
			};
		}
		/** FILTRES */
		getEntities();
		function getEntities(){
			getPromotions().then(function() {
				$rootScope.data.formations = _.sortBy($rootScope.data.formations);
				$rootScope.data.lieux = _.sortBy($rootScope.data.lieux);
				if($rootScope.data.filtersSelected.formations.length === 0){
					$rootScope.data.filtersSelected.formations = $rootScope.data.formations;
				}
				if($rootScope.data.filtersSelected.lieux.length === 0){
					$rootScope.data.filtersSelected.lieux = $rootScope.data.lieux;
				}
			}).then(function(){
				/** récupération des salles et formateurs, une fois les données associées au promotions récupérées. */
				$rootScope.data.alertes = alertes.data;
				$rootScope.data.parametres = parametres.data;
				$rootScope.data.salles = salles.data;

				$rootScope.data.formateurs = formateurs.data;
				if($rootScope.data.filtersSelected.salles.length === 0){
					_.each($rootScope.data.salles,function(salle,index){
						if(salle && salle.lieu){
							$rootScope.data.filtersSelected.salles[index] = salle.lieu.libelle+'-'+salle.code;
						}
					});
				}
				_.each($rootScope.data.formateurs,function(formateur,index){
					$rootScope.data.filtersSelected.formateurs[index] = formateur.nom + ' ' + formateur.prenom;
				});
			}).then(function(){
				/** Récupération des cours. */
				getLessons();
			});
		}
		/** récupère les promotions et les données associées. */
		function getPromotions(){
			var indexPromotionToDeleted = [];
			var deferred = $q.defer();
			if($rootScope.data.promotions.length > 0){
				var filtersPromotionsAvailable = [];
				_.each($rootScope.data.promotions,function(promotion){
					filtersPromotionsAvailable.push(promotion.code);
				});
				var unselectedPromotionsFilters = _.difference(filtersPromotionsAvailable,$rootScope.data.filtersSelected.promotions);
				_.each(unselectedPromotionsFilters,function(unselectedPromotion){
					var index = _.findLastIndex($rootScope.data.promotions,{code:unselectedPromotion});
					indexPromotionToDeleted.push(index);
				 });
			}else{
				$rootScope.data.promotions = promotions.data;
			}

			_.each($rootScope.data.promotions, function(promotion,index){
				if(_.contains(indexPromotionToDeleted,index)){
					$rootScope.data.filtersSelected.promotions.splice(index, 1);
				}else{
					$rootScope.data.filtersSelected.promotions[index] = promotion.code;
				}
				if(!angular.isUndefined(promotion.lieu) && promotion.lieu != null && !_.contains($rootScope.data.lieux,promotion.lieu.libelle)){
					$rootScope.data.lieux.push(promotion.lieu.libelle);
				}

				if(!angular.isUndefined(promotion.formation) && promotion.formation != null && !_.contains($rootScope.data.formations,promotion.formation.code)){
					$rootScope.data.formations.push(promotion.formation.code);
				}
			});
			deferred.resolve("success");
			return deferred.promise;
		}

		/** récupère les cours en fonction du mois sélectionné. */
		function getLessons(){
			vm.lessons = [];
			vm.interval = planningService.getIntervalDate($rootScope.data.filtersSelected.year,($rootScope.data.filtersSelected.month+1));
			vm.getWeeks = getWeeks();
			/** @WebService */
			planningService.getLessons(vm.interval).then(function(response) {
				if(response.status === 200){
					_.each(response.data,function(lesson){
						/** gestion du cours à cheval entre le premier mois affiché et le précédent. */
						var debutStart = moment(lesson.debut);
						if(debutStart.isBefore(vm.interval.dateStart,'week') && lesson.module.dureeEnSemaines > 1){
							for(var i = 0; i < lesson.module.dureeEnSemaines;i++){
								debutStart.add(1,'weeks');
								lesson.module.dureeEnSemaines -= 1;
								if(debutStart.isSame(vm.interval.dateStart,'week')){
									break;
								}
							}
						}
						lesson['week'] = debutStart.format('WW');
					});
					vm.lessons = response.data;
				}
			}).then(function(){
				displayPlanning(vm.lessons);
			});
		}

		/** Récupération des fitlres sélectionnées. */
		$scope.$on('filters', function (event, arg) {
			if(arg.apply) {
				var interval = planningService.getIntervalDate($rootScope.data.filtersSelected.year,($rootScope.data.filtersSelected.month+1));
				//$rootScope.data.promotions = [];
				/*$rootScope.data.filtersSelected.promotions = [];
				$rootScope.data.filtersSelected.formations = [];
				$rootScope.data.filtersSelected.lieux = [];
				$rootScope.data.filtersSelected.salles = [];*/
				$state.go('home.planning', {dateStart: interval.dateStart, dateEnd: interval.dateEnd},{reload:true});
			}
		});

		/** Vérifie que le cours correspond aux filtres sélectionnés. */
		function isFiltered(lesson){
			var isFiltered =  lesson.promotion && _.contains($rootScope.data.filtersSelected.promotions,lesson.promotion.code)
				&& lesson.promotion.formation && _.contains($rootScope.data.filtersSelected.formations,lesson.promotion.formation.code);
			if($rootScope.data.filtersSelected.viewPlanning === 'formateur') {
				isFiltered = isFiltered && lesson.formateur && _.contains($rootScope.data.filtersSelected.formateurs, lesson.formateur.nom + ' ' + lesson.formateur.prenom);
			}else if($rootScope.data.filtersSelected.viewPlanning === 'salle'){
				isFiltered = isFiltered && lesson.salle && lesson.salle.lieu && _.contains($rootScope.data.filtersSelected.salles,lesson.salle.lieu.libelle+'-'+lesson.salle.code);
			}
			return isFiltered;
		}

		/**
		 * récupère la formation en fonction de la promotion courante.
		 */
		function displayEntityFound(){

			var isPromotion = _.findIndex($rootScope.data.filtersSelected.promotions,function(p){
				return p.trim() === vm.row.displayBy.trim();
			});

			if($rootScope.data.filtersSelected.viewPlanning === 'promotion'){
				if(isPromotion > -1){
					var index = _.findIndex($rootScope.data.promotions,function(p){
						return p.code === vm.row.displayBy;
					});
					if($rootScope.data.promotions[index]) {
						var formation = $rootScope.data.promotions[index].formation;
						if (formation && !_.contains($rootScope.displayFilters.formations, formation.code)) {
							$rootScope.displayFilters.formations.push(formation.code);
						}
					}
				}
			}else if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
				if(!_.contains($rootScope.displayFilters.formateurs, vm.row.displayBy)){
					$rootScope.displayFilters.formateurs.push(vm.row.displayBy);
				}
			}
		}

		/**
		 * Rejecte les cours ayant des données importantes manquantes.
		 * @param lessonSorted : ensemble des cours filtrés
		 * @return lessonsSortedWithoutDataMissing
		 */
		function rejectLessonsWithDataMissing(lessonsSorted){
			var lessonsSortedWithoutDataMissing = lessonsSorted;
			if($rootScope.data.filtersSelected.viewPlanning === 'formateur') {
				lessonsSortedWithoutDataMissing = _.reject(lessonsSorted, function (l) {
					return l.formateur === null;
				});
			}else if($rootScope.data.filtersSelected.viewPlanning === 'salle'){
				lessonsSortedWithoutDataMissing = _.reject(lessonsSorted, function (l) {
					return l.salle === null;
				});
			}
			return lessonsSortedWithoutDataMissing;
		}

		/** Modifie les données du planning à afficher en ordonnée */
		vm.setViewPlanning = function (view){
			$rootScope.data.filtersSelected.viewPlanning = view;
			vm.displayPlanningImpossible = [];
			if($rootScope.data.filtersSelected.viewPlanning === 'promotion'){
				$rootScope.displayFilters.promotions = [];
				$rootScope.displayFilters.formations = [];
				$rootScope.displayFilters.lieux = [];
			}else if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
				$rootScope.displayFilters.formateurs = [];
			}
			$rootScope.displayFilters.cities = [];
			displayPlanning(vm.lessons);
		};

		/** AFFICHAGE DU PLANNING */

		/**
		 * Affiche le planning en fonction des cours planifiés.
		 * @param lessons:ensemble des cours planifiés.
		 */
		function displayPlanning(lessons){
			vm.contentMonths = [];
			vm.displayFilters = {
				formations : [],
				cities : [],
				promotions : []
			};
			vm.rowspan = {
				cities : {},
				previousCity : undefined,
				countCity : 0
			};
			vm.rowspanCity = {};
			var lessonsFiltered = _.filter(lessons,function(lesson){
				return isFiltered(lesson);
			});



			if(lessonsFiltered.length > 0){
				vm.displayPlanning = true;
				adaptDataToPlanning(lessonsFiltered);
				vm.managePlanning();
			}else {
				vm.displayPlanning = false;
			}
		}

		/** méthode mettant en forme les données récupérées du web service. */
		function adaptDataToPlanning(lessons){
			var lessonsWithoutDataMissing = rejectLessonsWithDataMissing(lessons);
			var lessonsFiltered = sortInTermsOfView(lessonsWithoutDataMissing);
			groupByView(lessonsFiltered);
		}

		/** trier en fonction de la vue souhaitée. */
		function sortInTermsOfView(lessonsWithoutDataMissing){
			var lessonsFiltered = _.sortBy(lessonsWithoutDataMissing, function (l) {
				return getDataInTermsOfViewSelected(l);
			});
			return lessonsFiltered;
		}

		/** définit la structure des données. */
		function groupByView(lessonsFiltered){
			/** regroument des données par lieu. */

			if($rootScope.data.filtersSelected.viewPlanning == 'salle'){
				vm.lessonsGrouped = _.groupBy(lessonsFiltered,function(l){
					if(l.salle && l.salle.lieu) {
						return l.salle.lieu;
					}
				});
			}else{
				vm.lessonsGrouped = _.groupBy(lessonsFiltered,function(l){
					if(l.promotion && l.promotion.lieu) {
						return l.promotion.lieu.libelle;
					}
				});
			}
			_.each(vm.lessonsGrouped, function (v, k) {
				vm.lessonsGrouped[k] = _.groupBy(v, function (l) {
					return getDataInTermsOfViewSelected(l);
				});
			});
		}
		/** récupère les données à afficher en fonction du filtre vue sélectionné. */
		function getDataInTermsOfViewSelected(l){
			var data = null;
			switch ($rootScope.data.filtersSelected.viewPlanning) {
				case 'promotion':
					if(l.promotion){data = l.promotion.code;}
					break;
				case 'formateur':
					if (l.formateur) {data = l.formateur.nom + ' ' + l.formateur.prenom;}
					break;
				case 'salle':
					if (l.salle && l.salle.lieu) {data = l.salle.lieu.libelle+'-'+l.salle.code;}
					break;
				default:
					if (l.promotion) {data = l.promotion.code;}
					break;
			}
			return data;
		}

		/** vérifie si le rowspan doit être appliqué. */
		vm.isRowspanApplied = function(city){
			if(vm.rowspan.previousCity !== city){
				vm.rowspan.previousCity = city;
				return true;
			}
			return false;
		};

		/** DONNEES PRINCIPALES DU PLANNING */

		/**
		 * Gestion des plannings
		 */
		vm.managePlanning = function(){
			if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
				planningService.getConges(vm.interval).then(function(response) {
					if(response.status === 200){
						vm.conges = response.data;
						treatmentAndResults();
					}else{
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}else{
				vm.conges = undefined;
				treatmentAndResults();
			}
		};

		function treatmentAndResults(){
			getDatas().then(function() {
				vm.rowspan.cities = _.groupBy(vm.contentMonths,function(v,k){
					return v.city;
				});
			}).then(function(){
				if(vm.displayPlanningImpossible.length === 1 ){
					Utils.viewToast("warn", "Un cours ne peut pas être planifié");
				}else if(vm.displayPlanningImpossible.length > 1){
					Utils.viewToast("warn", "Des cours ne peuvent pas être planifiés");
				}
			});
		}

		/**
		 * Gestion du contenu des promotions.
		 */
		function getDatas(){
			var deferred = $q.defer();
			for (var city in vm.lessonsGrouped) {
				if(!_.contains($rootScope.displayFilters.cities,city)){
					$rootScope.displayFilters.cities.push(city);
				}
				for(var displayBy in vm.lessonsGrouped[city]){
					vm.row = {
						city : city,
						displayBy : displayBy,
						lessons : undefined,
						display : false
					};
					vm.row.lessons = initDataForEachElementView();
					vm.planningImpossible = [];
					var conges = [];

					if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
						conges = manageConges(displayBy);
					}

					_.each(vm.lessonsGrouped[city][displayBy],function(lesson,key){
						var findError = _.find(vm.planningImpossible, function(planning){
							return planning.id == lesson.id;
						});
						if(isFiltered(lesson) && findError === undefined) {
							lesson.impossible = false;
							if(lesson.hasOwnProperty("otherLessonsDuringWeek")){
								delete lesson.otherLessonsDuringWeek;
							}
							if($rootScope.data.filtersSelected.viewPlanning === 'salle') {
								vm.row.city = lesson.salle.lieu.libelle;
							}

							if((moment(lesson.fin).diff(moment(lesson.debut),'weeks')+1) !== lesson.module.dureeEnSemaines) {
								lesson.module.dureeEnSemaines = (moment(lesson.fin).week() - moment(lesson.debut).week() + 1);
							}
							if(((moment(lesson.debut).week()-1) + (lesson.module.dureeEnSemaines-1)) <= (moment(lesson.fin).week()-1)){
								manageDureEnSemaines(lesson);
								var debutFormated = moment(lesson.debut).format(config.formatGeneric);
								var lessonsFiltered = vm.row.lessons;
								lessonsFiltered = _.filter(lessonsFiltered,function(l){
									return l !== undefined;
								});

								if (angular.isUndefined(vm.row.lessons[debutFormated])){
										if(lessonsFiltered && lessonsFiltered.length > 1 && moment(lessonsFiltered[lessonsFiltered.length-1])
											&& lesson.week >= (moment(lessonsFiltered[lessonsFiltered.length-1].debut).week()-1)
											&& lesson.week <= (moment(lessonsFiltered[lessonsFiltered.length-1].fin).week()-1)){
												manageLessonInSameWeek(lessonsFiltered,lessonsFiltered.length-1,lesson);
										}else{
											var impossible = impossibleCase(vm.lessonsGrouped[city][displayBy],lesson);
											addLesson(lesson,conges,impossible);
											lesson.alerts = manageAlerts(lesson);
										}
								}else{
									var impossible = impossibleCase(vm.lessonsGrouped[city][displayBy],lesson);
									if(!impossible){
										manageLessonInSameWeek(vm.row.lessons, debutFormated, lesson);
										vm.row.display = true;
									}
								}
								manageLessonPlanifiedForSeveralWeek(lesson);
							}
						}
					});
					displayEntityFound();

					if(vm.row.display){
						vm.contentMonths.push(vm.row);
						$rootScope.displayFilters.promotions.push(displayBy);
					}
					vm.displayPlanning = vm.contentMonths.length > 0 ? true : false;
				}
			}
			deferred.resolve('success');
			return deferred.promise;
		}

		/** Gestion du cas impossible*/
		function impossibleCase(lessonsFiltered,lesson){
			var find = _.find(vm.planningImpossible, function(planning){
				return planning.id == lesson.id;
			});
			if(find){return true};
			if(lesson.module.dureeEnSemaines > 1 && find === undefined) {
				_.find(lessonsFiltered,function(l){
					if(l.id !== lesson.id
						&& (moment(l.debut).week()-1) === (moment(lesson.fin).week()-1) && l.module.dureeEnSemaines > 1
						|| ((moment(l.debut).week()-1) > (moment(lesson.debut).week()-1))
						&& (moment(l.debut).week()-1) <= (moment(lesson.fin).week()-1)
						&& (moment(l.fin).week()-1) > (moment(lesson.fin).week()-1)){
						vm.planningImpossible.push(l);
						vm.displayPlanningImpossible.push(l);
					}
				});
			}
			return false;
		}

		/** Gestion des durées en semaines. */
		function manageDureEnSemaines(lesson){
			if(moment(lesson.fin).isSame(vm.interval.dateStart,'weeks')){
				lesson.module.dureeEnSemaines = (moment(vm.interval.dateStart).week()) - (moment(lesson.fin).week()-1);
			} else if(moment(lesson.debut).isBefore(vm.interval.dateStart)){
				lesson.module.dureeEnSemaines = (moment(lesson.fin).week()) - (moment(vm.interval.dateStart).week()-1);
			}

			if(lesson.fin > vm.interval.dateEnd) {
				var dateEndLesson = moment(lesson.fin);
				while(dateEndLesson.format(config.format) >= vm.interval.dateEnd) {
					dateEndLesson.subtract(1,'weeks');
					lesson.module.dureeEnSemaines -=  1;
				}
			}
		}

		/** Gestion des congés */
		function manageConges(displayBy){
			var conges = _.filter(vm.conges,function(c){
				return displayBy === (c.formateur.nom + ' ' + c.formateur.prenom) && c.etatConge != "REFUSE"
					&& (moment(c.dateFinConge).isBefore(vm.interval.dateEnd,'day') || moment(c.dateFinConge).isSame(vm.interval.dateEnd,'day'));
			});
			_.each(conges,function(c){
				var findLesson = vm.row.lessons[moment(c.dateDebutConge).format(config.formatGeneric)];
				c.dureeEnSemaines = moment(c.dateFinConge).diff(c.dateDebutConge,'weeks') + 1;
				if(angular.isUndefined(findLesson)){
					vm.row.lessons[moment(c.dateDebutConge).format(config.formatGeneric)] = c;
					if(c.dureeEnSemaines > 1){
						for(var index = c.dureeEnSemaines; index > 1; index --){
							var fin =  moment(c.dateDebutConge).clone().add(index-1,'weeks').format(config.formatGeneric);
							delete vm.row.lessons[fin];
						}
					}
				}
			});
			return conges;
		}

		/** ajout d'un cours, gestion du cas : ajout d'un cours pendant une période de vacances */
		function addLesson(lesson,conges,impossible){
			var findConge = _.find(conges,function(conge){
				return moment(lesson.debut).week() >= moment(conge.dateDebutConge).week()
					&& moment(lesson.fin).week() <= moment(conge.dateFinConge).week()
					|| moment(lesson.debut).week() == moment(conge.dateFinConge).week();
			});
			if(findConge === undefined) {
				if (!impossible) {
					if (moment(lesson.debut).isSame(vm.interval.dateStart) || moment(lesson.debut).isAfter(vm.interval.dateStart)) {
						if (moment(lesson.debut).day() > 1) {
							lesson.debut = moment(lesson.debut).startOf('week');
						}
						vm.row.lessons[moment(lesson.debut).format(config.formatGeneric)] = lesson;
					} else {
						lesson.week = vm.weeks[0].number;
						lesson.module.dureeEnSemaines = moment(lesson.fin).diff(moment(vm.interval.dateStart), 'weeks') + 1;
						vm.row.lessons[vm.weeks[0].start] = lesson;
					}
				}
			}else{
				if(lesson.module.dureeEnSemaines > 1){
					vm.planningImpossible.push(lesson);
					vm.displayPlanningImpossible.push(lesson);
				} else{
					manageLessonInSameWeek(vm.row.lessons, moment(findConge.dateDebutConge).format(config.formatGeneric), lesson);
				}
			}
			vm.row.display = true;
		}

		/** Gestion des cours ayant lieu la même semaine */
		function manageLessonInSameWeek(lessons, date, lesson){
			if (lessons[date].hasOwnProperty("otherLessonsDuringWeek")) {
				if(!_.contains(lesson.otherLessonsDuringWeek,lesson)) {
					lessons[date].otherLessonsDuringWeek.push(lesson);
				}
			} else {
				lessons[date].otherLessonsDuringWeek = [lesson];
			}
			if(!lesson.hasOwnProperty("etatConge") && !lessons[date].hasOwnProperty("etatConge")){
				var longestStrategicLesson = _.max(lessons[date].otherLessonsDuringWeek, function (l) {
					if (l) {
						return moment(l.fin).diff(moment(l.debut), 'days');
					}
				});
				displayTheLessonWithTheMostDayPlanified(lessons,longestStrategicLesson,lesson,date);
			}else if(lessons[date].dureeEnSemaines < lesson.module.dureeEnSemaines){
				lessons[date].dureeEnSemaines = lesson.module.dureeEnSemaines;
			}
		}
		/** affichage du cours ayant le plus de jours planifiés lorsque plusieurs cours sont sur la même semaine. */
		function displayTheLessonWithTheMostDayPlanified(lessons,longestStrategicLesson,lesson,date){
			var diffDaysLongestStrategicLesson = moment(longestStrategicLesson.fin).diff(moment(longestStrategicLesson.debut), 'days');
			var diffDaysLesson = moment(lessons[date].fin).diff(moment(lessons[date].debut), 'days');

			if (diffDaysLesson < diffDaysLongestStrategicLesson) {
				var index = lessons[date].otherLessonsDuringWeek.indexOf(longestStrategicLesson);
				if (index > -1) {
					var arrayTemp = lessons[date].otherLessonsDuringWeek;
					arrayTemp.splice(index,1);
					arrayTemp.push(lessons[date]);
					longestStrategicLesson.otherLessonsDuringWeek = arrayTemp;
					lessons[date] = longestStrategicLesson;
				}
			}
		}

		/** Gestion des cours planifiés sur plusieurs semaines. */
		function manageLessonPlanifiedForSeveralWeek(lesson){
			if(lesson.module.dureeEnSemaines > 1) {
				if(!_.contains(vm.planningImpossible,lesson)) {
					var diff = ((moment(lesson.fin).week() - 1) - (moment(lesson.debut).week() - 1));
					var depart = lesson.debut;
					if (moment(lesson.fin).isSame(vm.interval.dateStart, 'weeks')) {
					} else if (moment(lesson.debut).isBefore(vm.interval.dateStart)) {
						depart = vm.interval.dateStart;
					}
					diff = moment(depart).diff(moment(lesson.fin), 'weeks');
					for (var index = diff; index < 0; index++) {
						var startOfWeek = moment(lesson.fin).add(1, 'weeks').startOf('week').format(config.formatGeneric);
						delete vm.row.lessons[moment(startOfWeek).add(index, 'weeks').format(config.formatGeneric)];
					}
				}
			}
		}

		/** Initialisation de l'objet content avec les données temporelles. */
		function initDataForEachElementView(){
			var content = {};
			_.each(vm.weeks,function(week){
				if(week >= vm.weeks[0].number){
					content[week.start] = undefined;
				}
			});
			return content;
		}

		/** DONNEES SECONDAIRES DU PLANNING */

		/**
		 * Récupérer les semaines contenues dans l'interval.
		 * @returns {Array}
		 */
		function getWeeks(){
			vm.weeks = [];

			var dateStart = moment(vm.interval.dateStart);
			var dateEnd = moment(vm.interval.dateEnd);
			var dateEndWeekCurrent = dateStart.clone().add(4,'days');

			vm.weeks.push({
				'number':dateStart.format('WW'),
				'start':dateStart.format(config.formatGeneric),
				'end':dateEndWeekCurrent.format(config.formatGeneric)
			});

			var startDateManipulated = null;
			var endDateManipulated = null;
			var everyWeekAreSold = false;
			var countWeek = 1;
			/** tant que tous les semaines n'ont pas été dépassées, on passe à la suivante.*/
			while(!everyWeekAreSold){
				if(countWeek == 1){
					startDateManipulated = dateStart.add(1,'weeks');
					endDateManipulated = startDateManipulated.clone().add(4,'days');
				}else{
					startDateManipulated = startDateManipulated.clone().add(1,'weeks');
					endDateManipulated = startDateManipulated.clone().add(4,'days').endOf('day');
					if(!endDateManipulated.isBetween(dateStart, dateEnd)
						|| !startDateManipulated.isBetween(dateStart, dateEnd)
						|| endDateManipulated.isSame(dateEnd)){
						everyWeekAreSold = true;
					}
				}
				if(!everyWeekAreSold){
					vm.weeks.push({
						'number':startDateManipulated.format('WW'),
						'start':startDateManipulated.format(config.formatGeneric),
						'end':startDateManipulated.clone().add(4,'days').format(config.formatGeneric)
					});
					countWeek++;
				}
			}
			return vm.weeks;
		}
		/** enlève une partie de la clé lorsque la vue est salle. */
		vm.splitDisplayBy = function(displayBy){
			if($rootScope.data.filtersSelected.viewPlanning === 'salle'){
				displayBy = displayBy.split('-')[1];
			}
			return displayBy;
		};

		/** ALERTES */

		/** Gestion des alertes pour les salles. */
		function manageAlertsSalle(lessonCurrent, alerts, libelleAlerts){
			if(lessonCurrent.salle){
				if(lessonCurrent.salle.disponibilite){
					if(lessonCurrent.salle.capacite < lessonCurrent.nbStagiaire){
						addAlert(alerts,'capacity',lessonCurrent,libelleAlerts.capacity);
					}
					/** à la fois ce booléen doit être à vrai et aucun cours ne doit être liée à cette salle durant la période souhaitée. */
					_.find(vm.lessons, function(l){
						if(l.salle && l.salle.code===lessonCurrent.salle.code
							&& moment(l.debut).week() >= moment(lessonCurrent.debut).week()
							&& moment(l.debut).week() <= moment(lessonCurrent.fin).week()
							&& l.id !== lessonCurrent.id){
							addAlert(alerts,'occuped',l,libelleAlerts.occuped);
						}
					});
				}else{
					addAlert(alerts,'unavailability',lessonCurrent,libelleAlerts.unavailability);
				}

				if(lessonCurrent.module && lessonCurrent.salle.equipements.length > 0){
					if(lessonCurrent.module.equipements.length > 0){
						_.each(lessonCurrent.module.equipements,function(moduleEquipement){
							var equipementFound = _.find(lessonCurrent.salle.equipements,function(salleEquipement){
								return salleEquipement.id === moduleEquipement.id;
							});
							if(!equipementFound){
								addAlert(alerts,'equipements',lessonCurrent,libelleAlerts.equipements);
							}
						});
					}else{
						addAlert(alerts,'equipements',lessonCurrent,libelleAlerts.equipements);
					}
				}
			}
		}

		/** Gestion des alertes pour les compétences. */
		function manageAlertsCompetences(lessonCurrent, alerts, libelleAlerts){
			if(lessonCurrent.module){
				planningService.getExperiencesByFormateurAndModule(lessonCurrent.formateur.idFormateur,lessonCurrent.module.id).then(function(response) {
					if(response.status === 200){
						var experiencesFormateur = response.data;
						_.find($rootScope.data.parametres,function(parametre){
							if(parametre.code === 'PARAM_NB_DISP_EXP' && parametre.configs[0].valeur > experiencesFormateur){
								addAlert(alerts,'beginner',lessonCurrent,libelleAlerts.beginner);
							}
						});
					}
				});

				if(lessonCurrent.module.competences.length > 0){
					if(lessonCurrent.formateur.competences.length > 0){
						_.each(lessonCurrent.module.competences,function(moduleCompetence){
							var competenceFound = _.find(lessonCurrent.formateur.competences,function(formateurCompetence){
								return moduleCompetence.code === formateurCompetence.code && moduleCompetence.libelle === formateurCompetence.libelle;
							});
							if(!competenceFound){
								addAlert(alerts,'missingSkills',moduleCompetence,libelleAlerts.missingSkills);
							}
						});
					}else{
						alerts.push({
							code:'missingSkills',
							module:lessonCurrent.module,
							description: 'Le formateur  n\'a pas de compétences affectées pour le module '
						});
					}
				}
			}
		}

		/** indisponibilité du formateur. */
		function manageUnavailabilityTrainer(lessonsInTermsOfFormateurCurrent,lessonsInTermsOfFormateurCurrentWithFormateurAndSalle , response, alerts,libelleAlerts){
			if(response.status === 200){
				lessonsInTermsOfFormateurCurrent = response.data;
				var alertsUnavailableTrainers = [];
				_.each(lessonsInTermsOfFormateurCurrent,function(lessonToCompare){
					_.find(lessonsInTermsOfFormateurCurrent,function(otherLesson){
						if(lessonToCompare.module.id !== otherLesson.module.id){
							if(moment(otherLesson.debut).week() == moment(lessonToCompare.debut).week()
								|| moment(otherLesson.debut).isBetween(lessonToCompare.debut,lessonToCompare.fin)
								|| moment(otherLesson.debut).week() == moment(lessonToCompare.fin).week()) {
								var findAlert = _.find(alertsUnavailableTrainers,function(alert){
									return alert.module === otherLesson.module.libelleCourt;
								});
								if(!findAlert){
									alertsUnavailableTrainers.push({
										'date': moment(otherLesson.debut).format(config.formatGeneric),
										'module': otherLesson.module.libelleCourt
									});
									addAlert(alerts,'unavailableTrainer',otherLesson,libelleAlerts.unavailableTrainer);
								}
							}
						}
					});
					if(lessonToCompare.salle && lessonToCompare.salle.lieu){
						lessonsInTermsOfFormateurCurrentWithFormateurAndSalle.push(lessonToCompare);
					}
				});
			}
		}

		/** Gestion des alertes concernant les déplacements du formateur. */
		function manageDeplacementAlerts(lessonsInTermsOfFormateurCurrentWithFormateurAndSalle,lessonCurrent,alerts,libelleAlerts){
			if(lessonCurrent.salle && lessonCurrent.salle.lieu) {
				planningService.getDeplacementsFormateur(lessonCurrent.formateur.idFormateur).then(function (response) {
					var deplacements = response.data;

					var successiveDeplacementTolerated = {};

					if($rootScope.data.parametres.length > 0){
						var parameterDeplacementTolerated = _.find($rootScope.data.parametres,function(parametre){
							return parametre.code === 'PARAM_INT_VISION';
						});
						if(parameterDeplacementTolerated !== undefined){
							successiveDeplacementTolerated = parameterDeplacementTolerated.configs[0].valeur;
						}
					}
					var successiveDeplacement = 0;
					var availableWeekEnd = 0;
					var lessonsInTermsOfFormateurCurrentWithFormateurAndSalleFiltered = _.filter(lessonsInTermsOfFormateurCurrentWithFormateurAndSalle,function(lesson){
						return lesson.promotion.code === lessonCurrent.promotion.code;
					});
					_.each(lessonsInTermsOfFormateurCurrentWithFormateurAndSalleFiltered, function (lesson, index) {
						/** on enlève 1 pour gérer la fuseau horaire. */
						if(index > 0){
							if(moment(lesson.debut).week()-1 === availableWeekEnd){
								_.each(deplacements, function (deplacement) {
									if (deplacement && deplacement.lieu && deplacement.lieu.id !== lesson.salle.lieu.id) {
										return false;
									}
								});
								if(successiveDeplacementTolerated){
									successiveDeplacement += (moment(lesson.fin).week() - moment(lesson.debut).week()) + 1;
									if(successiveDeplacement >= successiveDeplacementTolerated){
										addAlert(alerts,'movement',lesson,libelleAlerts.movement);
										return true;
									}
								}
								if(index < (lessonsInTermsOfFormateurCurrentWithFormateurAndSalleFiltered.length - 1)){
									var duringWeekLesson = (moment(lesson.fin).add(-1,'day').week() - moment(lesson.debut).add(-1,'day').week()) + 1;
									availableWeekEnd += duringWeekLesson;
								}
							}else{
								return false;
							}
						}else{
							availableWeekEnd = moment(lesson.debut).week() + (moment(lesson.fin).week() - moment(lesson.debut).week());
							successiveDeplacement = (moment(lesson.fin).week() - moment(lesson.debut).week()+1);
						}
					});
				});
			}
		}
		/** Gestion des alertes. */
		function manageAlerts(lessonCurrent){
			var alerts = [];
			var libelleAlerts = planningService.getAlertesPlanningList();
			manageAlertsSalle(lessonCurrent,alerts, libelleAlerts);
			if(lessonCurrent.formateur){
				if(lessonCurrent.formateur.externe){
					var remainingWeekForConfirmation = _.find($rootScope.data.alertes,function(alerte){
						return alerte.code === 'ALE_CONF_EXT';
					});
					if(remainingWeekForConfirmation && moment(lessonCurrent.debut).diff(moment(),'weeks') < remainingWeekForConfirmation.configs[0].valeur && remainingWeekForConfirmation.configs[0].valeur > 0){
						addAlert(alerts,'externalSpeaker',lessonCurrent,libelleAlerts.externalSpeaker);
					}
				}else{
					var lessonsInTermsOfFormateurCurrent = [];
					var lessonsInTermsOfFormateurCurrentWithFormateurAndSalle = [];
					planningService.getLessonsByFormateur(lessonCurrent.formateur.idFormateur,vm.interval).then(function(response){
						manageUnavailabilityTrainer(lessonsInTermsOfFormateurCurrent,lessonsInTermsOfFormateurCurrentWithFormateurAndSalle, response, alerts,libelleAlerts);
					}).then(function(){
						manageDeplacementAlerts(lessonsInTermsOfFormateurCurrentWithFormateurAndSalle,lessonCurrent,alerts,libelleAlerts);
					});
				}
				manageAlertsCompetences(lessonCurrent, alerts, libelleAlerts);
			}
			return alerts;
		}

		/** méthode d'ajout d'une alerte. */
		function addAlert(alerts,code,lesson,description){
			alerts.push({
				code:code,
				lesson:lesson,
				description: description
			});
		}

		/** UTILS */

		/** Ouverture du menu */
		var originatorEv;
		vm.openMenu = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		/** SIDE NAV */

		vm.toggleRight = buildToggler('right');
		$rootScope.lessonSelected = {};
		vm.isOpenRight = function(){
			return $mdSidenav('right').isOpen();
		};

		vm.setLessonSelected = function(lesson){
			$rootScope.lessonSelected = lesson;
		};

		function buildToggler(navID) {
			return function() {
				$mdSidenav(navID)
					.toggle()
					.then(function () {
					});
			};
		}

		vm.close = function () {
			$mdSidenav('right').close()
				.then(function () {
				});
		};
	}
})();
