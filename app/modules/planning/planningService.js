(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:planningService
	 * @description
	 * # planningService
	 * Service of the app
	 */

  	angular
		.module('planning')
		.factory('planningService', Planning);
		// Inject your dependencies as .$inject = ['$http', 'someSevide'];
		// function Name ($http, someSevide) {...}

		Planning.$inject = ['$http','config','Dao', 'cache','$rootScope'];

		function Planning ($http,config,Dao, cache,$rootScope) {

			var alerts = {
				capacity: 'La capacité de la salle est insuffisante',
				occuped: 'La salle est déjà occupée pour cette période ',
				unavailability: 'La salle est indisponible',
				equipments: 'La salle ne dispose pas des équipements nécessaires pour réaliser le module ',
				externalSpeaker: 'L\'intervenant externe n\'a pas confirmé',
				unavailableTrainer: 'Le formateur n\'est pas disponible pour le module ',
				movement: 'Le formateur a réalisé trop de déplacements',
				beginner: 'Le formateur est débutant sur le module ',
				missingSkills: 'Le formateur n\'a pas les compétences requises pour dispenser le cours '
			};


			/**
			 * Use those functions
			 * to configure
			 * the application
			 */
			function getLessons(interval){
				return Dao.crudData("get", config.apiUrl + "/planning/" + interval.dateStart + "/" + interval.dateEnd);
			}

			function getExperiencesByFormateurAndModule(formateur,module){
				return Dao.crudData("get", config.apiUrl + "/formateur/cours/experiences/" + formateur + "/" + module);
			}

			function getLessonsByFormateur(idFormateur,interval){
				return Dao.crudData("get", config.apiUrl + "/formateur/cours/" + idFormateur + "/" + interval.dateStart + "/" + interval.dateEnd);
			}

			function getDeplacementsFormateur(idFormateur){
				return Dao.crudData("get", config.apiUrl + "/formateur/deplacementsExterne/" + idFormateur);
			}

			function getConges(interval){
				return Dao.crudData("get", config.apiUrl + "/conges/" + interval.dateStart + "/" + interval.dateEnd);
			}

			function getAlertesPlanningList(){
				return alerts;
			}

			/** Récupère la première semaine du mois sélectionné. */
			function getIntervalDate(year,month){
				var dateStart = moment().year(year).month(month).startOf('month').format(config.format);
				var dateEnd = moment(dateStart).add(1,'months').endOf('month').endOf('week').format(config.format);
				if(moment(dateStart).day() == 6){
					dateStart = moment(dateStart).add(2,'days').format(config.format);
					dateEnd = moment(dateStart).add(1,'months').endOf('month').endOf('week').format(config.format);
				}else {
					dateStart = moment(dateStart).startOf('week').format(config.format);
				}
				return {dateStart:dateStart,dateEnd:dateEnd};
			}

			function updateCours(cours){
				return Dao.crudData("put", config.apiUrl + "/cours", cours);
			}

			return {
				getIntervalDate:getIntervalDate,
				updateCours:updateCours,
				getAlertesPlanningList:getAlertesPlanningList,
				getLessons: getLessons,
				getExperiencesByFormateurAndModule: getExperiencesByFormateurAndModule,
				getLessonsByFormateur:getLessonsByFormateur,
				getDeplacementsFormateur:getDeplacementsFormateur,
				getConges: getConges
			};
		}

})();
