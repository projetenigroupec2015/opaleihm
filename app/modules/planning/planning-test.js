(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:planningTest
	 * @description
	 * # planningTest
	 * Test of the app
	 */

	describe('planning test', function () {
		var controller = null, $scope = null;

		beforeEach(function () {
			module('opale');
		});

		beforeEach(inject(function ($controller, $rootScope) {
			$scope = $rootScope.$new();
			controller = $controller('PlanningCtrl', {
				$scope: $scope
			});
		}));

		it('Should controller must be defined', function () {
			expect(controller).toBeDefined();
		});

	});
})();
