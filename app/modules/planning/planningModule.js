(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:planningModule
	 * @description
	 * # planningModule
	 * Module of the app
	 */

	var underscore = angular.module('underscore', []);
	underscore.factory('_', function() {
		return window._; //Underscore must already be loaded on the page
	});

  	angular.module('planning', ['underscore']);

})();
