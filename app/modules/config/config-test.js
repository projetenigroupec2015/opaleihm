(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.test:configTest
	 * @description
	 * # configTest
	 * Test of the app
	 */

	describe('config test', function () {
		var controller = null, $scope = null;

		beforeEach(function () {
			module('opale');
		});

		beforeEach(inject(function ($controller, $rootScope) {
			$scope = $rootScope.$new();
			controller = $controller('ConfigCtrl', {
				$scope: $scope
			});
		}));

		it('Should controller must be defined', function () {
			expect(controller).toBeDefined();
		});

	});
})();
