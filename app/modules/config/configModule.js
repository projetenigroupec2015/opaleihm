(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.module:configModule
	* @description
	* # configModule
	* Module of the app
	*/

	angular.module('config', [])
	.constant('config', {
		appName: 'OPALE',
		description: 'Organisation et Planification Assistées pour Les Entreprises',
		baseUrl: '/',
		apiUrl : 'http://127.0.0.1:8080/v1',
		formatGeneric : 'YYYY-MM-DD',
		formatWeek : 'DD MMM',
		formatDayAndMonth : 'DD-MM',
		format:'YYYY-MM-DDTHH:mm:ss'
	})
	.constant('USER_ROLES', {
		all: '*',
		admin: 'admin',
		resp: 'resp_form',
		form: 'form'
	});
})();
