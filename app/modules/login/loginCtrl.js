(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:loginCtrl
	* @description
	* # loginCtrl
	* Controller of the app
	*/

	angular
	.module('login')
	.controller('LoginCtrl', Login);

	Login.$inject = ['$rootScope', '$scope', 'LoginService', 'Session', '$state', 'Utils', 'authService', '$cookies'];

	function Login($rootScope, $scope, LoginService, Session, $state, Utils, authService, $cookies) {
		var vm = this;

		vm.login = function (credentials) {
			$rootScope.authenticationError = false;
			LoginService.login(credentials)
			.then(function (response, status, headers, config) {
				if(response.status === 200){
					if(response.data === null){
						Utils.viewToast("error", "Echec de la connexion !");
					} else {
						Utils.viewToast("success", "Connexion réussie");
					}
				} else {
					if(response.err === -1){
						Utils.viewToast("error", response.data);
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				}
			});
		};

	}

})();
