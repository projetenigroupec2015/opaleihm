'use strict';

/**
 * @ngdoc function
 * @name app.route:loginRoute
 * @description
 * # loginRoute
 * Route of the app
 */

angular.module('login')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {

		$stateProvider
			.state('login', {
				url:'/login',
				templateUrl: 'app/modules/login/login.html',
				controller: 'LoginCtrl',
				controllerAs: 'vm',
				access : {
					loginRequired: false,
					authorizedRoles: [USER_ROLES.all]
				}
			});


	}]);
