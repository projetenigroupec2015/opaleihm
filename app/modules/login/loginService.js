(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:loginService
	 * @description
	 * # loginService
	 * Service of the app
	 */

	angular
		.module('login')
		.factory('LoginService', Login)
		.factory('Session', Session);

	Login.$inject = ['config', 'Dao', '$http', '$state', '$rootScope', '$resource', 'authService', 'Session', '$cookies'];
	Session.$inject = ['$cookies'];

	function Login(config, Dao, $http, $state, $rootScope, $resource, authService, Session, $cookies) {
		return {
			login: login,
			getAccount: getAccount,
			isAuthorized: isAuthorized,
			logout: logout,
		};

		function login(credentials) {
			var headers = credentials ? {authorization: "Basic " + btoa(credentials.user + ":" + credentials.pass)} : {};
			// return $http.get("app/mock/userAdmin.json", {headers : headers})
			// return $http.get("app/mock/userForm.json", {headers : headers})
			return $http.get(config.apiUrl + '/user/account', {headers: headers})
				.then(function (response, status, headers, config) {
					if(response.status === 200){
						sessionStorage.setItem("auth", btoa(credentials.user + ":" + credentials.pass));
						authService.loginConfirmed(response.data);
						return {status: response.status, data: response.data};
					} else if(response.status === -1){
						return {status: response.status, data: "API indisponible. Vérifiez votre connexion."};
					} else {
						$rootScope.authenticationError = true;
						Session.invalidate();
						return {status: response.status, data: response.data};
					}
				});
		}

		function getAccount() {
			$rootScope.loadingAccount = true;
			// var headers = {authorization : "Basic " + sessionStorage.getItem("auth")};
			if (sessionStorage.getItem("auth")) {
				// $http.get("app/mock/userForm.json")
				// $http.get("app/mock/userAdmin.json")
				$http.get(config.apiUrl + '/user/account')
					.then(function (response) {
						authService.loginConfirmed(response.data);
					});
			} else {
				authService.loginCancelled();
			}
		}

		function isAuthorized(authorizedRoles) {
			if (!angular.isArray(authorizedRoles)) {
				if (authorizedRoles === '*') {
					return true;
				}
				authorizedRoles = [authorizedRoles];
			}
			var isAuthorized = false;
			angular.forEach(authorizedRoles, function (authorizedRole) {
				var authorized = null;
				if (angular.isDefined(Session.userRoles)) {
					authorized = Session.userRoles.libelle;
				}

				if (authorized === authorizedRole || authorizedRole === '*') {
					isAuthorized = true;
				}
			});
			return isAuthorized;
		}

		function logout() {
			$rootScope.authenticationError = false;
			$rootScope.authenticated = false;
			$rootScope.account = null;
			$http.get(config.apiUrl + '/logout');
			Session.invalidate();
			authService.loginCancelled();
		}

	}

	function Session($cookies) {
		var vm = this;
		vm.create = function (data) {
			this.id = data.idFormateur;
			this.login = data.username;
			this.firstName = data.prenom;
			this.lastName = data.nom;
			this.email = data.mail;
			this.userRoles = data.habilitation;
			this.isAdmin = (data.habilitation.libelle === 'admin' ? true : false);
		};
		vm.invalidate = function () {
			this.id = null;
			this.login = null;
			this.firstName = null;
			this.lastName = null;
			this.email = null;
			this.userRoles = null;
			sessionStorage.removeItem("auth");
			$cookies.remove("XSFR-TOKEN");
			$cookies.remove("JSESSIONID");
		};
		return vm;
	}

})();
