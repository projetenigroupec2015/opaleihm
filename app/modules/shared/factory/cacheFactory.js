(function() {
    'use strict';

    angular
        .module('opale')
        .factory('cache', factory);

    factory.$inject = ['$cacheFactory'];

    /* @ngInject */
    function factory($cacheFactory) {

        return $cacheFactory('list');

    }
})();
