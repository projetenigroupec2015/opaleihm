(function() {
  'use strict';

  /**
  * @ngdoc function
  * @name app.factory:utils Factory
  * @description
  * # Utils Factory
  * Factory of the app
  */

  angular
  .module('opale')
  .factory('Dao', Dao);


  Dao.$inject = ['$http', '$cacheFactory'];

  function Dao($http, $cacheFactory) {
    return {
      crudData : crudData
    };

    /**
    * Permet de réaliser les operation du CRUD auprès du WS
    * @param  {[string]} method   [valeur possible : "get", "put", "post", "delete"]
    * @param  {[string]} url      [l'url du ws]
    * @param  {[data]} data     [les donnees à modifier, ajouter ou supprimer]
    * @param  {[string]} cacheKey [la clé du cache pour les get]
    * @return {[data]}          [les données du ws]
    */
    function crudData (method, url, data, headers) {
      switch (method) {
        case "get":
        return getData(url, headers);
        case "put":
        return setData(url, method, data);
        case "post":
        return setData(url, method, data);
        case "delete":
        return setData(url, method, data);
      }
    }

    function getData(url, headers){
      return $http.get(url, {cache: true}, {headers : headers})
      .then(doResponse)
      .catch(error);
    }

    function setData(url, method, data) {
      return $http({
        url: url,
        method: method,
        data: data
      })
      .then(doResponse)
      .catch(error);
    }

    function doResponse(response) {
      var status = response.status;
      var data;
      switch(status) {
        case 200:
        data = response.data;
        break;
        case 401:
        data = 'Action non authorisée !';
        break;
        case 403:
        data = 'Accès interdit !';
        break;
        case 404:
        data = 'La ressource demandée n\'as pas été trouvée.';
        break;
		case 409:
		data = response.data.message;
		break;
        case 500:
        data = 'Une erreur inconnue est survenue sur le serveur !';
        break;
        case 0:
        data = 'Une erreur inconnue est survenue !';
        break;
        case -1:
        data = 'Impossible de se connecter au serveur ! Vérifiez votre connexion.';
        break;
        default:
        data = 'Une erreur inconnue est survenue !';
        break;
      }
      return {status: status, data: data};
    }

    function error(error) {
      var status = error.status;
      var errorMsg;
      switch(status) {
        case 401:
        errorMsg = 'Action non authorisée !';
        break;
        case 403:
        errorMsg = 'Accès interdit !';
        break;
        case 404:
        errorMsg = 'La ressource demandée n\'as pas été trouvée.';
        break;
        case 500:
        errorMsg = 'Une erreur inconnue est survenue sur le serveur !';
        break;
        case 0:
        errorMsg = 'Une erreur inconnue est survenue !';
        break;
        case -1:
        errorMsg = 'Impossible de se connecter au serveur ! Vérifiez votre connexion.';
        break;
        default:
        errorMsg = 'Une erreur inconnue est survenue !';
        break;
      }
      return {err: status, data: errorMsg};
    }
  }

})();
