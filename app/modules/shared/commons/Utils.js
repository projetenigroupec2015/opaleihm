(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.factory:utils Factory
	* @description
	* # Utils Factory
	* Factory of the app
	*/

	angular
	.module('opale')
	.factory('Utils', Utils);


	Utils.$inject = ['atomicNotifyService'];

	function Utils(atomicNotifyService) {

		return {
			/**
			* Affiche un toast parametre
			* @param  {[String]} content [Le texte à afficher]
			*/
			viewToast : function viewToast(type, content){
				switch (type) {
					case "info":
						atomicNotifyService.custom("info", content, "info");
					break;
					case "warn":
						atomicNotifyService.custom("warning", content, "warning");
					break;
					case "error":
						atomicNotifyService.custom("error", content, "error");
					break;
					case "success":
						atomicNotifyService.custom("success", content, "done");
					break;
					case "custom":
						atomicNotifyService.custom("info", content);
					break;
					default:

				}
			},
			/**
			* Déplace un element dans une liste
			* @param  {[Array]} array          [Le tableau à modifier]
			* @param  {[type]} value          [L'element de la liste à deplacer]
			* @param  {[type]} positionChange [Le deplacement : +1, -1, +2, -3...]
			* @return {[Array]}                [Le tableau modifie]
			*/
			moveElementInArray: function moveElementInArray (array, value, positionChange) {
				var oldIndex = array.indexOf(value);
				if (oldIndex > -1){
					var newIndex = (oldIndex + positionChange);

					if (newIndex < 0){
						newIndex = 0;
					} else if (newIndex >= array.length){
						newIndex = array.length;
					}

					var arrayClone = array.slice();
					arrayClone.splice(oldIndex,1);
					arrayClone.splice(newIndex,0,value);

					return arrayClone;
				}
				return array;
			},

			isUndefinedOrNull : function isUndefinedOrNull (val) {
				return angular.isUndefined(val) || val === null;
			}
		};

	}



})();
