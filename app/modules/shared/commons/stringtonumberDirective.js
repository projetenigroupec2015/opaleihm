(function() {
    'use strict';

    angular
        .module('opale')
        .directive('stringToNumber', directive);

    /* @ngInject */
    function directive() {
        var directive = {
            require: 'ngModel',
            restrict: 'A',
            link: linkFunc,
        };

        return directive;

        function linkFunc(scope, el, attr, ngModel) {
          ngModel.$parsers.push(function(value) {
            return '' + value;
          });
          ngModel.$formatters.push(function(value) {
            return parseFloat(value);
          });
        }
    }
})();
