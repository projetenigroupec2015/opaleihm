(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:sidenavplanningDirective
	* @description
	* # sidenavplanningDirective
	* Directive of the app
	*/

	angular
		.module('opale')
		.directive('sideNavPlanning', sideNavPlanning);

		function sideNavPlanning () {

			var directive = {
				link: link,
				restrict: 'EA',
				scope:{
				},
				controller: 'SideNavPlanningCtrl',
				controllerAs: 'vm',
				bindToController:true,
				templateUrl:'app/modules/shared/directives/sidenavplanning/sidenavplanning.html',
			};

			return directive;

			function link(scope, element, attrs) {

			}
		}

})();
