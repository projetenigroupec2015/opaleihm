(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:sidenavplanningCtrl
	* @description
	* # sidenavplanningCtrl
	* Controller of the app
	*/

	angular
		.module('planning')
		.controller('SideNavPlanningCtrl', SideNavPlanning);

		SideNavPlanning.$inject = ['$mdSidenav','$log','homeService','$rootScope','$stateParams','$state','planningService','Utils'];

		function SideNavPlanning($mdSidenav,$log,homeService,$rootScope,$stateParams,$state,planningService,Utils) {

			var vm = this;
			vm.alerts = [];
			vm.salles = [];
			vm.formateurs = [];

			vm.formateurSelected = {};
			vm.salleSelected = {};
			vm.coursSelected = {};

			/** on récupère les salles et formateurs de la base de données,
			 * puis les appels suivants iront les récupérer en cache. */
			$rootScope.$watch('lessonSelected',function(newValue,oldValue){
				vm.formateurSelected = {};
				vm.salleSelected = {};
				vm.lessons = [];
				if(newValue.hasOwnProperty('id') || newValue.hasOwnProperty('idConge')){
					homeService.getFormateurs().then(function(response){
						if(response.status === 200) {
							vm.formateurs = response.data;
							vm.formateurs = _.sortBy(vm.formateurs,function(f){
								return f.nom + ' ' + f.prenom;
							});
						}
					}).then(function(){
						homeService.getSalles().then(function(response){
							if(response.status === 200){
								vm.salles = response.data;
								vm.salles = _.sortBy(vm.salles,function(s){
									return s.lieu.libelle + ' ' + s.code;
								});
							}
						});
					}).then(function(){
						if(newValue.hasOwnProperty('id')){
							vm.title = newValue.module.libelleCourt;
							vm.lessonStart = newValue.debut || newValue.dateDebutConge;
							vm.lessonEnd = newValue.fin || newValue.dateFinConge;
							if(newValue.otherLessonsDuringWeek){
								_.each(newValue.otherLessonsDuringWeek,function(otherLessonsDuringWeek){
									vm.lessons.push(otherLessonsDuringWeek);
								});
								vm.lessons = _.sortBy(vm.lessons,function(lesson){
									return lesson.module.libelleCourt;
								});
							}
							vm.lessons.push(newValue);
							vm.alerts = getAlerts(newValue);
							vm.coursSelected = newValue;
							if(newValue){
								if(newValue.formateur){
									vm.formateurSelected = newValue.formateur.idFormateur;
								}
								if(newValue.salle){
									vm.salleSelected = newValue.salle.code;
								}
							}
						}else{
							_.each(newValue.otherLessonsDuringWeek,function(otherLessonsDuringWeek){
								vm.lessons.push(otherLessonsDuringWeek);
							});
							vm.coursSelected = newValue.otherLessonsDuringWeek[0];
							if(vm.coursSelected.formateur){
								vm.formateurSelected = vm.coursSelected.formateur.idFormateur;
							}
							if(vm.coursSelected.salle){
								vm.salleSelected = vm.coursSelected.salle.code;
							}
						}
					});
				}
			});

			vm.updateLesson = function(){
				var formateur = _.find(vm.formateurs,function(formateur){
					return formateur.idFormateur == vm.formateurSelected;
				});
				var salle = _.find(vm.salles,function(salle){
					return salle.code === vm.salleSelected;
				});
				var cours = _.find(vm.lessons,function(c){
					return c.id = vm.coursSelected;
				});

				if(cours.hasOwnProperty('id')) {
					cours.salle = salle;
					cours.formateur = formateur;
					cours.ecf['corrige'] = vm.ecfSelected;
					delete cours.week;
					delete cours.impossible;
					delete cours.alerts;
					delete cours.otherLessonsDuringWeek;

					planningService.updateCours(cours);
					vm.close();
					$rootScope.lessonSelected = {};

					if ($rootScope.data.filtersSelected.viewPlanning == 'salle'
						|| $rootScope.data.filtersSelected.viewPlanning == 'formateur') {
						$state.go('home.planning', {
							dateStart: $stateParams.dateStart,
							dateEnd: $stateParams.dateEnd
						}, {reload: true});
					}
				}else{
					Utils.viewToast("error", "Le cours n'a pas été sélectionné");
				}
			};

			vm.init = function(){

			};

			/** récupération des alertes pour le cours sélectionné. */
			function getAlerts(lesson){
				var alerts = [];
				if(lesson.alerts){
					_.find(lesson.alerts, function (alert) {
						if(alert.code === 'unavailableTrainer') {
							if (lesson.module.id != alert.lesson.module.id) {
								if ((((moment(alert.lesson.debut).week() - 1) >= lesson.week)
									&& (moment(alert.lesson.fin).week() - 1) <= (moment(lesson.fin).week() - 1))
									|| (moment(alert.lesson.debut).week() - 1) == (moment(lesson.fin).week() - 1)) {
									alerts.push(alert);
								}
							}
						}else {
							alerts.push(alert);
						}
					});
				}
				return alerts;
			}

			vm.close = function () {
				$mdSidenav('right').close()
					.then(function () {
					});
			};
		}
})();
