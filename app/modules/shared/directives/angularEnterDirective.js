(function() {
  'use strict';

  angular
  .module('opale')
  .directive('ngEnter', function () {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if(event.which === 13) {
          scope.$apply(function (){
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      });
    };
  })
  .directive('usernameExist', ['$http', 'config', '$q', function ($http, config, $q) {
    return {
      require: 'ngModel',
      link : function ($scope, element, attrs, ngModel) {
        ngModel.$asyncValidators.usernameExist = function(modelValue, viewValue) {
          var def = $q.defer();

          $http.get(config.apiUrl + '/security/user?username='+ modelValue).then(function (reponse) {
            if(angular.isObject(reponse.data)){
              def.resolve();
            } else {
              def.reject();
            }
          });

          return def.promise;
        };
      }
    };
  }]);
})();
