(function() {
    'use strict';

    angular
        .module('opale')
        .directive('access', access);

    access.$inject = ['LoginService'];

    /* @ngInject */
    function access(LoginService) {
        var directive = {
            restrict: 'A',
            link: linkFunc,
        };

        return directive;

        function linkFunc(scope, el, attr) {
          var roles = attrs.access.split(',');
          if (roles.length > 0) {
              if (LoginService.isAuthorized(roles)) {
                  element.removeClass('hide');
              } else {
                  element.addClass('hide');
              }
          }
        }
    }

})();
