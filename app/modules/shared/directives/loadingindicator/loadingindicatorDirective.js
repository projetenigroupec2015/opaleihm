(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:loadingindicatorDirective
	* @description
	* # loadingindicatorDirective
	* Directive of the app
	*/

	angular
		.module('opale')
		.directive('loadingIndicator', loadingIndicator);

		loadingIndicator.$inject = ['$http', '$rootScope'];

		function loadingIndicator ($http, $rootScope) {

			var directive = {
				restrict: 'E',
				link: link,
				transclude: true,
				scope: {load: "=?"},
				template: '<md-progress-linear md-mode="indeterminate" ng-show="load" class="loading-page md-warn md-hue-3"></md-progress-linear>'
			};

			return directive;

			function link(scope, element, attrs) {
				scope.load = true;
				scope.isLoading = false;

				scope.isLoading = isLoading;

				scope.$watch(scope.isLoading, toggleElement);

				function toggleElement(loading) {
					if (loading) {
						scope.load = true;
					} else {
						scope.load = false;
					}
				}

				function isLoading() {
					return $http.pendingRequests.length > 0;
				}
			}
		}

})();
