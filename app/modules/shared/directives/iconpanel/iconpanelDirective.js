(function() {
    'use strict';

    angular
    .module('opale')
    .directive('iconPanel', iconPanel);

    /* @ngInject */
    function iconPanel() {
        var directive = {
            restrict: 'EA',
            templateUrl: 'app/modules/shared/directives/iconpanel/iconpanel.tpl.html',
            scope: {
                type: '@',
                title: '@',
                message: '@'
            },
            link: linkFunc,
            controller: Controller,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    Controller.$inject = [];

    /* @ngInject */
    function Controller() {
        var vm = this;
        vm.show = false;
        switch (vm.type) {
            case 'info':
                vm.bg = 'blue';
                vm.icon = 'info';
            break;
            case 'warn':
                vm.bg = 'orange';
                vm.icon = 'warning';
            break;
            case 'error':
                vm.bg = 'red';
                vm.icon = 'error';
            break;
            case 'success':
                vm.bg = 'green';
                vm.icon = 'done';
            break;
            default:
        }
    }
})();
