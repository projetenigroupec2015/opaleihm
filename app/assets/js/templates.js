angular.module('opale').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/modules/commons/loading.html',
    "<div layout=\"column\" layout-align=\"center center\" layout-fill class=\"loading-data\">\n" +
    "	<md-progress-circular md-mode=\"indeterminate\" md-diameter=\"128\" class=\"md-warn md-hue-3\"></md-progress-circular>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/home/dashboard.html',
    "<div class=\"md-padding\" layout-fill flex layout-sm=\"column\" ng-controller=\"DashboardCtrl as ctrl\" ng-cloak>\n" +
    "    <div layout=\"row\" layout-fill>\n" +
    "        <md-card class=\"text-center notifications\" flex ng-class=\"overNotif ? 'md-whiteframe-z3' : ''\" ng-mouseover=\"overNotif = true\" ng-mouseleave=\"overNotif = false\">\n" +
    "            <md-card-header class=\"header\">\n" +
    "                <md-card-avatar>\n" +
    "                    <md-icon md-font-set=\"material-icons\" aria-label=\"down\">notifications</md-icon>\n" +
    "                </md-card-avatar>\n" +
    "                <md-card-header-text>\n" +
    "                    <h2 class=\"md-title\" style=\"float: left\">Notifications</h2>\n" +
    "                </md-card-header-text>\n" +
    "            </md-card-header>\n" +
    "            <md-card-content>\n" +
    "                <md-list class=\"md-dense\" flex>\n" +
    "                    <md-list-item layout=\"row\" ng-repeat=\"notif in ctrl.notifications\"  ng-click=\"ctrl.selectNotif(notif)\" md-ink-ripple>\n" +
    "                        <md-icon class=\"md-avatar-icon\" ng-class=\"vm.getNotifTheme(notif.codeAlerte)\">{{vm.getNotifIconName(notif.codeAlerte)}}</md-icon>\n" +
    "                        <span flex></span>\n" +
    "                        <div class=\"md-list-item-text\">\n" +
    "                            <p>{{notif.message}}</p>\n" +
    "                        </div>\n" +
    "                        <md-divider></md-divider>\n" +
    "                    </md-list-item>\n" +
    "                </md-list>\n" +
    "            </md-card-content>\n" +
    "        </md-card>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- <div layout=\"row\">\n" +
    "<md-card class=\"text-center\">\n" +
    "<md-card-content>\n" +
    "<h1>{{ vm.title }}</h1>\n" +
    "<h3>{{ vm.version }}</h3>\n" +
    "</md-card-content>\n" +
    "</md-card>\n" +
    "<md-card class=\"text-center\">\n" +
    "<md-card-content>\n" +
    "<h1>{{ vm.title }}</h1>\n" +
    "<h3>{{ vm.version }}</h3>\n" +
    "</md-card-content>\n" +
    "</md-card>\n" +
    "<md-card class=\"text-center\">\n" +
    "<md-card-content>\n" +
    "<h1>{{ vm.title }}</h1>\n" +
    "<h3>{{ vm.version }}</h3>\n" +
    "</md-card-content>\n" +
    "</md-card>\n" +
    "</div> -->\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/home/main.html',
    "<div layout=\"column\" layout-align=\"center center\" layout-fill class=\"loading-data\" ng-show=\"vm.loading\">\n" +
    "	<md-progress-circular md-mode=\"indeterminate\" md-diameter=\"128\" class=\"md-warn md-hue-3\"></md-progress-circular>\n" +
    "	<h2 class=\"md-accent\">{{vm.loadingText}}</h2>\n" +
    "</div>\n" +
    "<ng-atomic-notify custom-template=\"app/modules/shared/templates/notify-atomic.tpl.html\"></ng-atomic-notify>\n" +
    "<div ng-include=\"'app/modules/home/toolbar.html'\"></div>\n" +
    "<div layout=\"column\" class=\"relative\" layout-fill role=\"main\" ng-cloak>\n" +
    "	<div class=\"ng-pageslide\" id=\"demo-fast\" flex>\n" +
    "		<div layout=\"column\" layout-align=\"start none\" class=\"up\">\n" +
    "			<div flex=\"95\">\n" +
    "				<div ng-include=\"'app/modules/home/mainFilter.html'\" ng-cloak></div>\n" +
    "			</div>\n" +
    "			<div flex=\"5\">\n" +
    "				<div layout=\"row\" layout-align=\"center end\">\n" +
    "					<a id=\"demo-fast-close\" ng-click=\"vm.setFilterAccess();\" ng-show=\"vm.filterAccess\">\n" +
    "						<md-button class=\"md-fab md-mini btn-floating second\" aria-label=\"Fermeture des filtres\">\n" +
    "							<md-icon>keyboard_arrow_up</md-icon>\n" +
    "						</md-button>\n" +
    "					</a>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<div ui-view layout-fill flex></div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/home/mainFilter.html',
    "<div layout=\"row\">\n" +
    "	<div flex=\"15\" layout-align=\"start start\">\n" +
    "		<md-fab-speed-dial class=\"header_filter\" md-direction=\"Right\">\n" +
    "			<md-fab-trigger>\n" +
    "				<md-button aria-label=\"Filtres\" class=\"md-fab md-warn\" disabled>\n" +
    "					<md-icon id=\"icon_header_filter\">sort</md-icon>\n" +
    "				</md-button>\n" +
    "			</md-fab-trigger>\n" +
    "			<md-fab-actions>\n" +
    "				<md-button aria-label=\"Appliquer\" class=\"md-fab md-raised md-mini\" ng-click=\"vm.setFiltersSelected();\">\n" +
    "					<md-icon class=\"icon_header\">create</md-icon>\n" +
    "				</md-button>\n" +
    "			</md-fab-actions>\n" +
    "		</md-fab-speed-dial>\n" +
    "	</div>\n" +
    "\n" +
    "	<div flex=\"85\" class=\"frame-filters\">\n" +
    "		<div layout=\"column\">\n" +
    "			<div layout=\"row\" layout-align=\"center center\">\n" +
    "				<md-input-container flex=\"20\">\n" +
    "					<label class=\"title-select\">Mois à visualiser</label>\n" +
    "					<md-select class=\"select\" ng-model=\"data.filtersSelected.month\">\n" +
    "						<md-option ng-repeat=\"month in vm.months\" value=\"{{month}}\">{{month}}</md-option>\n" +
    "					</md-select>\n" +
    "				</md-input-container>\n" +
    "				<div flex=\"5\" />\n" +
    "\n" +
    "				<md-input-container>\n" +
    "					<label class=\"title-select\">Lieux</label>\n" +
    "					<md-select class=\"select\" ng-model=\"data.filtersSelected.lieux\" multiple>\n" +
    "						<md-option ng-repeat=\"lieu in data.lieux\" value=\"{{lieu}}\">{{lieu}}</md-option>\n" +
    "					</md-select>\n" +
    "				</md-input-container>\n" +
    "				<div flex=\"5\" />\n" +
    "\n" +
    "				<md-input-container flex=\"20\">\n" +
    "					<label class=\"title-select\">Salles</label>\n" +
    "					<md-select class=\"select\" ng-model=\"data.filtersSelected.salles\" multiple>\n" +
    "						<md-optgroup ng-repeat=\"lieuSelected in data.filtersSelected.lieux\" label=\"{{lieuSelected}}\">\n" +
    "							<md-option ng-repeat=\"salle in data.salles | filter:{lieu:{libelle:lieuSelected}}:true\" value=\"{{salle.lieu.libelle}}-{{salle.code}}\">{{salle.code}}</md-option>\n" +
    "						</md-optgroup>\n" +
    "					</md-select>\n" +
    "				</md-input-container>\n" +
    "				<div flex=\"5\" />\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<div layout=\"column\">\n" +
    "			<div layout=\"row\" layout-align=\"center center\">\n" +
    "				<md-input-container flex=\"20\">\n" +
    "					<label class=\"title-select\">Formations</label>\n" +
    "					<md-select class=\"select\" ng-model=\"data.filtersSelected.formations\" multiple>\n" +
    "						<md-option ng-repeat=\"formation in data.formations\" value=\"{{formation}}\">{{formation}}</md-option>\n" +
    "					</md-select>\n" +
    "				</md-input-container>\n" +
    "				<div flex=\"5\"></div>\n" +
    "				<md-input-container flex=\"20\">\n" +
    "					<label class=\"title-select\">Promotions</label>\n" +
    "					<md-select class=\"select\" ng-model=\"data.filtersSelected.promotions\" multiple>\n" +
    "						<md-option ng-repeat=\"promotion in data.promotions\" value=\"{{promotion.code}}\">{{promotion.code}}</md-option>\n" +
    "					</md-select>\n" +
    "				</md-input-container>\n" +
    "				<div flex=\"5\"></div>\n" +
    "				<md-input-container flex=\"20\" ng-if=\"filtersSelected.viewPlanning ==='formateur'\">\n" +
    "					<label class=\"title-select\">Formateurs</label>\n" +
    "					<md-select class=\"select\" ng-model=\"data.filtersSelected.formateurs\" multiple>\n" +
    "						<md-option ng-repeat=\"formateur in data.formateurs\" value=\"{{formateur.nom}} {{formateur.prenom}}\">{{formateur.nom}} {{formateur.prenom}}</md-option>\n" +
    "					</md-select>\n" +
    "				</md-input-container>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/home/toolbar.html',
    "<loading-indicator></loading-indicator>\n" +
    "<md-toolbar class=\"md-whiteframe-4dp\">\n" +
    "	<div class=\"menu md-toolbar-tools\">\n" +
    "		<div flex=\"35\">\n" +
    "			<h1>\n" +
    "				<span>{{vm.title}}</span>\n" +
    "			</h1>\n" +
    "			<p class=\"subtitle\" hide-xs hide-sm >{{vm.subtitle}}</p>\n" +
    "		</div>\n" +
    "		<!-- ng-show=\"vm.showSidePanel\" -->\n" +
    "		<div flex=\"30\" layout=\"column\" layout-align=\"end center\" style=\"height: 100%;\" class=\"down\" >\n" +
    "			<a pageslide=\"top\" href=\"#demo-fast\" ng-click=\"vm.setFilterAccess();\" ng-hide=\"vm.filterAccess\">\n" +
    "				<md-button class=\"md-fab md-mini btn-floating\" aria-label=\"Eat cake\">\n" +
    "					<md-icon>keyboard_arrow_down</md-icon>\n" +
    "				</md-button>\n" +
    "			</a>\n" +
    "		</div>\n" +
    "		<span flex></span>\n" +
    "		<div ng-click=\"vm.syncData()\" ng-show=\"vm.isAuthorized(vm.syncLink.authorizedRoles)\">\n" +
    "			<md-tooltip md-direction=\"bottom\">\n" +
    "				{{vm.syncLink.tooltip}}\n" +
    "			</md-tooltip>\n" +
    "			<md-button class=\"md-icon-button md-acccent\" aria-label=\"{{vm.syncLink.name}}\" ng-show=\"vm.syncLink.icon\">\n" +
    "				<ng-md-icon icon=\"{{vm.syncLink.icon}}\"></ng-md-icon>\n" +
    "			</md-button>\n" +
    "		</div>\n" +
    "\n" +
    "		<div ng-repeat=\"item in vm.menu\" ng-click=\"vm.navigateTo('home.' + item.link)\" ng-show=\"vm.isAuthorized(item.authorizedRoles)\">\n" +
    "			<md-tooltip md-direction=\"bottom\">\n" +
    "				{{item.tooltip}}\n" +
    "			</md-tooltip>\n" +
    "\n" +
    "			<md-button class=\"md-icon-button md-acccent\" aria-label=\"{{item.name}}\" ng-class=\"{'active' : pageCurrent == '{{item.link}}'}\" ng-show=\"item.icon\">\n" +
    "				<ng-md-icon icon=\"{{item.icon}}\"></ng-md-icon>\n" +
    "			</md-button>\n" +
    "		</div>\n" +
    "		<md-menu md-position-mode=\"target-right target\" md-menu-align-target md-offset=\"0 50\">\n" +
    "			<md-button class=\"md-icon-button md-acccent\" aria-label=\"{{vm.notification.name}}\" ng-click=\"vm.openNotif($mdOpenMenu, $event)\">\n" +
    "				<ng-md-icon icon=\"{{vm.notification.icon}}\" md-menu-origin></ng-md-icon>\n" +
    "				<span ng-class=\"vm.notification.name === 'Notifications' ? 'badge badge--overlap' : ''\" data-badge=\"{{vm.nbNotif}}\"></span>\n" +
    "			</md-button>\n" +
    "			<md-menu-content width=\"5\">\n" +
    "				<md-list class=\"md-dense notifications\">\n" +
    "					<md-list-item layout=\"row\" ng-repeat=\"notif in vm.notifications\"  ng-click=\"ctrl.selectNotif(notif)\" md-ink-ripple>\n" +
    "						<md-icon class=\"md-avatar-icon\" ng-class=\"vm.getNotifTheme(notif.codeAlerte)\">{{vm.getNotifIconName(notif.codeAlerte)}}</md-icon>\n" +
    "						<span flex></span>\n" +
    "						<div class=\"md-list-item-text\">\n" +
    "							<p>{{notif.message}}</p>\n" +
    "						</div>\n" +
    "						<md-divider></md-divider>\n" +
    "					</md-list-item>\n" +
    "				</md-list>\n" +
    "			</md-menu-content>\n" +
    "		</md-menu>\n" +
    "		<md-menu md-position-mode=\"target-right target\" md-offset=\"0 50\">\n" +
    "			<md-button class=\"md-icon-button md-acccent\" aria-label=\"{{item.name}}\" ng-click=\"vm.openMenuUser($mdOpenMenu, $event)\">\n" +
    "				<ng-md-icon icon=\"{{vm.userMenu.icon}}\" md-menu-origin></ng-md-icon>\n" +
    "			</md-button>\n" +
    "			<md-menu-content width=\"4\">\n" +
    "				<md-menu-item>\n" +
    "					<md-button ng-click=\"vm.navigateTo('home.profile')\">\n" +
    "						<md-icon>face</md-icon>\n" +
    "						Profil\n" +
    "					</md-button>\n" +
    "				</md-menu-item>\n" +
    "				<md-menu-divider></md-menu-divider>\n" +
    "				<md-menu-item>\n" +
    "					<md-button ng-click=\"vm.logout()\">\n" +
    "						<md-icon>power_settings_new</md-icon>\n" +
    "						Logout\n" +
    "					</md-button>\n" +
    "				</md-menu-item>\n" +
    "			</md-menu-content>\n" +
    "		</md-menu>\n" +
    "	</div>\n" +
    "</md-toolbar>\n"
  );


  $templateCache.put('app/modules/login/login.html',
    "<ng-atomic-notify custom-template=\"app/modules/shared/templates/notify-atomic.tpl.html\"></ng-atomic-notify>\n" +
    "<form name=\"loginForm\" class=\"login\" layout=\"column\" layout-fill layout-align=\"center center\" ng-controller=\"LoginCtrl as ctrl\">\n" +
    "  <md-card class=\"md-whiteframe-z5\">\n" +
    "    <md-card-header>\n" +
    "      <md-card-header-text>\n" +
    "        <h1 class=\"md-title\" style=\"text-align:center\">Connexion</h1>\n" +
    "      </md-card-header-text>\n" +
    "    </md-card-header>\n" +
    "    <md-card-content layout=\"row\" layout-align=\"center center\">\n" +
    "      <md-input-container flex class=\"md-icon-float\">\n" +
    "        <md-icon md-font-set=\"material-icons\" aria-label=\"down\">person</md-icon>\n" +
    "        <label>Identifiant</label>\n" +
    "        <input type=\"text\" name=\"username\" ng-model=\"ctrl.credentials.user\">\n" +
    "      </md-input-container>\n" +
    "      <md-input-container flex class=\"md-icon-float md-block\">\n" +
    "        <md-icon md-font-set=\"material-icons\" aria-label=\"down\">lock</md-icon>\n" +
    "        <label>Mot de passe</label>\n" +
    "        <input type=\"password\" name=\"password\" ng-model=\"ctrl.credentials.pass\" ng-enter=\"ctrl.login(ctrl.credentials)\">\n" +
    "    </md-input-container>\n" +
    "    <md-button class=\"md-raised md-warn md-hue-3 md-icon-button\" ng-enter ng-click=\"ctrl.login(ctrl.credentials)\" ng-disabled=\"!loginForm.username.$valid || !loginForm.password.$valid\">\n" +
    "      <md-icon md-font-set=\"material-icons\">play_arrow</md-icon>\n" +
    "    </md-button>\n" +
    "  </md-card-content>\n" +
    "</md-card>\n" +
    "</form>\n"
  );


  $templateCache.put('app/modules/planning/header-planning.html',
    "<section class=\"ac-container\" layout=\"row\">\n" +
    "	<div class=\"display-filters\" flex>\n" +
    "		<input id=\"ac-1\" name=\"accordion-1\" type=\"checkbox\" />\n" +
    "		<label class=\"background\" for=\"ac-1\">\n" +
    "			<md-button class=\"md-fab md-mini\" disabled aria-label=\"Filtrer\">\n" +
    "				<i class=\"material-icons\">sort</i>\n" +
    "			</md-button>\n" +
    "			Filtres\n" +
    "		</label>\n" +
    "		<article class=\"ac-small\">\n" +
    "			<md-content layout=\"column\">\n" +
    "				<md-tabs md-dynamic-height md-border-bottom>\n" +
    "					<md-tab label=\"Lieux\" flex>\n" +
    "						<md-content class=\"md-padding\">\n" +
    "							<md-chips class=\"custom-chips\" ng-model=\"displayFilters.cities\" readonly=\"true\">\n" +
    "								<md-chip-template ng-hide=\"$chip === 'undefined'\">\n" +
    "									<span><strong>#{{$chip}}</strong></span>\n" +
    "								</md-chip-template>\n" +
    "							</md-chips>\n" +
    "						</md-content>\n" +
    "					</md-tab>\n" +
    "					<md-tab label=\"Formations\" flex>\n" +
    "						<md-content class=\"md-padding\">\n" +
    "							<md-chips class=\"custom-chips\" ng-model=\"displayFilters.formations\" readonly=\"true\">\n" +
    "								<md-chip-template><span><strong>#{{$chip}}</strong></span></md-chip-template>\n" +
    "							</md-chips>\n" +
    "						</md-content>\n" +
    "					</md-tab>\n" +
    "					<md-tab label=\"Promotions\" ng-if=\"data.filtersSelected.viewPlanning ==='promotion'\" flex>\n" +
    "						<md-content class=\"md-padding\">\n" +
    "							<md-chips class=\"custom-chips\" ng-model=\"displayFilters.promotions\" readonly=\"true\">\n" +
    "								<md-chip-template><span><strong>#{{$chip}}</strong></span></md-chip-template>\n" +
    "							</md-chips>\n" +
    "						</md-content>\n" +
    "					</md-tab>\n" +
    "\n" +
    "					<md-tab label=\"Formateurs\" ng-if=\"data.filtersSelected.viewPlanning ==='formateur'\" flex>\n" +
    "						<md-content class=\"md-padding\">\n" +
    "							<md-chips class=\"custom-chips\" ng-model=\"displayFilters.formateurs\" readonly=\"true\">\n" +
    "								<md-chip-template><span><strong>#{{$chip}}</strong></span></md-chip-template>\n" +
    "							</md-chips>\n" +
    "						</md-content>\n" +
    "					</md-tab>\n" +
    "				</md-tabs>\n" +
    "			</md-content>\n" +
    "		</article>\n" +
    "	</div>\n" +
    "	<div flex=\"5\"></div>\n" +
    "	<div class=\"display-filters\" flex>\n" +
    "\n" +
    "		<input id=\"ac-2\" name=\"accordion-1\" type=\"checkbox\" />\n" +
    "		<label class=\"background\" for=\"ac-2\">\n" +
    "			<md-button class=\"md-fab md-mini\" disabled aria-label=\"Affichage\">\n" +
    "				<i class=\"material-icons\">visibility</i>\n" +
    "			</md-button>\n" +
    "			Affichage\n" +
    "		</label>\n" +
    "		<article class=\"ac-small\">\n" +
    "			<md-tabs md-dynamic-height md-border-bottom>\n" +
    "				<md-tab label=\"Vision\" flex>\n" +
    "					<md-content class=\"md-padding\">\n" +
    "						<div class=\"collapsible-body\" layout=\"row\" layout-align=\"center center\">\n" +
    "							<md-button class=\"grpView left\" ng-click=\"vm.setViewPlanning('promotion')\" ng-class=\"{'active':data.filtersSelected.viewPlanning === 'promotion'}\">Promotions</md-button>\n" +
    "							<md-button class=\"grpView middle\" ng-click=\"vm.setViewPlanning('formateur')\" ng-class=\"{'active':data.filtersSelected.viewPlanning === 'formateur'}\">Formateurs</md-button>\n" +
    "							<md-button class=\"grpView right\" ng-click=\"vm.setViewPlanning('salle')\" ng-class=\"{'active':data.filtersSelected.viewPlanning === 'salle'}\">Salles</md-button>\n" +
    "						</div>\n" +
    "					</md-content>\n" +
    "				</md-tab>\n" +
    "				<md-tab label=\"Conflits\" flex>\n" +
    "					<md-content class=\"md-padding\">\n" +
    "						<div class=\"ImpossibleCase\">\n" +
    "							<md-list>\n" +
    "								<md-list-item class=\"secondary-button-padding\" ng-repeat=\"impossibleCase in vm.displayPlanningImpossible\">\n" +
    "									<p>{{impossibleCase.module.libelle}}</p>\n" +
    "									<md-button class=\"md-raised\" ng-click=\"vm.toggleRight();vm.setLessonSelected(impossibleCase)\">Editer</md-button>\n" +
    "								</md-list-item>\n" +
    "							</md-list>\n" +
    "						</div>\n" +
    "					</md-content>\n" +
    "				</md-tab>\n" +
    "			</md-tabs>\n" +
    "		</article>\n" +
    "	</div>\n" +
    "</section>\n"
  );


  $templateCache.put('app/modules/planning/lesson.html',
    "<div class=\"header-content\">\n" +
    "	<div layout=\"row\" layout-align=\"space-between start\">\n" +
    "		<div class=\"number_classroom\">\n" +
    "			<span class=\"count_module trainees\">{{lesson.nbStagiaire}}</span>\n" +
    "			{{lesson.salle.code}}\n" +
    "		</div>\n" +
    "		<div side-nav-planning></div>\n" +
    "	</div>\n" +
    "</div>\n" +
    "<div class=\"body-content\">\n" +
    "	<span class=\"title\">{{lesson.libelle}}</span>\n" +
    "	<i class=\"material-icons statut_formateur\">school</i>\n" +
    "</div>\n" +
    "<div layout=\"row\" layout-align=\"space-between start\" class=\"footer-content\">\n" +
    "	<div ng-class=\"{'content-formateur': lesson.formateur}\">\n" +
    "		<md-icon class=\"person-icon\">person</md-icon>\n" +
    "		<span class=\"username\" ng-if=\"lesson.module.dureeEnSemaines === 1 && lesson.formateur.nom.length >= 6\">\n" +
    "			{{lesson.formateur.nom + ' ' +  lesson.formateur.prenom.charAt(0)|uppercase | limitTo:7}}...\n" +
    "		</span>\n" +
    "		<span class=\"username\" ng-if=\"lesson.module.dureeEnSemaines > 1 || lesson.formateur.nom.length <= 6 && content.module.dureeEnSemaines === 1\">\n" +
    "			{{lesson.formateur.nom + ' ' +  lesson.formateur.prenom.charAt(0)|uppercase}}\n" +
    "		</span>\n" +
    "		<md-tooltip md-direction=\"bottom\">\n" +
    "			{{lesson.formateur.nom}} {{lesson.formateur.prenom}}\n" +
    "		</md-tooltip>\n" +
    "	</div>\n" +
    "\n" +
    "	<div ng-if=\"lesson.otherLessonsDuringWeek.length\">\n" +
    "		<md-menu md-position-mode=\"target-right target\" md-offset=\"0 50\">\n" +
    "			<a ng-href=\"#\" ng-click=\"vm.openMenu($mdOpenMenu, $event)\" class=\"count_module remaining\">\n" +
    "				+{{lesson.otherLessonsDuringWeek.length}}\n" +
    "			</a>\n" +
    "			<md-menu-content width=\"4\" class=\"menu_content_lesson_multiple\">\n" +
    "				<md-menu-item class=\"header\">\n" +
    "					<span class=\"title\">Autres cours dispensés</span>\n" +
    "				</md-menu-item>\n" +
    "				<md-menu-item ng-repeat=\"lessonMultiple in lesson.otherLessonsDuringWeek\" class=\"item\">\n" +
    "					<span class=\"lessonMultiple\" flex=\"50\">{{lessonMultiple.module.libelleCourt}}</span>\n" +
    "					<div class=\"lessonMultiple_date\" flex>\n" +
    "						<span ng-init=\"dateStart = vm.convertDate(lessonMultiple.debut)\">{{dateStart}}</span>\n" +
    "						<span ng-init=\"dateEnd = vm.convertDate(lessonMultiple.fin)\">/ {{dateEnd}}</span>\n" +
    "					</div>\n" +
    "				</md-menu-item>\n" +
    "			</md-menu-content>\n" +
    "		</md-menu>\n" +
    "	</div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/planning/planning.html',
    "<div class=\"md-padding\" flex layout-sm=\"column\" ng-cloak>\n" +
    "	<div ng-include=\"'/app/modules/planning/header-planning.html'\"></div>\n" +
    "\n" +
    "	<div ng-if=\"!vm.displayPlanning\">\n" +
    "		<icon-panel type=\"info\" title=\"Aucune données n'a été trouvée.\"></icon-panel>\n" +
    "	</div>\n" +
    "\n" +
    "	<div side-nav-planning lesson=\"vm.lessonSelected\"></div>\n" +
    "\n" +
    "	<div ng-if=\"vm.displayPlanning\">\n" +
    "		<content-months-planning></content-months-planning>\n" +
    "		<table class=\"lesson\">\n" +
    "			<thead>\n" +
    "			<tr class=\"weeks\">\n" +
    "				<th class=\"hide_th lieu\"></th>\n" +
    "				<th class=\"hide_th promotion\"></th>\n" +
    "				<th class=\"week\" ng-repeat=\"week in vm.getWeeks\">\n" +
    "					<span class=\"number-week\">Semaine {{week.number}}</span><br/> <span class=\"date-week\">{{week.start}} - {{week.end}}</span>\n" +
    "				</th>\n" +
    "			</tr>\n" +
    "			</thead>\n" +
    "			<tbody>\n" +
    "				<tr class=\"module\" ng-repeat=\"data in vm.contentMonths\">\n" +
    "					<td class=\"lieu_module\" ng-init=\"applyRowspan = vm.isRowspanApplied(data.city)\" ng-show=\"applyRowspan\" rowspan=\"{{vm.rowspan.cities[data.city].length}}\">\n" +
    "						<p ng-hide=\"data.city === 'undefined'\">{{data.city}}</p>\n" +
    "					</td>\n" +
    "					<td class=\"promotion\" ng-init=\"displayBySplit = vm.splitDisplayBy(data.displayBy)\">{{displayBySplit}}</td>\n" +
    "					<td class=\"content {{lesson.facturation|lowercase}}\" ng-repeat=\"lesson in data.lessons\" colspan=\"{{lesson.module.dureeEnSemaines || lesson.dureeEnSemaines}}\">\n" +
    "						<div ng-if=\"lesson\" layout=\"column\" layout-align=\"space-around none\" flex>\n" +
    "							<div class=\"lesson\" ng-if=\"lesson.libelle\">\n" +
    "								<div class=\"header-content\">\n" +
    "									<div layout=\"row\" layout-align=\"space-between start\">\n" +
    "										<div class=\"number_classroom\">\n" +
    "											<span class=\"count_module trainees\">{{lesson.nbStagiaire}}</span>\n" +
    "											{{lesson.salle.code}}\n" +
    "										</div>\n" +
    "										<a ng-href=\"\" ng-click=\"vm.toggleRight();vm.setLessonSelected(lesson)\" ng-class=\"{'active' : vm.isOpenRight() == true}\">\n" +
    "											<i class=\"material-icons md-15 update_module\">create</i>\n" +
    "										</a>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "								<div class=\"body-content\">\n" +
    "									<span class=\"title\">{{lesson.libelle}}</span>\n" +
    "									<i class=\"material-icons statut_formateur\" ng-if=\"lesson.ecf\">school</i>\n" +
    "									<i class=\"material-icons statut_formateur\" ng-if=\"lesson.ecf.corrige\">done</i>\n" +
    "								</div>\n" +
    "								<div class=\"footer-content\" layout=\"row\" layout-align=\"space-between start\">\n" +
    "									<div ng-class=\"{'content-formateur': lesson.formateur}\">\n" +
    "										<md-icon class=\"person-icon\">person</md-icon>\n" +
    "										<span class=\"username\" ng-if=\"lesson.module.dureeEnSemaines === 1 && lesson.formateur.nom.length >= 6\">\n" +
    "											{{lesson.formateur.nom + ' ' +  lesson.formateur.prenom.charAt(0)|uppercase | limitTo:7}}...\n" +
    "										</span>\n" +
    "										<span class=\"username\" ng-if=\"lesson.module.dureeEnSemaines > 1 || lesson.formateur.nom.length <= 6 && content.module.dureeEnSemaines === 1\">\n" +
    "											{{lesson.formateur.nom + ' ' +  lesson.formateur.prenom.charAt(0)|uppercase}}\n" +
    "										</span>\n" +
    "										<md-tooltip md-direction=\"bottom\" ng-if=\"lesson.formateur.nom\">\n" +
    "											{{lesson.formateur.nom}} {{lesson.formateur.prenom}}\n" +
    "										</md-tooltip>\n" +
    "									</div>\n" +
    "\n" +
    "									<div ng-if=\"lesson.otherLessonsDuringWeek.length\">\n" +
    "										<md-menu md-position-mode=\"target-right target\" md-offset=\"0 50\">\n" +
    "											<a ng-href=\"#\" ng-click=\"vm.openMenu($mdOpenMenu, $event)\" class=\"count_module remaining\">\n" +
    "												+{{lesson.otherLessonsDuringWeek.length}}\n" +
    "											</a>\n" +
    "											<md-menu-content width=\"4\" class=\"menu_content_lesson_multiple\">\n" +
    "												<md-menu-item class=\"header\">\n" +
    "													<span class=\"title\">Autres cours dispensés</span>\n" +
    "												</md-menu-item>\n" +
    "												<md-menu-item ng-repeat=\"lessonMultiple in lesson.otherLessonsDuringWeek\" class=\"item\">\n" +
    "													<span class=\"lessonMultiple\" flex=\"50\">{{lessonMultiple.module.libelleCourt}}</span>\n" +
    "													<div class=\"lessonMultiple_date\" flex>\n" +
    "														<span>{{lessonMultiple.debut | date :'dd-MM-yyyy'}} / {{lessonMultiple.fin | date :'dd-MM-yyyy'}}</span>\n" +
    "													</div>\n" +
    "												</md-menu-item>\n" +
    "											</md-menu-content>\n" +
    "										</md-menu>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "							<div class=\"conge\" ng-if=\"lesson.etatConge\">\n" +
    "								<div class=\"header-content\">\n" +
    "									<div layout=\"row\" layout-align=\"space-between start\" flex-offset=\"90\">\n" +
    "										<a ng-href=\"\" ng-click=\"vm.toggleRight()\" ng-class=\"{'active' : vm.isOpenRight() == true}\">\n" +
    "											<i class=\"material-icons md-15 update_module\">create</i>\n" +
    "										</a>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "								<div class=\"body-content\">\n" +
    "									<md-icon>schedule</md-icon>\n" +
    "									<span class=\"title\">{{lesson.etatConge}}</span>\n" +
    "								</div>\n" +
    "								<div class=\"footer-content\" layout=\"row\" layout-align=\"space-between start\" flex-offset=\"90\">\n" +
    "									<div ng-if=\"lesson.otherLessonsDuringWeek.length\">\n" +
    "										<md-menu md-position-mode=\"target-right target\" md-offset=\"0 50\">\n" +
    "											<a ng-href=\"#\" ng-click=\"vm.openMenu($mdOpenMenu, $event)\" class=\"count_module remaining\">\n" +
    "												+{{lesson.otherLessonsDuringWeek.length}}\n" +
    "											</a>\n" +
    "											<md-menu-content width=\"4\" class=\"menu_content_lesson_multiple\">\n" +
    "												<md-menu-item class=\"header\">\n" +
    "													<span class=\"title\">Autres cours dispensés</span>\n" +
    "												</md-menu-item>\n" +
    "												<md-menu-item ng-repeat=\"lessonMultiple in lesson.otherLessonsDuringWeek\" class=\"item\">\n" +
    "													<span class=\"lessonMultiple\" flex=\"50\">{{lessonMultiple.module.libelleCourt}}</span>\n" +
    "													<div class=\"lessonMultiple_date\" flex>\n" +
    "														<span ng-init=\"dateStart = lessonMultiple.debut\">{{dateStart | date:'yyyy-MM-dd'}}</span>\n" +
    "														<span ng-init=\"dateEnd = lessonMultiple.fin\">/ {{dateEnd | date:'yyyy-MM-dd'}}</span>\n" +
    "													</div>\n" +
    "												</md-menu-item>\n" +
    "											</md-menu-content>\n" +
    "										</md-menu>\n" +
    "									</div>\n" +
    "								</div>\n" +
    "							</div>\n" +
    "						</div>\n" +
    "					</td>\n" +
    "				</tr>\n" +
    "			</tbody>\n" +
    "		</table>\n" +
    "	</div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/profile/profile.html',
    "<div class=\"md-padding\" flex layout-sm=\"column\">\n" +
    "    <md-card class=\"text-center profile\" flex ng-class=\"countNotif ? 'md-whiteframe-z3' : ''\">\n" +
    "        <md-card-header class=\"header\">\n" +
    "            <md-card-avatar>\n" +
    "                <md-icon md-font-set=\"material-icons\" aria-label=\"down\">person</md-icon>\n" +
    "            </md-card-avatar>\n" +
    "            <md-card-header-text>\n" +
    "                <h2 class=\"md-title\" style=\"float: left\">Profil de l'utilisateur: {{vm.user.firstName}} {{vm.user.lastName}}</h2>\n" +
    "            </md-card-header-text>\n" +
    "        </md-card-header>\n" +
    "        <md-card-content layout=\"row\">\n" +
    "            <md-card class=\"text-center\" flex ng-class=\"countNotif ? 'md-whiteframe-z3' : ''\">\n" +
    "                <md-card-header class=\"\">\n" +
    "                    <md-card-avatar>\n" +
    "                        <md-icon md-font-set=\"material-icons\" aria-label=\"down\">info</md-icon>\n" +
    "                    </md-card-avatar>\n" +
    "                    <md-card-header-text>\n" +
    "                        <h2 class=\"md-title\" style=\"float: left\">Informations</h2>\n" +
    "                    </md-card-header-text>\n" +
    "                </md-card-header>\n" +
    "                <md-card-content>\n" +
    "                    <md-list class=\"md-dense\" flex>\n" +
    "                        <md-list-item class=\"md-2-line\">\n" +
    "                            <div class=\"md-list-item-text\">\n" +
    "                                <p>ID : <span ng-bind=\"vm.user.id\"></span></p>\n" +
    "                            </div>\n" +
    "                        </md-list-item>\n" +
    "                        <md-divider></md-divider>\n" +
    "                        <md-list-item class=\"md-2-line\">\n" +
    "                            <div class=\"md-list-item-text\">\n" +
    "                                <p>Nom : <span ng-bind=\"vm.user.firstName\"></span></p>\n" +
    "                            </div>\n" +
    "                        </md-list-item>\n" +
    "                        <md-divider></md-divider>\n" +
    "                        <md-list-item class=\"md-2-line\">\n" +
    "                            <div class=\"md-list-item-text\">\n" +
    "                                <p>Prénom : <span ng-bind=\"vm.user.lastName\"></span></p>\n" +
    "                            </div>\n" +
    "                        </md-list-item>\n" +
    "                        <md-divider></md-divider>\n" +
    "                        <md-list-item class=\"md-2-line\">\n" +
    "                            <div class=\"md-list-item-text\">\n" +
    "                                <p>E-mail : <span ng-bind=\"vm.user.email\"></span></p>\n" +
    "                            </div>\n" +
    "                        </md-list-item>\n" +
    "                        <md-divider></md-divider>\n" +
    "                        <md-list-item class=\"md-2-line\">\n" +
    "                            <div class=\"md-list-item-text\">\n" +
    "                                <p>Identifiant : <span ng-bind=\"vm.user.login\"></span></p>\n" +
    "                            </div>\n" +
    "                        </md-list-item>\n" +
    "                    </md-list>\n" +
    "                </md-card-content>\n" +
    "            </md-card>\n" +
    "            <md-card class=\"text-center \" flex ng-class=\"countNotif ? 'md-whiteframe-z3' : ''\">\n" +
    "                <md-card-header class=\"\">\n" +
    "                    <md-card-avatar>\n" +
    "                        <md-icon md-font-set=\"material-icons\" aria-label=\"down\">lock</md-icon>\n" +
    "                    </md-card-avatar>\n" +
    "                    <md-card-header-text>\n" +
    "                        <h2 class=\"md-title\" style=\"float: left\">Habilitation</h2>\n" +
    "                    </md-card-header-text>\n" +
    "                </md-card-header>\n" +
    "                <md-card-content>\n" +
    "                    <md-list class=\"md-dense\" flex>\n" +
    "                        <md-list-item class=\"md-2-line\">\n" +
    "                            <div class=\"md-list-item-text\">\n" +
    "                                <p><span ng-bind=\"vm.getHumanRole(vm.user.userRoles.libelle) | uppercase\"></span></p>\n" +
    "                            </div>\n" +
    "                        </md-list-item>\n" +
    "                    </md-list>\n" +
    "                </md-card-content>\n" +
    "            </md-card>\n" +
    "        </md-card-content>\n" +
    "    </md-card>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/settings/partial/affectation.tmpl.html',
    "<div role=\"dialog\" aria-label=\"Affectation\" layout=\"column\" layout-align=\"center center\">\n" +
    "  <md-toolbar>\n" +
    "    <div class=\"md-toolbar-tools\">\n" +
    "      <h2>Nouvelle affectation de site</h2>\n" +
    "    </div>\n" +
    "  </md-toolbar>\n" +
    "  <form>\n" +
    "    <div>\n" +
    "      <p ng-bind=\"identite\"></p>\n" +
    "      <md-select ng-model=\"newAffectation.lieu\" placeholder=\"Lieu\" class=\"md-no-underline\" required md-no-asterisk=\"false\" flex>\n" +
    "        <md-option value=\"{{lieu.id}}\" ng-repeat=\"lieu in lieux track by lieu.id\">{{lieu.libelle}}</md-option>\n" +
    "      </md-select>\n" +
    "      <md-checkbox ng-model=\"newAffectation.default\" aria-label=\"default\" ng-disabled=\"disableDefault\">Defaut</md-checkbox>\n" +
    "      <md-select ng-model=\"newAffectation.preference\" placeholder=\"Lieu\" class=\"md-no-underline\" ng-attr-disabled=\"{{ newAffectation.default === true  ? 'disabled' : undefined }}\" required flex>\n" +
    "        <md-option value=\"1\">1</md-option>\n" +
    "        <md-option value=\"2\">2</md-option>\n" +
    "        <md-option value=\"3\">3</md-option>\n" +
    "      </md-select>\n" +
    "    </div>\n" +
    "\n" +
    "    <div layout=\"row\">\n" +
    "      <md-button md-autofocus flex class=\"md-raised md-primary\" ng-click=\"closeDialog()\" ng-disabled=\"!newAffectation\">\n" +
    "        Valider\n" +
    "      </md-button>\n" +
    "    </div>\n" +
    "  </form>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/settings/partial/alertes.part.html',
    "<div layout=\"column\" layout-fill ng-controller=\"AlertesCtrl as ctrl\">\n" +
    "    <form name=\"alerteConfigForm\" class=\"configuration\" >\n" +
    "        <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "            <h2 class=\"md-toolbar-tools \" ncy-breadcrumb></h2>\n" +
    "            <md-button class=\"md-raised md-warn md-hue-3 md-icon-button\" ng-click=\"ctrl.save(ctrl.alertes)\"><md-icon md-font-set=\"material-icons\">done</md-icon></md-button>\n" +
    "        </md-toolbar>\n" +
    "        <div class=\"md-padding\">\n" +
    "            <md-list>\n" +
    "                <md-list-item class=\"md-2-line\" ng-repeat=\"alerte in ctrl.alertes\">\n" +
    "                    <div class=\"md-list-item-text\">\n" +
    "                        <label flex=\"90\" for=\"{{alerte.code}}\">{{alerte.libelle}}</label>\n" +
    "                        <md-input-container class=\"md-secondary\" flex ng-repeat=\"config in alerte.configs\">\n" +
    "                            <label>Valeur</label>\n" +
    "                            <input type=\"number\" required id=\"{{alerte.code}}\" string-to-number name=\"{{alerte.code}}\" ng-model=\"config.valeur\" aria-label=\"Alerte value\" /> <!--{{alerte.code}}-->\n" +
    "                            <div class=\"hint\">{{config.unite}}</div>\n" +
    "                            <div ng-messages=\"alerteConfigForm.{{alerte.code}}.$error\">\n" +
    "                                <div ng-message=\"required\">Le champs est obligatoire.</div>\n" +
    "                            </div>\n" +
    "                        </md-input-container>\n" +
    "                        <md-divider></md-divider>\n" +
    "                    </div>\n" +
    "                </md-list-item>\n" +
    "            </md-list>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/settings/partial/apidocs.part.html',
    "<style>\n" +
    "    .swagger-validator {\n" +
    "        display: none;\n" +
    "    }\n" +
    "</style>\n" +
    "<div ng-controller=\"ApiDocCtrl as ctrl\">\n" +
    "    <h3 ng-show=\"ctrl.isLoading\">loading ...</h3>\n" +
    "    <div layout-fill flex swagger-ui url=\"ctrl.url\" loading=\"isLoading\" parser=\"json\" api-explorer=\"true\" trusted-sources=\"true\"\n" +
    "         error-handler=\"ctrl.myErrorHandler\" template-url=\"app/modules/shared/templates/apidoc/apidoc.tpl.html\">\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/settings/partial/competences.tmpl.html',
    "<div role=\"dialog\" aria-label=\"Eat me!\" layout=\"column\" layout-align=\"center center\">\n" +
    "  <md-toolbar>\n" +
    "    <div class=\"md-toolbar-tools\">\n" +
    "      <h2>Créer une compétence</h2>\n" +
    "    </div>\n" +
    "  </md-toolbar>\n" +
    "  <form>\n" +
    "    <div>\n" +
    "      <md-input-container flex>\n" +
    "        <label>Code</label>\n" +
    "        <input type=\"text\" ng-model=\"newCompetence.code\" maxlength=\"20\" required>\n" +
    "      </md-input-container>\n" +
    "      <md-input-container flex>\n" +
    "        <label>Libelle</label>\n" +
    "        <input type=\"text\" ng-model=\"newCompetence.libelle\" maxlength=\"50\" required>\n" +
    "      </md-input-container>\n" +
    "    </div>\n" +
    "\n" +
    "    <div layout=\"row\">\n" +
    "      <md-button md-autofocus flex class=\"md-primary\" ng-click=\"closeDialog()\" ng-disabled=\"!newCompetence\">\n" +
    "        Valider\n" +
    "      </md-button>\n" +
    "    </div>\n" +
    "  </form>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/settings/partial/formateurs.part.html',
    "<form name=\"formateursConfigForm\" layout=\"column\" layout-fill class=\"configuration\"  style=\"overflow: auto; height: 100%;\" ng-controller=\"FormateursCtrl as ctrl\" >\n" +
    "        <div>\n" +
    "                <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "                        <h2 class=\"md-toolbar-tools\" ncy-breadcrumb></h2>\n" +
    "                </md-toolbar>\n" +
    "        </div>\n" +
    "        <div layout=\"row\">\n" +
    "                <div layout=\"column\" flex=\"70\">\n" +
    "                        <md-toolbar class=\"toolbar-secondary\" layout=\"row\" layout-align=\"end center\">\n" +
    "                                <p class=\"md-toolbar-tools\">Affectation de compétences</p>\n" +
    "                        </md-toolbar>\n" +
    "                        <div layout=\"row\">\n" +
    "                                <div layout=\"column\" class=\"md-padding\" flex=\"35\">\n" +
    "                                        <md-autocomplete\n" +
    "                                        md-input-name=\"inputFormateur\"\n" +
    "                                        md-no-cache=\"true\"\n" +
    "                                        md-selected-item=\"ctrl.selectedFormateur\"\n" +
    "                                        md-search-text=\"ctrl.searchFormateur\"\n" +
    "                                        md-search-text-change=\"ctrl.searchFormateurChange(ctrl.searchFormateur)\"\n" +
    "                                        md-items=\"formateur in ctrl.querySearch(ctrl.searchFormateur, 'formateur')\"\n" +
    "                                        md-item-text=\"formateur.nom\"\n" +
    "                                        md-floating-label=\"Rechercher un formateur\">\n" +
    "                                        <md-item-template>\n" +
    "                                                <span md-highlight-text=\"ctrl.searchFormateur\">{{formateur.nom}} {{formateur.prenom}}</span>\n" +
    "                                        </md-item-template>\n" +
    "                                </md-autocomplete>\n" +
    "                                <md-content>\n" +
    "                                        <md-list>\n" +
    "                                                <md-list-item class=\"md-2-line\" ng-repeat=\"formateur in ctrl.formateurs | orderBy : 'nom' track by formateur.idFormateur\" ng-click=\"ctrl.selectFormateur(formateur)\">\n" +
    "                                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">person</md-icon>\n" +
    "                                                        <div class=\"md-list-item-text\">\n" +
    "                                                                <p>{{formateur.nom}} {{formateur.prenom}}</p>\n" +
    "                                                        </div>\n" +
    "                                                        <md-divider></md-divider>\n" +
    "                                                </md-list-item>\n" +
    "                                        </md-list>\n" +
    "                                </md-content>\n" +
    "                        </div>\n" +
    "                        <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                                <md-autocomplete\n" +
    "                                md-input-name=\"inputCompetence\"\n" +
    "                                md-no-cache=\"true\"\n" +
    "                                md-selected-item=\"ctrl.selectedCompetence\"\n" +
    "                                md-selected-item-change=\"ctrl.selectedCompetenceChange(competence)\"\n" +
    "                                md-search-text=\"ctrl.searchCompetence\"\n" +
    "                                md-search-text-change=\"ctrl.searchCompetenceChange(ctrl.searchCompetence)\"\n" +
    "                                md-items=\"competence in ctrl.querySearch(ctrl.searchCompetence, 'competence')\"\n" +
    "                                md-item-text=\"competence.libelle\"\n" +
    "                                md-floating-label=\"Ajouter une compétence\">\n" +
    "                                <md-item-template>\n" +
    "                                        <span md-highlight-text=\"ctrl.searchCompetence\">{{competence.code | uppercase}} - {{competence.libelle | uppercase}}</span>\n" +
    "                                </md-item-template>\n" +
    "                        </md-autocomplete>\n" +
    "                        <md-content>\n" +
    "                                <md-list ng-cloak>\n" +
    "                                        <md-list-item class=\"md-2-line\" ng-repeat=\"competence in ctrl.competencesFormateur\">\n" +
    "                                                <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">class</md-icon>\n" +
    "                                                <div class=\"md-list-item-text\">\n" +
    "                                                        <p><span class=\"md-body-2\" ng-bind=\"competence.code | uppercase\"></span> - <span class=\"md-body-2\" ng-bind=\"competence.libelle | uppercase\"></span></p>\n" +
    "                                                        <md-button class=\"md-secondary\" ng-click=\"ctrl.removeCompetence(competence)\">Enlever</md-button>\n" +
    "                                                </div>\n" +
    "                                        </md-list-item>\n" +
    "                                </md-list>\n" +
    "                        </md-content>\n" +
    "                </div>\n" +
    "        </div>\n" +
    "\n" +
    "</div>\n" +
    "<div layout=\"column\" flex=\"30\" class=\"md-whiteframe-z2\">\n" +
    "        <md-toolbar class=\"toolbar-secondary\" layout=\"row\" layout-align=\"end center\">\n" +
    "                <p class=\"md-toolbar-tools\">Affectation de sites</p>\n" +
    "                <md-button  class=\"md-raised md-fab md-mini md-warn md-hue-3\" ng-click=\"ctrl.newAffectation()\"><md-icon md-font-set=\"material-icons\" aria-label=\"down\">add</md-icon></md-button>\n" +
    "        </md-toolbar>\n" +
    "        <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                <md-content flex>\n" +
    "                        <md-list>\n" +
    "                                <md-list-item class=\"md-2-line\" ng-repeat=\"affectation in ctrl.affectationsLieux\" ng-class=\"ctrl.isDefault(affectation) ? 'is-first' : '' \" ng-click=\"ctrl.newAffectation(affectation)\">\n" +
    "                                        <div class=\"md-list-item-text\">\n" +
    "                                                <p>{{affectation.lieu.libelle}}</p>\n" +
    "                                                <md-button class=\"md-secondary md-fab md-mini md-raised\" ng-click=\"ctrl.removeAffectation(competence)\"><md-icon md-font-set=\"material-icons\" aria-label=\"down\">remove</md-icon></md-button>\n" +
    "                                        </div>\n" +
    "                                </md-list-item>\n" +
    "                        </md-list>\n" +
    "                </md-content>\n" +
    "        </div>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "</form>\n"
  );


  $templateCache.put('app/modules/settings/partial/habilitations.part.html',
    "<form name=\"habiliationsConfigForm\" class=\"configuration\" flex layout=\"column\" style=\"overflow: auto; height: 100%;\">\n" +
    "        <div layout-fill ng-controller=\"HabilitationsCtrl as ctrl\">\n" +
    "                <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "                        <h2 class=\"md-toolbar-tools\" ncy-breadcrumb></h2>\n" +
    "                        <!-- <md-button class=\"md-raised md-warn md-hue-3 md-icon-button\" ng-disabled=\"alerteConfigForm.param.$pristine\" ng-click=\"vm.save(vm.alertes)\"><md-icon md-font-set=\"material-icons\">done</md-icon></md-button> -->\n" +
    "                </md-toolbar>\n" +
    "                <div layout=\"row\">\n" +
    "                        <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                                <div layout=\"column\" class=\"md-padding\" flex=\"35\">\n" +
    "                                        <md-autocomplete\n" +
    "                                        md-input-name=\"inputFormateur\"\n" +
    "                                        md-no-cache=\"true\"\n" +
    "                                        md-selected-item=\"ctrl.formateurSelected\"\n" +
    "                                        md-search-text=\"ctrl.searchFormateur\"\n" +
    "                                        md-search-text-change=\"ctrl.searchFormateurChange(ctrl.searchFormateur)\"\n" +
    "                                        md-items=\"formateur in ctrl.querySearch(ctrl.searchFormateur)\"\n" +
    "                                        md-item-text=\"formateur.nom\"\n" +
    "                                        md-floating-label=\"Rechercher un formateur\">\n" +
    "                                        <md-item-template>\n" +
    "                                                <span md-highlight-text=\"ctrl.searchFormateur\">{{formateur.nom}} {{formateur.prenom}}</span>\n" +
    "                                        </md-item-template>\n" +
    "                                </md-autocomplete>\n" +
    "                                <md-content>\n" +
    "                                        <md-list>\n" +
    "                                                <md-list-item class=\"md-2-line\" ng-repeat=\"formateur in ctrl.formateurs | orderBy : 'nom'\" ng-click=\"ctrl.selectFormateur(formateur)\">\n" +
    "                                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">person</md-icon>\n" +
    "                                                        <div class=\"md-list-item-text\">\n" +
    "                                                                <p>{{formateur.nom}} {{formateur.prenom}}</p>\n" +
    "                                                        </div>\n" +
    "                                                        <md-divider></md-divider>\n" +
    "                                                </md-list-item>\n" +
    "                                        </md-list>\n" +
    "                                </md-content>\n" +
    "                        </div>\n" +
    "                        </div>\n" +
    "                        <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                            <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "                                    <p class=\"md-toolbar-tools\">Habilitations</p>\n" +
    "                            </md-toolbar>\n" +
    "                                <md-content flex>\n" +
    "                                        <md-list>\n" +
    "                                                <md-list-item class=\"md-2-line\" ng-class=\"ctrl.formateurSelected.habilitation.libelle === habilitation.libelle ? 'is-first' : ''\" ng-repeat=\"habilitation in ctrl.habilitations\" ng-click=\"ctrl.setHabilitation(habilitation)\">\n" +
    "                                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">desktop_windows</md-icon>\n" +
    "                                                        <div class=\"md-list-item-text\">\n" +
    "                                                                <p ng-bind=\"ctrl.getHumanRole(habilitation.libelle) | uppercase\"></p>\n" +
    "                                                        </div>\n" +
    "                                                </md-list-item>\n" +
    "                                        </md-list>\n" +
    "                                </md-content>\n" +
    "                        </div>\n" +
    "                </div>\n" +
    "\n" +
    "        </div>\n" +
    "</form>\n"
  );


  $templateCache.put('app/modules/settings/partial/modules.part.html',
    "<form name=\"modulesConfigForm\" layout=\"column\" layout-fill class=\"configuration\"  style=\"overflow: auto; height: 100%;\" ng-controller=\"ModulesCtrl as ctrl\">\n" +
    "        <div>\n" +
    "                <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "                        <h2 class=\"md-toolbar-tools\" ncy-breadcrumb></h2>\n" +
    "                        <md-button  class=\"md-raised md-warn md-hue-3\" ng-click=\"ctrl.newCompetence($event)\">Créer une compétence</md-button>\n" +
    "                </md-toolbar>\n" +
    "\n" +
    "        </div>\n" +
    "        <div layout=\"row\">\n" +
    "                <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                        <md-autocomplete\n" +
    "                        md-input-name=\"inputModule\"\n" +
    "                        md-no-cache=\"true\"\n" +
    "                        md-selected-item=\"ctrl.selectedModule\"\n" +
    "                        md-selected-item-change=\"ctrl.getModulesSpecs(module)\"\n" +
    "                        md-search-text=\"ctrl.searchModule\"\n" +
    "                        md-search-text-change=\"ctrl.searchModuleChange(ctrl.searchModule)\"\n" +
    "                        md-items=\"module in ctrl.querySearch(ctrl.searchModule, 'module')\"\n" +
    "                        md-item-text=\"module.libelle\"\n" +
    "                        md-floating-label=\"Rechercher un module\">\n" +
    "                        <md-item-template>\n" +
    "                                <span md-highlight-text=\"ctrl.searchModule\">{{module.libelleCourt}} - {{module.libelle}}</span>\n" +
    "                        </md-item-template>\n" +
    "                </md-autocomplete>\n" +
    "                <md-content>\n" +
    "                        <md-list ng-cloak>\n" +
    "                                <md-list-item class=\"md-2-line\" ng-repeat=\"module in ctrl.modules\" ng-click=\"ctrl.getModulesSpecs(module)\">\n" +
    "                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">view_module</md-icon>\n" +
    "                                        <div class=\"md-list-item-text\">\n" +
    "                                                <p>{{module.libelleCourt}} - {{module.libelle}}</p>\n" +
    "                                        </div>\n" +
    "                                </md-list-item>\n" +
    "                        </md-list>\n" +
    "                </md-content>\n" +
    "        </div>\n" +
    "        <div layout=\"column\" flex ng-show=\"ctrl.selectedModule\">\n" +
    "                <md-card class=\"notifications\" flex>\n" +
    "                        <md-card-header class=\"header\">\n" +
    "                                <md-card-avatar>\n" +
    "                                        <md-icon md-font-set=\"material-icons\" aria-label=\"down\">swap_horiz</md-icon>\n" +
    "                                </md-card-avatar>\n" +
    "                                <md-card-header-text>\n" +
    "                                        <md-autocomplete\n" +
    "                                        md-input-name=\"inputCompetence\"\n" +
    "                                        md-no-cache=\"true\"\n" +
    "                                        md-selected-item=\"ctrl.selectedCompetence\"\n" +
    "                                        md-selected-item-change=\"ctrl.selectedCompetenceChange(competence)\"\n" +
    "                                        md-search-text=\"ctrl.searchCompetence\"\n" +
    "                                        md-search-text-change=\"ctrl.searchCompetenceChange(ctrl.searchCompetence)\"\n" +
    "                                        md-items=\"competence in ctrl.querySearch(ctrl.searchCompetence, 'competence')\"\n" +
    "                                        md-item-text=\"competence.libelle\"\n" +
    "                                        md-floating-label=\"Ajouter une compétence\">\n" +
    "                                        <md-item-template>\n" +
    "                                                <span md-highlight-text=\"ctrl.searchCompetence\">{{competence.code | uppercase}} - {{competence.libelle | uppercase}}</span>\n" +
    "                                        </md-item-template>\n" +
    "                                </md-autocomplete>\n" +
    "                                </md-card-header-text>\n" +
    "                        </md-card-header>\n" +
    "                        <md-card-content layout=\"row\" >\n" +
    "                                <md-content flex>\n" +
    "                                        <md-list class=\"md-dense\">\n" +
    "                                                <md-list-item class=\"md-2-line\" ng-repeat=\"competence in ctrl.competencesModule\">\n" +
    "                                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">class</md-icon>\n" +
    "                                                        <div class=\"md-list-item-text\">\n" +
    "                                                                <p><span class=\"md-body-2\" ng-bind=\"competence.code | uppercase\"></span> - <span class=\"md-body-2\" ng-bind=\"competence.libelle | uppercase\"></span></p>\n" +
    "                                                                <md-button class=\"md-secondary\" ng-click=\"ctrl.removeCompetence(competence)\">Enlever</md-button>\n" +
    "                                                        </div>\n" +
    "                                                </md-list-item>\n" +
    "                                        </md-list>\n" +
    "                                </md-content>\n" +
    "                        </md-card-content>\n" +
    "                </md-card>\n" +
    "                <md-card class=\"notifications\" flex>\n" +
    "                        <md-card-header class=\"header\">\n" +
    "                                <md-card-avatar>\n" +
    "                                        <md-icon md-font-set=\"material-icons\" aria-label=\"down\">swap_horiz</md-icon>\n" +
    "                                </md-card-avatar>\n" +
    "                                <md-card-header-text>\n" +
    "                                        <md-autocomplete\n" +
    "                                        md-input-name=\"inputEquipement\"\n" +
    "                                        md-no-cache=\"true\"\n" +
    "                                        md-selected-item=\"ctrl.selectedEquipement\"\n" +
    "                                        md-selected-item-change=\"ctrl.selectedEquipementChange(equipement)\"\n" +
    "                                        md-search-text=\"ctrl.searchEquipement\"\n" +
    "                                        md-search-text-change=\"ctrl.searchEquipementChange(ctrl.searchEquipement)\"\n" +
    "                                        md-items=\"equipement in ctrl.querySearch(ctrl.searchEquipement, 'equipement')\"\n" +
    "                                        md-item-text=\"equipement.modele\"\n" +
    "                                        md-floating-label=\"Ajouter un équipement\">\n" +
    "                                        <md-item-template>\n" +
    "                                                <span md-highlight-text=\"ctrl.searchEquipement\">{{equipement.lot}} - {{equipement.modele}}</span>\n" +
    "                                        </md-item-template>\n" +
    "                                </md-autocomplete>\n" +
    "                                </md-card-header-text>\n" +
    "                        </md-card-header>\n" +
    "                        <md-card-content layout=\"row\" >\n" +
    "                                <md-content flex>\n" +
    "                                        <md-list class=\"md-dense\">\n" +
    "                                                <md-list-item class=\"md-2-line\" ng-repeat=\"equipement in ctrl.equipementsModule\">\n" +
    "                                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">desktop_windows</md-icon>\n" +
    "                                                        <div class=\"md-list-item-text\">\n" +
    "                                                                <p><span class=\"md-body-2\">Lot:</span> {{equipement.lot}} <span class=\"md-body-2\">Modèle:</span> {{equipement.modele}}</p>\n" +
    "                                                                <md-button class=\"md-secondary\" ng-click=\"ctrl.removeEquipement(equipement)\">Enlever</md-button>\n" +
    "                                                        </div>\n" +
    "                                                </md-list-item>\n" +
    "                                        </md-list>\n" +
    "                                </md-content>\n" +
    "                        </md-card-content>\n" +
    "                </md-card>\n" +
    "        </div>\n" +
    "</div>\n" +
    "</form>\n"
  );


  $templateCache.put('app/modules/settings/partial/params.part.html',
    "<div layout=\"column\" layout-fill ng-controller=\"ParametresCtrl as ctrl\">\n" +
    "    <form name=\"paramConfigForm\" class=\"configuration\" >\n" +
    "        <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "            <h2 class=\"md-toolbar-tools\" ncy-breadcrumb></h2>\n" +
    "            <md-button class=\"md-raised md-warn md-hue-3 md-icon-button\" ng-click=\"ctrl.save(ctrl.params)\"><md-icon md-font-set=\"material-icons\">done</md-icon></md-button>\n" +
    "        </md-toolbar>\n" +
    "        <div class=\"md-padding\">\n" +
    "            <md-list>\n" +
    "                <md-list-item class=\"md-2-line\" ng-repeat=\"params in ctrl.params\">\n" +
    "                    <div class=\"md-list-item-text\">\n" +
    "                        <label flex=\"90\" for=\"{{params.code}}\">{{params.libelle}}</label>\n" +
    "                        <md-input-container class=\"md-secondary\" flex ng-repeat=\"config in params.configs\">\n" +
    "                            <label>Valeur</label>\n" +
    "                            <input type=\"number\" required id=\"{{params.code}}\" string-to-number name=\"{{params.code}}\" ng-model=\"config.valeur\" aria-label=\"Alerte value\" /> <!--{{alerte.code}}-->\n" +
    "                            <div class=\"hint\">{{config.unite}}</div>\n" +
    "                            <div ng-messages=\"paramConfigForm.{{params.code}}.$error\">\n" +
    "                                <div ng-message=\"required\">Le champs est obligatoire.</div>\n" +
    "                            </div>\n" +
    "                        </md-input-container>\n" +
    "                        <md-divider></md-divider>\n" +
    "                    </div>\n" +
    "                </md-list-item>\n" +
    "            </md-list>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/settings/partial/pisteAudit.part.html',
    "<div layout=\"column\" layout-fill ng-controller=\"PisteAuditCtrl as ctrl\">\n" +
    "    <form name=\"alerteConfigForm\" class=\"configuration\" >\n" +
    "        <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "            <h2 class=\"md-toolbar-tools \" ncy-breadcrumb></h2>\n" +
    "        </md-toolbar>\n" +
    "        <md-content class=\"md-padding\">\n" +
    "            <md-table-container>\n" +
    "        		<table md-table>\n" +
    "        			<thead md-head>\n" +
    "        			<tr md-row>\n" +
    "                        <th md-column md-order-by=\"date\" md-desc class=\"md-column ng-isolate-scope md-numeric md-sort\"><span>Date</span></th>\n" +
    "        				<th md-column class=\"md-column ng-isolate-scope md-numeric md-sort\"><span>Utilisateur</span></th>\n" +
    "        				<th md-column class=\"md-column ng-isolate-scope md-numeric md-sort md-active\"><span>Action</span></th>\n" +
    "        			</tr>\n" +
    "        			</thead>\n" +
    "        			<tbody md-body>\n" +
    "        			<tr md-row ng-repeat=\"trace in ctrl.pisteAudit | limitTo: ctrl.query.limit: (ctrl.query.page -1) * ctrl.query.limit\">\n" +
    "        				<md-content>\n" +
    "                        <td md-cell>{{trace.date | date : 'dd/MM/yyyy HH:mm'}}</td>\n" +
    "        				<td md-cell>{{trace.user}}</td>\n" +
    "        				<td md-cell>{{trace.action}}</td>\n" +
    "        				</md-content>\n" +
    "        			</tr>\n" +
    "        			</tbody>\n" +
    "        		</table>\n" +
    "        	</md-table-container>\n" +
    "        	<md-table-pagination ng-show=\"ctrl.hasPisteAudit\" md-limit=\"ctrl.query.limit\" md-page=\"ctrl.query.page\" md-limit-options=\"[5, 10]\" md-total=\"{{ctrl.pisteAudit.length}}\"  md-page-select></md-table-pagination>\n" +
    "        </md-content>\n" +
    "    </form>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/settings/partial/promotions.part.html',
    "<form name=\"promotionsConfigForm\" class=\"configuration\" layout-fill layout=\"column\" style=\"overflow: auto; height: 100%;\" ng-controller=\"PromotionsCtrl as ctrl\">\n" +
    "        <div>\n" +
    "                <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "                        <h2 class=\"md-toolbar-tools\" ncy-breadcrumb></h2>\n" +
    "                </md-toolbar>\n" +
    "        </div>\n" +
    "        <div layout=\"row\">\n" +
    "                <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                        <div layout=\"row\">\n" +
    "                                <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                                        <md-autocomplete\n" +
    "                                        md-input-name=\"inputPromotion\"\n" +
    "                                        md-no-cache=\"true\"\n" +
    "                                        md-selected-item=\"ctrl.promotionSelected\"\n" +
    "                                        md-search-text=\"ctrl.searchPromotion\"\n" +
    "                                        md-search-text-change=\"ctrl.searchPromotionChange(ctrl.searchPromotion)\"\n" +
    "                                        md-items=\"promotion in ctrl.querySearch(ctrl.searchPromotion)\"\n" +
    "                                        md-item-text=\"promotion.code\"\n" +
    "                                        md-floating-label=\"Rechercher une promotion\">\n" +
    "                                        <md-item-template>\n" +
    "                                                <span md-highlight-text=\"ctrl.searchPromotion\">{{promotion.code}}</span>\n" +
    "                                        </md-item-template>\n" +
    "                                </md-autocomplete>\n" +
    "                                <md-content>\n" +
    "                                        <md-list>\n" +
    "                                                <md-list-item class=\"md-2-line\" ng-repeat=\"promotion in ctrl.promotions\" ng-click=\"ctrl.selectPromotion(promotion)\">\n" +
    "                                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">folder_shared</md-icon>\n" +
    "                                                        <div class=\"md-list-item-text\">\n" +
    "                                                                <p>{{promotion.code}}</p>\n" +
    "                                                        </div>\n" +
    "                                                        <md-divider></md-divider>\n" +
    "                                                </md-list-item>\n" +
    "                                        </md-list>\n" +
    "                                </md-content>\n" +
    "                        </div>\n" +
    "                </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div layout=\"column\" class=\"md-padding\" flex>\n" +
    "                <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "                        <p class=\"md-toolbar-tools\">Lieux</p>\n" +
    "                </md-toolbar>\n" +
    "                <md-content flex>\n" +
    "                        <md-list>\n" +
    "                                <md-list-item class=\"md-2-line\" ng-class=\"ctrl.promotionSelected.lieu.libelle === lieu.libelle ? 'is-first' : ''\" ng-repeat=\"lieu in ctrl.lieux\" ng-click=\"ctrl.setLieu(lieu)\">\n" +
    "                                        <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">place</md-icon>\n" +
    "                                        <div class=\"md-list-item-text\">\n" +
    "                                                <p ng-bind=\"lieu.libelle | uppercase\"></p>\n" +
    "                                        </div>\n" +
    "                                </md-list-item>\n" +
    "                        </md-list>\n" +
    "                </md-content>\n" +
    "        </div>\n" +
    "</div>\n" +
    "</form>\n"
  );


  $templateCache.put('app/modules/settings/partial/salles.part.html',
    "<form name=\"sallesConfigForm\" layout=\"column\" layout-fill class=\"configuration\" flex style=\"overflow: auto; height: 100%;\" ng-controller=\"SallesCtrl as ctrl\">\n" +
    "        <div layout=\"column\" >\n" +
    "                        <md-toolbar layout=\"row\" layout-align=\"end center\">\n" +
    "                                <h2 class=\"md-toolbar-tools\" ncy-breadcrumb></h2>\n" +
    "                                <md-button class=\"md-raised md-warn md-hue-3 md-icon-button\" ng-disabled=\"!ctrl.isMaterielSelected\" ng-click=\"ctrl.save(ctrl.materielSelected,ctrl.isNew)\">\n" +
    "                                        <md-icon md-font-set=\"material-icons\">done</md-icon>\n" +
    "                                </md-button>\n" +
    "                        </md-toolbar>\n" +
    "                        <div layout=\"row\">\n" +
    "                                <div layout=\"column\" flex=\"25\" md-whiteframe=\"2\">\n" +
    "                                        <md-select ng-model=\"vm.selectedLieu\" aria-label=\"lieu\" placeholder=\"Selectionner un lieu\">\n" +
    "                                                <md-option ng-value=\"lieu\"  ng-repeat=\"lieu in ctrl.lieux\" ng-click=\"ctrl.selectSalles(lieu.libelle)\">{{ lieu.libelle }}</md-option>\n" +
    "                                        </md-select>\n" +
    "                                        <md-content flex>\n" +
    "                                                <md-list class=\"menu-salles\">\n" +
    "                                                        <md-list-item class=\"md-2-line\" ng-repeat=\"salle in ctrl.salles\" ng-click=\"ctrl.selectMateriels(salle)\" md-ink-ripple>\n" +
    "                                                                {{salle.libelle}}\n" +
    "                                                        </md-list-item>\n" +
    "                                                </md-list>\n" +
    "                                        </md-content>\n" +
    "                                </div>\n" +
    "                                <div flex=\"75\" layout=\"column\">\n" +
    "                                        <md-toolbar class=\"toolbar-secondary\" layout=\"row\" layout-align=\"end center\">\n" +
    "                                                <h3 class=\"md-toolbar-tools\">{{ctrl.contentTitle}}</h3>\n" +
    "                                                <md-button class=\"md-raised md-primary md-hue-2 md-icon-button\" ng-click=\"ctrl.addMateriel()\">\n" +
    "                                                        <md-icon md-font-set=\"material-icons\">add</md-icon>\n" +
    "                                                </md-button>\n" +
    "                                        </md-toolbar>\n" +
    "                                        <md-toolbar ng-show=\"ctrl.isSalleSelected\" md-colors=\"::{background: 'grey-200'}\" class=\"toolbar-secondary md-whiteframe-z2\" layout=\"row\" layout-align=\"end center\">\n" +
    "                                                <h3 class=\"md-toolbar-tools\">Rendre la salle indisponible</h3>\n" +
    "                                                <md-switch ng-model=\"ctrl.selectedSalle.disponibilite\" class=\"md-warn md-hue-2\" aria-label=\"Rendre indisponible\" ng-change=\"ctrl.isDisponible(ctrl.selectedSalle, ctrl.selectedSalle.disponibilite)\"></md-switch>\n" +
    "                                        </md-toolbar>\n" +
    "                                                <md-list ng-hide=\"ctrl.isMaterielSelected\">\n" +
    "                                                        <md-list-item class=\"md-3-line\" ng-repeat=\"materiel in ctrl.materiels\" ng-click=\"ctrl.selectMateriel(materiel)\">\n" +
    "                                                                <md-icon md-font-set=\"material-icons\" class=\"md-avatar\">desktop_windows</md-icon>\n" +
    "                                                                <div class=\"md-list-item-text\">\n" +
    "                                                                        <p><span class=\"md-body-2\">Lot:</span> {{materiel.lot}} <span class=\"md-body-2\">Modèle:</span> {{materiel.modele}} <span class=\"md-body-2\">Fin de garantie:</span> {{materiel.finGarantie | amDateFormat:'DD MMMM YYYY'}}</p>\n" +
    "                                                                        <p><span class=\"md-body-2\">Type:</span> {{materiel.type}} <span class=\"md-body-2\">Disque:</span> {{materiel.disque}} <span class=\"md-body-2\">Dernier nettoyage:</span> {{materiel.dernierNettoyage | amDateFormat:'DD MMMM YYYY'}}</p>\n" +
    "                                                                        <md-button class=\"md-secondary md-raised md-warn\" ng-click=\"ctrl.deleteEquipement(materiel)\">Supprimer</md-button>\n" +
    "                                                                </div>\n" +
    "                                                                <md-divider></md-divider>\n" +
    "                                                        </md-list-item>\n" +
    "                                                </md-list>\n" +
    "                                                <md-content class=\"md-inline-form md-padding\" layout=\"column\" ng-hide=\"!ctrl.isMaterielSelected\">\n" +
    "                                                        <md-card>\n" +
    "                                                                <md-card-header md-colors=\"::{background: 'blue-100'}\">\n" +
    "                                                                        <md-card-avatar>\n" +
    "                                                                                <md-icon md-font-set=\"material-icons\" aria-label=\"down\">info_outline</md-icon>\n" +
    "                                                                        </md-card-avatar>\n" +
    "                                                                        <md-card-header-text>\n" +
    "                                                                                <h2 class=\"md-title\" style=\"float: left\">Informations générales</h2>\n" +
    "                                                                        </md-card-header-text>\n" +
    "                                                                </md-card-header>\n" +
    "                                                                <md-card-content layout=\"row\" >\n" +
    "                                                                        <md-input-container flex>\n" +
    "                                                                                <label>Lot</label>\n" +
    "                                                                                <input type=\"text\" ng-model=\"ctrl.materielSelected.lot\" maxlength=\"4\">\n" +
    "                                                                        </md-input-container>\n" +
    "                                                                        <md-input-container flex>\n" +
    "                                                                                <label>Type</label>\n" +
    "                                                                                <input type=\"text\" ng-model=\"ctrl.materielSelected.type\" maxlength=\"30\">\n" +
    "                                                                        </md-input-container>\n" +
    "                                                                        <md-input-container flex>\n" +
    "                                                                                <label>Modèle</label>\n" +
    "                                                                                <input type=\"text\" ng-model=\"ctrl.materielSelected.modele\" maxlength=\"50\">\n" +
    "                                                                        </md-input-container>\n" +
    "                                                                </md-card-content>\n" +
    "                                                        </md-card>\n" +
    "                                                        <md-card>\n" +
    "                                                                <md-card-header md-colors=\"::{background: 'green-100'}\">\n" +
    "                                                                        <md-card-avatar>\n" +
    "                                                                                <md-icon md-font-set=\"material-icons\" aria-label=\"down\">desktop_windows</md-icon>\n" +
    "                                                                        </md-card-avatar>\n" +
    "                                                                        <md-card-header-text>\n" +
    "                                                                                <h2 class=\"md-title\" style=\"float: left\">Informations techniques</h2>\n" +
    "                                                                        </md-card-header-text>\n" +
    "                                                                </md-card-header>\n" +
    "                                                                <md-card-content layout=\"row\" >\n" +
    "                                                                        <md-input-container flex>\n" +
    "                                                                                <label>CPU</label>\n" +
    "                                                                                <input type=\"text\" ng-model=\"ctrl.materielSelected.cpu\" maxlength=\"15\">\n" +
    "                                                                        </md-input-container>\n" +
    "                                                                        <md-input-container flex>\n" +
    "                                                                                <label>RAM</label>\n" +
    "                                                                                <input type=\"text\" ng-model=\"ctrl.materielSelected.ram\" maxlength=\"3\">\n" +
    "                                                                        </md-input-container>\n" +
    "                                                                        <md-input-container flex>\n" +
    "                                                                                <label>Disque</label>\n" +
    "                                                                                <input type=\"text\" ng-model=\"ctrl.materielSelected.disque\" maxlength=\"50\">\n" +
    "                                                                        </md-input-container>\n" +
    "                                                                </md-card-content>\n" +
    "                                                        </md-card>\n" +
    "                                                        <md-card>\n" +
    "                                                                <md-card-header md-colors=\"::{background: 'blue-grey-100'}\">\n" +
    "                                                                        <md-card-avatar>\n" +
    "                                                                                <md-icon md-font-set=\"material-icons\" aria-label=\"down\">more_horiz</md-icon>\n" +
    "                                                                        </md-card-avatar>\n" +
    "                                                                        <md-card-header-text>\n" +
    "                                                                                <h2 class=\"md-title\" style=\"float: left\">Informations complémentaires</h2>\n" +
    "                                                                        </md-card-header-text>\n" +
    "                                                                </md-card-header>\n" +
    "                                                                <md-card-content layout=\"row\" >\n" +
    "                                                                                <label>Fin de garantie</label>\n" +
    "                                                                                <md-datepicker ng-model=\"ctrl.materielSelected.finGarantie\" md-placeholder=\"Fin de garantie\"></md-datepicker>\n" +
    "                                                                                <label>Dernier nettoyage</label>\n" +
    "                                                                                <md-datepicker ng-model=\"ctrl.materielSelected.dernierNettoyage\" md-placeholder=\"Dernier nettoyage\"></md-datepicker>\n" +
    "                                                                </md-card-content>\n" +
    "                                                        </md-card>\n" +
    "                                                </md-content>\n" +
    "\n" +
    "\n" +
    "                                        </div>\n" +
    "                                </div>\n" +
    "                        </div>\n" +
    "                </form>\n"
  );


  $templateCache.put('app/modules/settings/settings.html',
    "<div layout=\"row\" flex layout-fill class=\"settings\"  ng-cloak>\n" +
    "    <md-button class=\"md-raised md-accent md-icon-button md-nav\" layout=\"column\" ng-hide=\"vm.isOpenLeft()\" hide-gt-md ng-click=\"vm.toggleSidenav()\"><md-icon md-font-set=\"material-icons\">chevron_right</md-icon></md-button>\n" +
    "    <md-sidenav class=\"md-sidenav-left md-whiteframe-z2\" md-component-id=\"left\" md-is-locked-open=\"$mdMedia('gt-md')\">\n" +
    "        <md-toolbar class=\"md-theme-indigo\">\n" +
    "            <h1 class=\"md-toolbar-tools header\">{{vm.init.pageTitle}}</h1>\n" +
    "        </md-toolbar>\n" +
    "        <div ng-controller=\"SidenavCtrl as side\" flex>\n" +
    "            <md-tabs md-selected=\"selectedIndexInteger\" md-stretch-tabs=\"always\" layout-fill>\n" +
    "                <md-tab label=\"{{itemTab.name}}\"  md-on-select=\"\" md-on-deselect=\"\" ng-repeat=\"itemTab in side.tabMenu\">\n" +
    "                    <md-tab-label>\n" +
    "                        {{itemTab.name}}\n" +
    "                    </md-tab-label>\n" +
    "                    <md-tab-body>\n" +
    "                        <md-list class=\"menu-settings\" >\n" +
    "                            <md-list-item ng-repeat=\"item in itemTab.items\" ng-click=\"side.navigateTo(item.url); vm.getData(item.url)\" >\n" +
    "                                <p>{{item.name}}</p>\n" +
    "                                <md-icon class=\"md-secondary\" md-font-set=\"material-icons\">play_arrow</md-icon>\n" +
    "                            </md-list-item>\n" +
    "                        </md-list>\n" +
    "                    </md-tab-body>\n" +
    "                </md-tab>\n" +
    "            </md-tabs>\n" +
    "        </div>\n" +
    "    </md-sidenav>\n" +
    "\n" +
    "    <div ui-view layout=\"column\" flex>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/shared/directives/iconpanel/iconpanel.tpl.html',
    "<md-card md-colors=\"::{background: '{{vm.bg}}-700'}\" class=\"md-whiteframe-z3\">\n" +
    "  <md-card-header ng-click=\"vm.show = !vm.show\" style=\"cursor: pointer\">\n" +
    "    <md-card-avatar>\n" +
    "      <md-icon md-font-set=\"material-icons\" aria-label=\"down\">{{vm.icon}}</md-icon>\n" +
    "    </md-card-avatar>\n" +
    "    <md-card-header-text>\n" +
    "      <h2 class=\"md-title\" style=\"float: left\">{{vm.title}}</h2>\n" +
    "    </md-card-header-text>\n" +
    "  </md-card-header>\n" +
    "  <md-card-content ng-show=\"vm.show\" md-colors=\"::{background: '{{vm.bg}}-400'}\">\n" +
    "    <p style=\"text-align: justify\">{{vm.message}}</p>\n" +
    "  </md-card-content>\n" +
    "</md-card>\n"
  );


  $templateCache.put('app/modules/shared/directives/sidenavplanning/sidenavplanning.html',
    "<md-sidenav class=\"md-sidenav-right md-whiteframe-4dp\"\n" +
    "				md-disable-backdrop\n" +
    "				md-component-id=\"right\">\n" +
    "	<md-toolbar class=\"md-theme-light lesson\" layout=\"row\">\n" +
    "		<h1 class=\"md-toolbar-tools\">{{vm.title}}</h1>\n" +
    "		<md-icon ng-click=\"vm.close()\">close</md-icon>\n" +
    "	</md-toolbar>\n" +
    "\n" +
    "	<md-tabs flex layout=\"column\" layout-fill class=\"md-accent affectation\">\n" +
    "		<md-tab flex layout=\"column\" label=\"Affectation\" >\n" +
    "			<md-tab-content flex layout-fill>\n" +
    "				<form layout =\"column\" novalidate>\n" +
    "					<md-input-container ng-if=\"vm.lessons.length > 1\">\n" +
    "						<label>Cours</label>\n" +
    "						<md-select aria-label=\"Cours\" class=\"select\" ng-model=\"vm.coursSelected\">\n" +
    "							<md-option selected ng-repeat=\"lesson in vm.lessons track by lesson.id\" value=\"{{lesson.id}}\">{{lesson.module.libelleCourt}}</md-option>\n" +
    "						</md-select>\n" +
    "					</md-input-container>\n" +
    "					<md-input-container ng-if=\"vm.lessons.length == 1\">\n" +
    "						<label>Cours</label>\n" +
    "						<md-select aria-label=\"Cours\" class=\"select\" ng-model=\"vm.coursSelected\">\n" +
    "							<md-option disabled selected ng-repeat=\"lesson in vm.lessons track by lesson.id\" value=\"{{lesson.id}}\">{{lesson.module.libelleCourt}}</md-option>\n" +
    "						</md-select>\n" +
    "					</md-input-container>\n" +
    "					<md-input-container>\n" +
    "						<label>Formateurs</label>\n" +
    "						<md-select aria-label=\"formateurs\" class=\"select\" ng-model=\"vm.formateurSelected\">\n" +
    "							<md-option ng-repeat=\"formateur in vm.formateurs track by formateur.idFormateur\" value=\"{{formateur.idFormateur}}\">{{formateur.nom}} {{formateur.prenom}}</md-option>\n" +
    "						</md-select>\n" +
    "					</md-input-container>\n" +
    "					<md-input-container>\n" +
    "						<label>Salles</label>\n" +
    "						<md-select aria-label=\"salles\" class=\"select\" ng-model=\"vm.salleSelected\">\n" +
    "							<md-option ng-repeat=\"salle in vm.salles track by salle.code\" value=\"{{salle.code}}\">{{salle.code}}</md-option>\n" +
    "						</md-select>\n" +
    "					</md-input-container>\n" +
    "					<md-button class=\"md-raised md-primary\" ng-click=\"vm.updateLesson()\">Affecter</md-button>\n" +
    "				</form>\n" +
    "			</md-tab-content>\n" +
    "		</md-tab>\n" +
    "\n" +
    "		<md-tab label=\"Alertes\" layout-fill>\n" +
    "			<md-tab-content flex layout-fill>\n" +
    "				<md-list class=\"md-dense displayAlerts\" flex>\n" +
    "					<md-list-item class=\"md-3-line alert\" ng-repeat=\"alert in vm.alerts\">\n" +
    "						<md-icon class=\"md-avatar-icon notifications warning\">warning</md-icon>\n" +
    "						<div class=\"md-list-item-text\" layout=\"column\">\n" +
    "							<p>{{alert.description}} - {{alert.lesson.module.libelleCourt}}</p>\n" +
    "						</div>\n" +
    "					</md-list-item>\n" +
    "				</md-list>\n" +
    "			</md-tab-content>\n" +
    "		</md-tab>\n" +
    "	</md-tabs>\n" +
    "</md-sidenav>\n"
  );


  $templateCache.put('app/modules/shared/templates/apidoc/apidoc.tpl.html',
    "<div layout=\"column\" layout-fill flex class=\"swagger-ui configuration\" aria-live=\"polite\" aria-relevant=\"additions removals\">\n" +
    "	<md-toolbar layout=\"row\" layout-align=\"end center\" flex>\n" +
    "		<h2 class=\"md-toolbar-tools\" ng-bind=\"infos.title\"></h2>\n" +
    "		<p ng-bind-html=\"infos.description\"></p>\n" +
    "	</md-toolbar>\n" +
    "	<md-card flex>\n" +
    "		<md-card-content ng-if=\"infos.contact\" md-colors=\"::{background: 'grey-100'}\">\n" +
    "			<div ng-if=\"infos.contact.name\" class=\"api-infos-contact-name\"><span swagger-translate=\"infoContactCreatedBy\" swagger-translate-value=\"infos.contact\"></span></div>\n" +
    "			<div ng-if=\"infos.contact.url\" class=\"api-infos-contact-url\"><span swagger-translate=\"infoContactUrl\"></span> <a href=\"{{infos.contact.url}}\" ng-bind=\"infos.contact.url\"></a></div>\n" +
    "			<a ng-if=\"infos.contact.email\" class=\"api-infos-contact-url\" href=\"mailto:{{infos.contact.email}}?subject={{infos.title}}\" swagger-translate=\"infoContactEmail\"></a>\n" +
    "			<div class=\"api-infos-license\" ng-if=\"infos.license\">\n" +
    "				<span swagger-translate=\"infoLicense\"></span><a href=\"{{infos.license.url}}\" ng-bind=\"infos.license.name\"></a>\n" +
    "			</div>\n" +
    "		</md-card-content>\n" +
    "	</md-card>\n" +
    "	<md-content flex>\n" +
    "		<md-list>\n" +
    "			<md-list-item class=\"md-2-line\" ng-repeat=\"api in resources track by $index\">\n" +
    "				<div class=\"md-list-item-text\" ng-include=\"'app/modules/shared/templates/apidoc/endpoint.tpl.html'\">\n" +
    "				</div>\n" +
    "			</md-list-item>\n" +
    "		</md-list>\n" +
    "	</md-content>\n" +
    "	<md-card flex>\n" +
    "		<md-card-content md-colors=\"::{background: 'grey-100'}\" ng-if=\"infos\">\n" +
    "			[<span swagger-translate=\"infoBaseUrl\"></span>: <span class=\"h4\" ng-bind=\"infos.basePath\"></span>, <span swagger-translate=\"infoApiVersion\"></span>: <span class=\"h4\" ng-bind=\"infos.version\"></span>, <span swagger-translate=\"infoHost\"></span>: <span class=\"h4\" ng-bind=\"infos.scheme\"></span>://<span class=\"h4\" ng-bind=\"infos.host\"></span>]\n" +
    "			<a ng-if=\"validatorUrl!='false'\" target=\"_blank\" href=\"{{validatorUrl}}/debug?url={{url}}\"><img class=\"pull-right swagger-validator\" ng-src=\"{{validatorUrl}}?url={{url}}\"/></a>\n" +
    "		</md-card-content>\n" +
    "	</md-card>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/shared/templates/apidoc/endpoint.tpl.html',
    "<md-card id=\"{{api.name}}\" md-colors=\"::{background: 'grey-400'}\" class=\"md-whiteframe-z3\">\n" +
    "  <md-card-header ng-click=\"api.open=!api.open;permalink(api.name)\" style=\"cursor: pointer\">\n" +
    "    <md-card-avatar>\n" +
    "      <md-icon md-font-set=\"material-icons\" aria-label=\"down\" ng-if=\"api.open\">remove</md-icon>\n" +
    "      <md-icon md-font-set=\"material-icons\" aria-label=\"down\" ng-if=\"!api.open\">add</md-icon>\n" +
    "    </md-card-avatar>\n" +
    "    <md-card-header-text>\n" +
    "      <h2 class=\"md-title\" style=\"float: left\" ng-bind=\"api.name\"><span ng-if=\"api.description\"> : <span ng-bind=\"api.description\"></span></span></h2>\n" +
    "    </md-card-header-text>\n" +
    "  </md-card-header>\n" +
    "  <md-card-content id=\"{{api.name}}\" md-colors=\"::{background: 'grey-200'}\">\n" +
    "    <ul id=\"{{api.name}}*\" class=\"list-inline pull-left endpoint-heading\">\n" +
    "        <li>\n" +
    "            <h4>\n" +
    "                <a href=\"javascript:;\" ng-click=\"api.open=!api.open;permalink(api.name)\" ng-bind=\"api.name\"></a>\n" +
    "                <span ng-if=\"api.description\"> : <span ng-bind=\"api.description\"></span></span>\n" +
    "            </h4>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "    <ul class=\"list-inline pull-right endpoint-actions\">\n" +
    "        <li>\n" +
    "            <a href=\"javascript:;\" ng-click=\"expand(api);permalink(api.name)\" swagger-translate=\"endPointListOperations\"></a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a href=\"javascript:;\" ng-click=\"expand(api,true);permalink(api.name+'*')\" swagger-translate=\"endPointExpandOperations\"></a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "    <ul class=\"list-unstyled operations\" ng-if=\"api.open\">\n" +
    "        <li ng-repeat=\"op in api.operations track by $index\" class=\"operation {{op.httpMethod}}\" ng-include=\"'templates/operation.html'\"></li>\n" +
    "    </ul>\n" +
    "  </md-card-content>\n" +
    "</md-card>\n"
  );


  $templateCache.put('app/modules/shared/templates/breadcrumb.tpl.html',
    "<ul class=\"breadcrumb\">\n" +
    "    <li ng-repeat=\"step in steps | limitTo:(steps.length-1)\">\n" +
    "        <a href=\"{{step.ncyBreadcrumbLink}}\" ng-bind-html=\"step.ncyBreadcrumbLabel\"></a>\n" +
    "        <md-icon md-font-set=\"material-icons\" flex>keyboard_arrow_right</md-icon>\n" +
    "    </li>\n" +
    "\n" +
    "    <li ng-repeat=\"step in steps | limitTo:-1\" class=\"active\">\n" +
    "        <span ng-bind-html=\"step.ncyBreadcrumbLabel\"></span>\n" +
    "    </li>\n" +
    "</ul>\n"
  );


  $templateCache.put('app/modules/shared/templates/notify-atomic.tpl.html',
    "<div class=\"atomic-notify\">\n" +
    "    <div class=\"atomic-notify-item\" ng-repeat=\"item in items\" ng-class=\"discoverClass(item)\">\n" +
    "        <div class=\"icon\" ng-if=\"item.icon\">\n" +
    "            <md-icon md-font-set=\"material-icons\" aria-label=\"down\">{{item.icon}}</md-icon>\n" +
    "        </div>\n" +
    "        <div class=\"body\">\n" +
    "            <p>{{item.text}}</p>\n" +
    "        </div>\n" +
    "        <button type=\"button\" class=\"close\" ng-click=\"dismiss(item)\">&times;</button>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/timesheet/timesheet.html',
    "<div class=\"md-padding\" flex layout-sm=\"column\">\n" +
    "    <md-card>\n" +
    "        <md-card-content>\n" +
    "            <h2 class=\"md-title\">Content from: timesheet page</h2>\n" +
    "        </md-card-content>\n" +
    "    </md-card>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/user/user.html',
    "<div class=\"md-padding\" flex layout-sm=\"column\">\n" +
    "    <md-card>\n" +
    "        <md-card-content>\n" +
    "            <h2 class=\"md-title\">Content from: user page</h2>\n" +
    "        </md-card-content>\n" +
    "    </md-card>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/vacationrequest/addVacationrequestDialog.html',
    "<md-dialog add-item-dialog=\"\" role=\"dialog\" tabindex=\"-1\" aria-describedby=\"dialog_27\" class=\"md-transition-in\" style=\"\" class=\"layout-column\" layout=\"column\">\n" +
    "	<div role=\"dialog\" layout=\"column\" layout-align=\"center center\">\n" +
    "		<md-toolbar>\n" +
    "			<div class=\"md-toolbar-tools\">\n" +
    "				<h2>Nouvelle demande de congé</h2>\n" +
    "			</div>\n" +
    "		</md-toolbar>\n" +
    "		<form layout-margin>\n" +
    "			<div>\n" +
    "				<p>Date de début : </p>\n" +
    "				<md-datepicker ng-model=\"vm.newTimeOff.dateStart\" md-placeholder=\"Date début\" md-open-on-focus flex></md-datepicker>\n" +
    "				<div layout=\"row\">\n" +
    "					<md-input-container>\n" +
    "						<input ng-model=\"vm.newTimeOff.hourStart\" type=\"number\" min=\"0\" max=\"23\">\n" +
    "					</md-input-container>\n" +
    "					<md-input-container>\n" +
    "						<input ng-model=\"vm.newTimeOff.minuteStart\" type=\"number\" min=\"0\" max=\"59\">\n" +
    "					</md-input-container>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div>\n" +
    "				<p>Date de fin : </p>\n" +
    "				<md-datepicker ng-model=\"vm.newTimeOff.dateEnd\" md-placeholder=\"Date fin\" md-open-on-focus flex></md-datepicker>\n" +
    "				<div layout=\"row\">\n" +
    "					<md-input-container>\n" +
    "						<input ng-model=\"vm.newTimeOff.hourEnd\" type=\"number\" min=\"0\" max=\"23\">\n" +
    "					</md-input-container>\n" +
    "					<md-input-container>\n" +
    "						<input ng-model=\"vm.newTimeOff.minuteEnd\" type=\"number\" min=\"0\" max=\"59\">\n" +
    "					</md-input-container>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			<div>\n" +
    "				<md-input-container class=\"md-block\" flex-gt-sm>\n" +
    "					<label>Commentaire</label>\n" +
    "					<input ng-model=\"vm.newTimeOff.motif\">\n" +
    "				</md-input-container>\n" +
    "			</div>\n" +
    "		</form>\n" +
    "	<div class=\"md-dialog-actions layout-row layout-align-center\" layout=\"row\">\n" +
    "		<button class=\"md-primary md-button md-ink-ripple\" type=\"button\" aria-label=\"Annuler\" ng-click=\"cancelDialog()\"><span>Annuler</span></button>\n" +
    "		<button class=\"md-primary md-button md-ink-ripple\" type=\"button\" aria-label=\"Valider\" ng-click=\"validDialog()\"><span>Valider</span></button>\n" +
    "	</div>\n" +
    "</md-dialog>\n" +
    "\n"
  );


  $templateCache.put('app/modules/vacationrequest/vacationrequest.html',
    "<div layout=\"column\" flex layout-fill>\n" +
    "	<md-toolbar class=\"md-table-toolbar md-default\">\n" +
    "		<div class=\"md-toolbar-tools\">\n" +
    "			<span>Demandes de congé</span>\n" +
    "			<div flex></div>\n" +
    "			<md-button class=\"md-hue-3 md-warn md-raised md-icon-button\" aria-label=\"add_vacation_request\" ng-click=\"vm.showAddVacationrequestDialog($event)\">\n" +
    "				<md-icon>add</md-icon>\n" +
    "			</md-button>\n" +
    "		</div>\n" +
    "	</md-toolbar>\n" +
    "	<div>\n" +
    "		<div layout=\"row\">\n" +
    "			<p>Filtres :</p>\n" +
    "			<div layout=\"row\">\n" +
    "				<md-input-container>\n" +
    "					<label>Date début</label>\n" +
    "					<md-datepicker ng-model=\"vm.filtreDateDebut\" md-placeholder=\"Date début\" md-open-on-focus ng-change=\"vm.refreshHolidays()\"></md-datepicker>\n" +
    "				</md-input-container>\n" +
    "				<md-input-container>\n" +
    "					<label>Date fin</label>\n" +
    "					<md-datepicker ng-model=\"vm.filtreDateFin\" md-placeholder=\"Date fin\" md-open-on-focus ng-change=\"vm.refreshHolidays()\"></md-datepicker>\n" +
    "				</md-input-container>\n" +
    "			</div>\n" +
    "		</div>\n" +
    "		<md-input-container class=\"md-block\" ng-show=\"vm.isAdmin()\">\n" +
    "			<label>Recherche par formateur</label>\n" +
    "			<input ng-model=\"vm.searchText\">\n" +
    "		</md-input-container>\n" +
    "	</div>\n" +
    "	<md-table-container>\n" +
    "		<table md-table>\n" +
    "			<thead md-head md-order=\"vm.query.order\">\n" +
    "			<tr md-row>\n" +
    "				<th md-column class=\"md-column ng-isolate-scope md-numeric md-sort\"><span>Demandeur</span></th>\n" +
    "				<th md-column md-order-by=\"dateDemandeConge\" md-desc class=\"md-column ng-isolate-scope md-numeric md-sort\"><span>Date demande</span></th>\n" +
    "				<th md-column md-order-by=\"dateDebutConge\" md-desc class=\"md-column ng-isolate-scope md-numeric md-sort md-active\"><span>Date début</span></th>\n" +
    "				<th md-column md-order-by=\"dateFinConge\" md-desc class=\"md-column ng-isolate-scope md-numeric md-sort\"><span>Date fin</span></th>\n" +
    "				<th md-column md-order-by=\"etatConge\" class=\"md-column ng-isolate-scope md-numeric md-sort\"><span>État</span></th>\n" +
    "				<th md-column><span>Commentaire</span></th>\n" +
    "			</tr>\n" +
    "			</thead>\n" +
    "			<tbody md-body>\n" +
    "			<!--<tr md-row ng-repeat=\"holiday in vm.holidays | filter: vm.filterByFormateur | limitTo: vm.query.limit: (vm.query.page -1) * vm.query.limit\">-->\n" +
    "			<tr md-row ng-repeat=\"holiday in vm.holidays | filter: vm.filterByFormateur | limitTo: vm.query.limit: (vm.query.page -1) * vm.query.limit\">\n" +
    "				<md-content>\n" +
    "				<td md-cell>{{holiday.formateur.prenom}} {{holiday.formateur.nom}}</td>\n" +
    "				<td md-cell>{{holiday.dateDemandeConge | date : 'dd/MM/yyyy HH:mm'}}</td>\n" +
    "				<td md-cell>{{holiday.dateDebutConge | date : 'dd/MM/yyyy HH:mm'}}</td>\n" +
    "				<td md-cell>{{holiday.dateFinConge | date : 'dd/MM/yyyy HH:mm'}}</td>\n" +
    "				<td md-cell class=\"layout-align-center\" layout-align=\"center\" ng-if=\"(holiday.etatConge == vm.etatEnum.EN_ATTENTE.valeurEnum && vm.isAdmin())\">\n" +
    "					<button class=\"md-primary md-button md-ink-ripple\" type=\"button\" aria-label=\"Valider\" ng-click=\"vm.showValidationVacationrequestDialog(holiday, $event)\"><span>Répondre</span></button>\n" +
    "				</td>\n" +
    "				<td md-cell ng-if=\"holiday.etatConge != vm.etatEnum.EN_ATTENTE.valeurEnum || !vm.isAdmin()\">{{vm.getDescriptionEtat(holiday.etatConge)}}</td>\n" +
    "				<td md-cell>{{holiday.motif}}</td>\n" +
    "				</md-content>\n" +
    "			</tr>\n" +
    "			</tbody>\n" +
    "		</table>\n" +
    "	</md-table-container>\n" +
    "	<md-table-pagination ng-show=\"vm.hasHolidays\" md-limit=\"vm.query.limit\" md-page=\"vm.query.page\" md-limit-options=\"[5, 10, 15, 20]\" md-total=\"{{vm.getCongesFiltres().length}}\"  md-page-select></md-table-pagination>\n" +
    "</div>\n"
  );


  $templateCache.put('app/modules/vacationrequest/validationVacationrequestDialog.html',
    "<md-dialog add-item-dialog=\"\" role=\"dialog\" tabindex=\"-1\" class=\"md-transition-in\" style=\"\" class=\"layout-column\" layout=\"column\">\n" +
    "	<div role=\"dialog\" layout=\"column\" layout-align=\"center center\">\n" +
    "		<md-toolbar>\n" +
    "			<div class=\"md-toolbar-tools\">\n" +
    "				<h2>Validation d'une demande de congé</h2>\n" +
    "			</div>\n" +
    "		</md-toolbar>\n" +
    "		<form layout-margin>\n" +
    "			<div layout=\"column\">\n" +
    "				<p>Formateur : <b>{{vm.inValidationConge.formateur.prenom}} {{vm.inValidationConge.formateur.nom}}</b></p>\n" +
    "				<p>Début du congé : <b>{{vm.inValidationConge.dateDebutConge | date : 'dd/MM/yyyy HH:mm'}}</b></p>\n" +
    "				<p>Fin du congé : <b>{{vm.inValidationConge.dateFinConge | date : 'dd/MM/yyyy HH:mm'}}</b></p>\n" +
    "				<p>Commentaire : <b>{{vm.inValidationConge.motif}}</b></p>\n" +
    "			</div>\n" +
    "		</form>\n" +
    "	</div>\n" +
    "	<div class=\"md-dialog-actions layout-row layout-align-center\"  layout=\"row\">\n" +
    "		<button class=\"md-primary md-button md-ink-ripple\" type=\"button\" aria-label=\"Refuser\" ng-click=\"refuseCongeDialog()\"><span>Refuser</span></button>\n" +
    "		<button class=\"md-primary md-button md-ink-ripple\" type=\"button\" aria-label=\"Valider\" ng-click=\"validCongeDialog()\"><span>Valider</span></button>\n" +
    "	</div>\n" +
    "</md-dialog>\n" +
    "\n"
  );

}]);
