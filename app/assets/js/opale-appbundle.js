/*!
* opale - v0.0.1 - MIT LICENSE 2016-10-24. 
* @author Groupe C
*/
(function() {
	'use strict';

	/**
	 * @ngdoc index
	 * @name app
	 * @description
	 * # app
	 *
	 * Main modules of the application.
	 */

	angular.module('opale', [
		'ngResource',
		'ngAria',
		'ngMaterial',
		'ngMdIcons',
		'ngCookies',
		'ngAnimate',
		'ngMessages',
		'ngSanitize',
		'ui.router',
		'ncy-angular-breadcrumb',
		'angularMoment',
		'atomic-notify',
		'home',
		'config',
		'timesheet',
		'vacationrequest',
		'profile',
		'settings',
		'planning',
		'user',
		'login',
		'loading',
		'http-auth-interceptor',
		'swaggerUi',
		'md.data.table',
		'ngMaterialDatePicker',
	]);

})();

(function () {
	'use strict';

	/**
	* @ngdoc configuration file
	* @name app.config:config
	* @description
	* # Config and run block
	* Configutation of the app
	*/


	angular
	.module('opale')
	.config(configure)
	.factory('XSRFInterceptor', XSRFInterceptorFactory)
	.run(runBlock);

	configure.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$mdThemingProvider', '$breadcrumbProvider', '$mdToastProvider', 'atomicNotifyProvider'];

	function configure($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $mdThemingProvider, $breadcrumbProvider, $mdToastProvider, atomicNotifyProvider) {

		$locationProvider.hashPrefix('!');

		// This is required for Browser Sync to work poperly
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$httpProvider.defaults.withCredentials = true;
		$httpProvider.interceptors.push('XSRFInterceptor');


		$urlRouterProvider.otherwise('/dashboard');

		$mdThemingProvider.theme('default')
		.primaryPalette('blue', {
			'default': '400',
			'hue-1': '100',
			'hue-2': '600',
			'hue-3': 'A100'
		})
		.accentPalette('light-blue', {
			'default': '900',
			'hue-1': '200',
			'hue-2': '700',
			'hue-3': 'A400'
		})
		.warnPalette('lime', {
			'default': '900',
			'hue-1': '200',
			'hue-2': '700',
			'hue-3': 'A400'
		});

		/**
		* Parametrage du fil d'ariane
		*/
		$breadcrumbProvider.setOptions({
			templateUrl: 'app/modules/shared/templates/breadcrumb.tpl.html'
		});

		/**
		* Paramétrage du système de notifications par toast
		*/
		atomicNotifyProvider.setDefaultDelay(5000);
		atomicNotifyProvider.useIconOnNotification(true);

	}

	runBlock.$inject = ['config', '$rootScope', 'amMoment', 'LoginService', 'Session', 'USER_ROLES', '$q', '$timeout', '$state', '$cookies', '$location'];

	function runBlock(config, $rootScope, amMoment, LoginService, Session, USER_ROLES, $q, $timeout, $state, $cookies, $location) {
		$rootScope.$on('$stateChangeStart',	function(event, toState, toParams, fromState, fromParams, options){
			if($location.originalPath === "/login" && $rootScope.authenticated) {
				event.preventDefault();
			} else if(toState.access && toState.access.loginRequired && !$rootScope.authenticated){
				event.preventDefault();
				$rootScope.$broadcast("event:auth-loginRequired", {});
			} else if(toState.access && !LoginService.isAuthorized(toState.access.authorizedRoles)){
				event.preventDefault();
				$rootScope.$broadcast("event:auth-forbidden", {});
			}
		});


		$rootScope.$on('event:auth-loginConfirmed', function (event, data) {
			$rootScope.loadingAccount = false;
			var nextLocation = redirectOnLogin(data.habilitation.libelle);
			var delay = ($state.$current.name === "loading" ? 1000 : 0);
			$timeout(function () {
				Session.create(data);
				$rootScope.account = Session;
				$rootScope.authenticated = true;
				if(nextLocation === 'home.planning'){
					var dateStart = moment().startOf('month').startOf('week').add(1,'days').format(config.format);
					var dateEnd = moment().add(1,'months').endOf('month').endOf('week').format(config.format);
					$state.go(nextLocation, {dateStart:dateStart,dateEnd:dateEnd}, { location: "replace"});
				} else {
					$state.go(nextLocation, { location: "replace"});
				}

				// $location.path(nextLocation).replace();
			}, delay);
		});

		function redirectOnLogin (habilitation) {
			if(habilitation === 'admin'){
				return ($rootScope.requestedUrl ? $rootScope.requestedUrl : "home.dashboard");
			} else {
				return ($rootScope.requestedUrl ? $rootScope.requestedUrl : "home.planning");
			}
		}

		// Call when the 401 response is returned by the server
		$rootScope.$on('event:auth-loginRequired', function (event, data) {
			if ($rootScope.loadingAccount && data.status !== 401) {
				console.log("401");
				$rootScope.requestedUrl = $location.path();
				$state.go('loading');
			} else {
				console.log("login");
				Session.invalidate();
				$rootScope.authenticated = false;
				$rootScope.loadingAccount = false;
				$state.go('login', { location: "replace"});
			}
		});

		// Call when the 403 response is returned by the server
		$rootScope.$on('event:auth-forbidden', function (rejection) {
			$rootScope.$evalAsync(function () {
				$state.go('error.403', { location: "replace", notify: false });
			});
		});

		// Call when the user logs out
		$rootScope.$on('event:auth-loginCancelled', function () {
			$location.path("/login").replace();
		});

		$rootScope.$on('event:api-connect-error', function () {
			Utils.viewToast("error", "Impossible de se connecter à l'API!");
		});

		// Get already authenticated user account
		LoginService.getAccount();
		amMoment.changeLocale('fr');
	}

	function XSRFInterceptorFactory($log, $cookies, $cacheFactory, $rootScope, $q){
		var xsrfToken = $cookies.get('XSRF-TOKEN');
		var auth = sessionStorage.getItem("auth");

		var XSRFInterceptor = {

			request: function(config) {

				$rootScope.isLoading = true;

				if (xsrfToken) {
					config.headers['X-XSRF-TOKEN'] = xsrfToken;
				}
				if(auth){
					config.headers['Authorization'] = auth;
				}

				if(config.method === 'PUT' || config.method === 'POST' || config.method === 'DELETE'){
					var $httpDefaultCache = $cacheFactory.get('$http');
					$httpDefaultCache.removeAll();
				}

				return config;
			},

			response: function(response) {
				$rootScope.isLoading = false;

				var newToken = response.headers('X-XSRF-TOKEN');

				if (newToken) {
					xsrfToken = newToken;
				}

				return response;
			},

			responseError: function(response) {
				console.log(response);
				if(response.status === -1){
					$rootScope.$broadcast('auth-loginCancelled', {});
					// return response || $q.when(response);
				} else if (response.status === 401) {
					$rootScope.$broadcast('auth-loginRequired', {});
					// return response || $q.when();
				} else if(response.status === 403){
					$rootScope.$broadcast('auth-loginForbidden', {});
					// return response || $q.when(response);
				} else {
					return response;
				}
			}
		};

		return XSRFInterceptor;
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.module:homeModule
	* @description
	* # homeModule
	* Module of the app
	*/

	angular.module('loading', []);
})();

(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.module:configModule
	* @description
	* # configModule
	* Module of the app
	*/

	angular.module('config', [])
	.constant('config', {
		appName: 'OPALE',
		description: 'Organisation et Planification Assistées pour Les Entreprises',
		baseUrl: '/',
		apiUrl : 'http://127.0.0.1:8080/v1',
		formatGeneric : 'YYYY-MM-DD',
		formatWeek : 'DD MMM',
		formatDayAndMonth : 'DD-MM',
		format:'YYYY-MM-DDTHH:mm:ss'
	})
	.constant('USER_ROLES', {
		all: '*',
		admin: 'admin',
		resp: 'resp_form',
		form: 'form'
	});
})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.module:homeModule
	* @description
	* # homeModule
	* Module of the app
	*/

	angular.module('home', ['pageslide-directive']);
})();

(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:loginModule
	 * @description
	 * # loginModule
	 * Module of the app
	 */

  	angular.module('login', ['opale']);

})();

(function () {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.module:planningModule
	 * @description
	 * # planningModule
	 * Module of the app
	 */

	var underscore = angular.module('underscore', []);
	underscore.factory('_', function() {
		return window._; //Underscore must already be loaded on the page
	});

  	angular.module('planning', ['underscore']);

})();

(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:notificationsModule
   * @description
   * # notificationsModule
   * Module of the app
   */

   angular.module('profile', []);

})();

(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:settingsModule
   * @description
   * # settingsModule
   * Module of the app
   */

   angular.module('settings', []);

})();

(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:timesheetModule
   * @description
   * # timesheetModule
   * Module of the app
   */

   angular.module('timesheet', []);

})();

(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:userModule
   * @description
   * # userModule
   * Module of the app
   */

   angular.module('user', []);

})();

(function () {
   'use strict';

   /**
   * @ngdoc function
   * @name app.module:vacationrequestModule
   * @description
   * # vacationrequestModule
   * Module of the app
   */

   angular.module('vacationrequest', [])
	   .config(function($mdDateLocaleProvider) {

		   // Example of a French localization.
		   $mdDateLocaleProvider.months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
		   $mdDateLocaleProvider.shortMonths = ['janv', 'févr', 'mars', 'avr', 'mai', 'juin', 'juil', 'août', 'sept', 'oct', 'nov', 'déc'];
		   $mdDateLocaleProvider.days = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
		   $mdDateLocaleProvider.shortDays = ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'];

		   // Can change week display to start on Monday.
		   $mdDateLocaleProvider.firstDayOfWeek = 1;

		   $mdDateLocaleProvider.msgCalendar = 'Calendrier';
		   $mdDateLocaleProvider.msgOpenCalendar = 'Ouvrir le calendrier';

		   // Optional.
		   // $mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6];

		   // Example uses moment.js to parse and format dates.
		   $mdDateLocaleProvider.parseDate = function(dateString) {
			   var m = moment(dateString, 'L', true);
			   m.tz('Europe/Paris');
			   return m.isValid() ? m.toDate() : new Date(NaN);
		   };

		   $mdDateLocaleProvider.formatDate = function(date) {
			   var m = moment(date);
			   return m.isValid() ? m.format('DD-MM-YYYY') : '';
		   };

		   // $mdDateLocaleProvider.monthHeaderFormatter = function(date) {
			//    return myShortMonths[date.getMonth()] + ' ' + date.getFullYear();
		   // };

		   // In addition to date display, date components also need localized messages
		   // for aria-labels for screen-reader users.

		   $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
			   return 'Semaine ' + weekNumber;
		   };

		   // // You can also set when your calendar begins and ends.
		   // $mdDateLocaleProvider.firstRenderableDate = new Date(1776, 6, 4);
		   // $mdDateLocaleProvider.lastRenderableDate = new Date(2030, 11, 21);
	   });
})();

'use strict';

	/**
	* @ngdoc function
	* @name app.route:HomeRoute
	* @description
	* # HomeRoute
	* Route of the app
	*/

angular.module('loading')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {
		$stateProvider
		.state('loading', {
			url:'/loading',
			templateUrl: 'app/modules/commons/loading.html',
			access : {
				loginRequired: false,
				authorizedRoles: [USER_ROLES.all]
			}
		});
	}]);

'use strict';

	/**
	* @ngdoc function
	* @name app.route:HomeRoute
	* @description
	* # HomeRoute
	* Route of the app
	*/

angular.module('opale')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {
		$stateProvider
			.state('home', {
				url: '',
				abstract: true,
				templateUrl: 'app/modules/home/main.html',
				controller: 'HomeCtrl',
				controllerAs: 'vm',
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.all]
				}
			})
			.state('home.dashboard', {
				url:'/dashboard',
				templateUrl: 'app/modules/home/dashboard.html',
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			});

	}]);

'use strict';

/**
 * @ngdoc function
 * @name app.route:loginRoute
 * @description
 * # loginRoute
 * Route of the app
 */

angular.module('login')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {

		$stateProvider
			.state('login', {
				url:'/login',
				templateUrl: 'app/modules/login/login.html',
				controller: 'LoginCtrl',
				controllerAs: 'vm',
				access : {
					loginRequired: false,
					authorizedRoles: [USER_ROLES.all]
				}
			});


	}]);

'use strict';

/**
 * @ngdoc function
 * @name app.route:planningRoute
 * @description
 * # planningRoute
 * Route of the app
 */

angular.module('planning')
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			.state('home.planning', {
				url:'/planning/:dateStart/:dateEnd',
				templateUrl: 'app/modules/planning/planning.html',
				controller: 'PlanningCtrl',
				controllerAs: 'vm',
				resolve: {

					parametres: ["homeService",function(homeService){
						return homeService.getParametres();
					}],
					alertes: ["homeService",function (homeService) {
						return homeService.getAlertes();
					}],
					formateurs: ["homeService", function(homeService) {
						return homeService.getFormateurs();
					}],
					salles: ["homeService", function(homeService) {
						return homeService.getSalles();
					}],
					promotions: ['homeService','$stateParams',function(homeService,$stateParams){
						return homeService.getPromotions($stateParams.dateStart,$stateParams.dateEnd);
					}]
				}
			});
	}]);

'use strict';

/**
 * @ngdoc function
 * @name app.route:notificationsRoute
 * @description
 * # notificationsRoute
 * Route of the app
 */

angular.module('profile')
	.config(['$stateProvider', function ($stateProvider) {

		$stateProvider
			.state('home.profile', {
				url:'/profile',
				templateUrl: 'app/modules/profile/profile.html',
				controller: 'ProfileCtrl',
				controllerAs: 'vm'
			});


	}]);

'use strict';

/**
 * @ngdoc function
 * @name app.route:settingsRoute
 * @description
 * # settingsRoute
 * Route of the app
 */

angular.module('settings')
	.config(['$stateProvider', 'USER_ROLES', function ($stateProvider, USER_ROLES) {

		$stateProvider
			.state('home.settings', {
				url:'/settings',
				templateUrl: 'app/modules/settings/settings.html',
				controller: "SettingsCtrl",
				controllerAs: "vm",
				ncyBreadcrumb: {
					label: 'Paramétrage'
				},
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.alertes', {
				url:'/alertes',
				templateUrl: 'app/modules/settings/partial/alertes.part.html',
				ncyBreadcrumb: {
			    label: 'Alertes'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.params', {
				url:'/params',
				templateUrl: 'app/modules/settings/partial/params.part.html',
				ncyBreadcrumb: {
			    label: 'Paramètres'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.habilitations', {
				url:'/habilitations',
				templateUrl: 'app/modules/settings/partial/habilitations.part.html',
				ncyBreadcrumb: {
			    label: 'Habilitations'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.pisteaudit', {
				url:'/pisteaudit',
				templateUrl: 'app/modules/settings/partial/pisteAudit.part.html',
				ncyBreadcrumb: {
			    label: 'Traces Applicatives'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.formateurs', {
				url:'/formateurs',
				templateUrl: 'app/modules/settings/partial/formateurs.part.html',
				ncyBreadcrumb: {
			    label: 'Formateurs'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.promotions', {
				url:'/promotions',
				templateUrl: 'app/modules/settings/partial/promotions.part.html',
				ncyBreadcrumb: {
			    label: 'Promotions'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.modules', {
				url:'/modules',
				templateUrl: 'app/modules/settings/partial/modules.part.html',
				ncyBreadcrumb: {
			    label: 'Modules'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			})
			.state('home.settings.salles', {
				url:'/salles',
				templateUrl: 'app/modules/settings/partial/salles.part.html',
				ncyBreadcrumb: {
			    label: 'Salles'
			  },
				access : {
					loginRequired: true,
					authorizedRoles: [USER_ROLES.admin]
				}
			});
	}]);

'use strict';

/**
* @ngdoc function
* @name app.route:timesheetRoute
* @description
* # timesheetRoute
* Route of the app
*/

angular.module('timesheet')
.config(['$stateProvider', function ($stateProvider) {

	$stateProvider
	.state('home.timesheet', {
		url:'/timesheet',
		templateUrl: 'app/modules/timesheet/timesheet.html',
		controller: 'TimesheetCtrl',
		controllerAs: 'vm'
	});


}]);

'use strict';

/**
* @ngdoc function
* @name app.route:userRoute
* @description
* # userRoute
* Route of the app
*/

angular.module('user')
.config(['$stateProvider', function ($stateProvider) {

	$stateProvider
	.state('home.user', {
		url:'/user',
		templateUrl: 'app/modules/user/user.html',
		controller: 'UserCtrl',
		controllerAs: 'vm'
	});


}]);

'use strict';

/**
* @ngdoc function
* @name app.route:vacationrequestRoute
* @description
* # vacationrequestRoute
* Route of the app
*/

angular.module('vacationrequest')
.config(['$stateProvider', function ($stateProvider) {

	$stateProvider
	.state('home.vacationrequest', {
		url:'/vacationrequest',
		templateUrl: 'app/modules/vacationrequest/vacationrequest.html',
		controller: 'VacationrequestCtrl',
		controllerAs: 'vm'
	});


}]);

(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:HomeCtrl
	* @description
	* # HomeCtrl
	* Controller of the app
	*/

	angular
	.module('opale')
	.controller('HomeCtrl', Home)
	.controller('DashboardCtrl', Dashboard);

	Home.$inject = ['$scope','$state', 'homeService', 'config','$rootScope', 'Utils', 'LoginService', 'USER_ROLES'];
	Dashboard.$inject = ['homeService', 'Utils', '$rootScope'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Home($scope, $state, homeService, config, $rootScope, Utils, LoginService, USER_ROLES) {
		/*jshint validthis: true */
		var vm = this;
		vm.title = config.appName;
		vm.subtitle = config.description;
		vm.loading = false;
		vm.loadingText = "Veuillez patientez...";
		vm.version = "1.0.0";
		vm.months = getMonths();
		vm.showSidePanel = false;
		vm.showSidePanelTop = false;

		vm.isAuthorized = function (authorizedRole) {
			if(LoginService.isAuthorized(authorizedRole)){
				return true;
			}
		};

		vm.notifications = getNotifications(homeService, vm, Utils, $rootScope);

		$rootScope.$on('event:notificationsUpdate', function () {
			vm.notifications = $rootScope.notifications;
		});

		function getMonths(){
			var months = [];
			for(var i = 0; i < 12; i++){
				var month = moment().month(i).format('MMMM');
				months.push(month);
			}
			return months;
		}

		vm.menu = homeService.getMenuList();

		vm.syncLink = {
			link: 'refresh',
			name: 'Rafraîchissement',
			icon: 'sync',
			tooltip: 'Synchroniser les données',
			authorizedRoles: [USER_ROLES.admin]
		};

		vm.notification = 	{
			link: 'notifications',
			name: 'Notifications',
			icon: 'notifications',
			tooltip: 'Notifications',
			authorizedRoles: [USER_ROLES.all]
		};

		vm.userMenu = {
			link: 'user',
			name: 'User',
			icon: 'person',
		};

		/** Ouverture du menu */
		var originatorEv;
		vm.openMenuUser = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		vm.openNotif = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		vm.getNotifTheme = function (typeNotif) {
			if (typeNotif) {
				switch (typeNotif.toLowerCase()) {
					case 'conge':
					return 'notifications info';
					case 'fiche_temps':
					return 'notifications success';
					case 'alerte':
					return 'notifications error';
					default:
					return 'notifications warning';
				}
			}
			return 'notifications warning';
		};

		vm.getNotifIconName = function (typeNotif) {
			if (typeNotif) {
				switch (typeNotif.toLowerCase()) {
					case 'conge':
					return 'access_time';
					case 'fiche_temps':
					return 'assignment';
					case 'alerte':
					return 'warning';
					default:
					return 'notifications';
				}
			}
			return 'notifications';

		};

		$rootScope.$watch($state.current,function(newValue,oldValue){
			if(newValue === 'planning'){
				vm.showSidePanel = true;
			}else{
				vm.showSidePanel = false;
			}
		});

		vm.syncData = function syncData(){
			vm.loading = true;
			return homeService.syncData().then(function(response) {
				if(response.status === 200){
					vm.loading = false;
					Utils.viewToast("La synchronisation s'est correctement déroulée !");
				} else {
					vm.loading = false;
					Utils.viewToast("Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		vm.navigateTo = function (target) {
			var page = target;

			if(page === 'home.planning'){
				var dateStart = moment().startOf('month').startOf('week').add(1,'days').format(config.format);
				var dateEnd = moment().add(1,'months').endOf('month').endOf('week').format(config.format);

				$state.go(page, {dateStart:dateStart,dateEnd:dateEnd});
			}else {
				$state.go(page);
			}
		};
		/* Action sur le bouton logout du menu User */
		vm.logout = function(){
			homeService.logout();
		};

		/** FILTERS */
		vm.filterAccess = vm.filterAccess === undefined ? false : vm.filterAccess;
		vm.setFilterAccess = function(){
			console.log(vm.filterAccess);
			if(vm.filterAccess){
				vm.filterAccess = false;
				$('.ng-pageslide').css({height:'0px'});
			}else {
				vm.filterAccess = true;
				$('.ng-pageslide').css({height:'200px'});
			}
		};

		vm.setFiltersSelected = function(){
			$scope.$broadcast('filters', {apply:true});
		};
	}

	function Dashboard (homeService, Utils, $rootScope) {
		var vm = this;

		getNotifications(homeService, vm, Utils, $rootScope);

		vm.navigateTo = function (target) {
			var page = target;
			$state.go(page);
		};
	}

	function getNotifications(homeService, vm, Utils, $rootScope) {
		homeService.getNotifications()
		.then(function (response) {
			if(response.status === 200){
				$rootScope.notifications = response.data;
				$rootScope.$broadcast("notificationsUpdate", {});
				vm.notifications = response.data;
				vm.nbNotif = vm.notifications.length;
			} else {
				if(response.err === -1){
					Utils.viewToast("error", response.data);
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			}
		});
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:loginCtrl
	* @description
	* # loginCtrl
	* Controller of the app
	*/

	angular
	.module('login')
	.controller('LoginCtrl', Login);

	Login.$inject = ['$rootScope', '$scope', 'LoginService', 'Session', '$state', 'Utils', 'authService', '$cookies'];

	function Login($rootScope, $scope, LoginService, Session, $state, Utils, authService, $cookies) {
		var vm = this;

		vm.login = function (credentials) {
			$rootScope.authenticationError = false;
			LoginService.login(credentials)
			.then(function (response, status, headers, config) {
				if(response.status === 200){
					if(response.data === null){
						Utils.viewToast("error", "Echec de la connexion !");
					} else {
						Utils.viewToast("success", "Connexion réussie");
					}
				} else {
					if(response.err === -1){
						Utils.viewToast("error", response.data);
					} else {
						Utils.viewToast("error", "Erreur :" + response.err + ". " + response.data);
					}
				}

			}).catch(function (response, status, headers, config) {
				$rootScope.authenticationError = true;
				Session.invalidate();
				return {err: status, data: response};
			});
		};

	}

})();

(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:planningCtrl
	 * @description
	 * # planningCtrl
	 * Controller of the app
	 */

	angular
		.module('planning')
		.controller('PlanningCtrl', Planning);

	Planning.$inject = ['$rootScope','config','planningService','parametres','alertes','formateurs','salles','promotions','_','$scope','Utils','$stateParams','$q','$state','$mdSidenav'];

	function Planning($rootScope, config,planningService,parametres,alertes,formateurs,salles,promotions,_,$scope,Utils,$stateParams,$q,$state,$mdSidenav) {
		var vm = this;
		vm.weeks = [];
		vm.studyOfTheWeek = vm.studyOfTheWeek > 0 ? vm.studyOfTheWeek : 0;
		vm.contentMonths = [];
		vm.lessons = {};
		vm.alerts = [];
		$rootScope.displayFilters = {
			formations:[],
			promotions:[],
			formateurs:[],
			lieux:[],
			cities:[]
		};
		vm.displayPlanningImpossible = [];
		if($rootScope.data === undefined){
			$rootScope.data = {
				formations:[],
				formateurs:[],
				promotions:[],
				salles:[],
				lieux:[],
				apply: false,
				filtersSelected:{
					formations :[],
					promotions:[],
					lieux:[],
					salles:[],
					formateurs:[],
					viewPlanning:'promotion',
					month:moment().format('MMMM')
				}
			};
		}
		/** FILTRES */
		getEntities();
		function getEntities(){
			getPromotions().then(function() {
				$rootScope.data.formations = _.sortBy($rootScope.data.formations);
				$rootScope.data.lieux = _.sortBy($rootScope.data.lieux);
				if($rootScope.data.filtersSelected.formations.length === 0){
					$rootScope.data.filtersSelected.formations = $rootScope.data.formations;
				}
				if($rootScope.data.filtersSelected.lieux.length === 0){
					$rootScope.data.filtersSelected.lieux = $rootScope.data.lieux;
				}
			}).then(function(){
				/** récupération des salles et formateurs, une fois les données associées au promotions récupérées. */
				$rootScope.data.alertes = alertes.data;
				$rootScope.data.parametres = parametres.data;
				$rootScope.data.salles = salles.data;

				$rootScope.data.formateurs = formateurs.data;
				if($rootScope.data.filtersSelected.salles.length === 0){
					_.each($rootScope.data.salles,function(salle,index){
						if(salle && salle.lieu){
							$rootScope.data.filtersSelected.salles[index] = salle.lieu.libelle+'-'+salle.code;
						}
					});
				}
				_.each($rootScope.data.formateurs,function(formateur,index){
					$rootScope.data.filtersSelected.formateurs[index] = formateur.nom + ' ' + formateur.prenom;
				});
			}).then(function(){
				/** Récupération des cours. */
				getLessons();
			});
		}
		/** récupère les promotions et les données associées. */
		function getPromotions(){
			var indexPromotionToDeleted = [];
			var deferred = $q.defer();
			if($rootScope.data.promotions.length > 0){
				var filtersPromotionsAvailable = [];
				_.each($rootScope.data.promotions,function(promotion){
					filtersPromotionsAvailable.push(promotion.code);
				});
				var unselectedPromotionsFilters = _.difference(filtersPromotionsAvailable,$rootScope.data.filtersSelected.promotions);
				_.each(unselectedPromotionsFilters,function(unselectedPromotion){
					var index = _.findLastIndex($rootScope.data.promotions,{code:unselectedPromotion});
					indexPromotionToDeleted.push(index);
				 });
			}else{
				$rootScope.data.promotions = promotions.data;
			}

			_.each($rootScope.data.promotions, function(promotion,index){
				if(_.contains(indexPromotionToDeleted,index)){
					$rootScope.data.filtersSelected.promotions.splice(index, 1);
				}else{
					$rootScope.data.filtersSelected.promotions[index] = promotion.code;
				}
				if(!angular.isUndefined(promotion.lieu) && promotion.lieu != null && !_.contains($rootScope.data.lieux,promotion.lieu.libelle)){
					$rootScope.data.lieux.push(promotion.lieu.libelle);
				}
				if(!angular.isUndefined(promotion.formation) && promotion.formation != null && !_.contains($rootScope.data.formations,promotion.formation.code)){
					$rootScope.data.formations.push(promotion.formation.code);
				}
			});
			deferred.resolve("success");
			return deferred.promise;
		}

		/** récupère les cours en fonction du mois sélectionné. */
		function getLessons(){
			vm.lessons = [];
			vm.interval = {
				dateStart:$stateParams.dateStart,
				dateEnd : $stateParams.dateEnd
			};
			vm.getWeeks = getWeeks();
			/** @WebService */
			planningService.getLessons(vm.interval).then(function(response) {
				if(response.status === 200){
					_.each(response.data,function(lesson){
						/** gestion du cours à cheval entre le premier mois affiché et le précédent. */
						var debutStart = moment(lesson.debut);
						if(debutStart.isBefore(vm.interval.dateStart,'week') && lesson.module.dureeEnSemaines > 1){
							for(var i = 0; i < lesson.module.dureeEnSemaines;i++){
								debutStart.add(1,'weeks');
								lesson.module.dureeEnSemaines -= 1;
								if(debutStart.isSame(vm.interval.dateStart,'week')){
									break;
								}
							}
						}
						lesson['week'] = debutStart.format('WW');
					});
					vm.lessons = response.data;
				}
			}).then(function(){
				displayPlanning(vm.lessons);
			});
		}

		/** Récupération des fitlres sélectionnées. */
		$scope.$on('filters', function (event, arg) {
			if(arg.apply) {
				var dateStart = moment().month(($rootScope.data.filtersSelected.month+1)).startOf('month').startOf('week').add(1, 'days').format(config.format);
				var dateEnd = moment().month($rootScope.data.filtersSelected.month+1).add(1, 'months').endOf('month').endOf('week').format(config.format);
				$state.go('home.planning', {dateStart: dateStart, dateEnd: dateEnd},{reload:true});
			}
		});

		/** Vérifie que le cours correspond aux filtres sélectionnés. */
		function isFiltered(lesson){
			var isFiltered =  lesson.promotion && _.contains($rootScope.data.filtersSelected.promotions,lesson.promotion.code)
				&& lesson.promotion.formation && _.contains($rootScope.data.filtersSelected.formations,lesson.promotion.formation.code);
			if($rootScope.data.filtersSelected.viewPlanning === 'formateur') {
				isFiltered = isFiltered && lesson.formateur && _.contains($rootScope.data.filtersSelected.formateurs, lesson.formateur.nom + ' ' + lesson.formateur.prenom);
			}else if($rootScope.data.filtersSelected.viewPlanning === 'salle'){
				isFiltered = isFiltered && lesson.salle && lesson.salle.lieu && _.contains($rootScope.data.filtersSelected.salles,lesson.salle.lieu.libelle+'-'+lesson.salle.code);
			}
			return isFiltered;
		}

		/**
		 * récupère la formation en fonction de la promotion courante.
		 */
		function displayEntityFound(){

			var isPromotion = _.findIndex($rootScope.data.filtersSelected.promotions,function(p){
				return p.trim() === vm.row.displayBy.trim();
			});

			if($rootScope.data.filtersSelected.viewPlanning === 'promotion'){
				if(isPromotion > -1){
					var index = _.findIndex($rootScope.data.promotions,function(p){
						return p.code === vm.row.displayBy;
					});
					var formation = $rootScope.data.promotions[index].formation.code;
					if(formation && !_.contains($rootScope.displayFilters.formations,formation)){
						$rootScope.displayFilters.formations.push(formation);
					}
				}
			}else if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
				if(!_.contains($rootScope.displayFilters.formateurs, vm.row.displayBy)){
					$rootScope.displayFilters.formateurs.push(vm.row.displayBy);
				}
			}
		}

		/**
		 * Rejecte les cours ayant des données importantes manquantes.
		 * @param lessonSorted : ensemble des cours filtrés
		 * @return lessonsSortedWithoutDataMissing
		 */
		function rejectLessonsWithDataMissing(lessonsSorted){
			var lessonsSortedWithoutDataMissing = lessonsSorted;
			if($rootScope.data.filtersSelected.viewPlanning === 'formateur') {
				lessonsSortedWithoutDataMissing = _.reject(lessonsSorted, function (l) {
					return l.formateur === null;
				});
			}else if($rootScope.data.filtersSelected.viewPlanning === 'salle'){
				lessonsSortedWithoutDataMissing = _.reject(lessonsSorted, function (l) {
					return l.salle === null;
				});
			}
			return lessonsSortedWithoutDataMissing;
		}

		/** Modifie les données du planning à afficher en ordonnée */
		vm.setViewPlanning = function (view){
			$rootScope.data.filtersSelected.viewPlanning = view;
			vm.displayPlanningImpossible = [];
			if($rootScope.data.filtersSelected.viewPlanning === 'promotion'){
				$rootScope.displayFilters.promotions = [];
				$rootScope.displayFilters.formations = [];
				$rootScope.displayFilters.lieux = [];
			}else if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
				$rootScope.displayFilters.formateurs = [];
			}
			$rootScope.displayFilters.cities = [];
			displayPlanning(vm.lessons);
		};

		/** AFFICHAGE DU PLANNING */

		/**
		 * Affiche le planning en fonction des cours planifiés.
		 * @param lessons:ensemble des cours planifiés.
		 */
		function displayPlanning(lessons){
			vm.contentMonths = [];
			vm.displayFilters = {
				formations : [],
				cities : [],
				promotions : []
			};
			vm.rowspan = {
				cities : {},
				previousCity : undefined,
				countCity : 0
			};
			vm.rowspanCity = {};
			var lessonsFiltered = _.filter(lessons,function(lesson){
				return isFiltered(lesson);
			});
			if(lessonsFiltered.length > 0){
				vm.displayPlanning = true;
				adaptDataToPlanning(lessonsFiltered);
				vm.managePlanning();
			}else {
				vm.displayPlanning = false;
			}
		}

		/** méthode mettant en forme les données récupérées du web service. */
		function adaptDataToPlanning(lessons){
			var lessonsWithoutDataMissing = rejectLessonsWithDataMissing(lessons);
			var lessonsFiltered = sortInTermsOfView(lessonsWithoutDataMissing);
			groupByView(lessonsFiltered);
		}

		/** trier en fonction de la vue souhaitée. */
		function sortInTermsOfView(lessonsWithoutDataMissing){
			var lessonsFiltered = _.sortBy(lessonsWithoutDataMissing, function (l) {
				return getDataInTermsOfViewSelected(l);
			});
			return lessonsFiltered;
		}

		/** définit la structure des données. */
		function groupByView(lessonsFiltered){
			/** regroument des données par lieu. */

			if($rootScope.data.filtersSelected.viewPlanning == 'salle'){
				vm.lessonsGrouped = _.groupBy(lessonsFiltered,function(l){
					if(l.salle && l.salle.lieu) {
						return l.salle.lieu;
					}
				});
			}else{
				vm.lessonsGrouped = _.groupBy(lessonsFiltered,function(l){
					if(l.promotion && l.promotion.lieu) {
						return l.promotion.lieu.libelle;
					}
				});
			}
			_.each(vm.lessonsGrouped, function (v, k) {
				vm.lessonsGrouped[k] = _.groupBy(v, function (l) {
					return getDataInTermsOfViewSelected(l);
				});
			});
		}
		/** récupère les données à afficher en fonction du filtre vue sélectionné. */
		function getDataInTermsOfViewSelected(l){
			var data = null;
			switch ($rootScope.data.filtersSelected.viewPlanning) {
				case 'promotion':
					if(l.promotion){data = l.promotion.code;}
					break;
				case 'formateur':
					if (l.formateur) {data = l.formateur.nom + ' ' + l.formateur.prenom;}
					break;
				case 'salle':
					if (l.salle && l.salle.lieu) {data = l.salle.lieu.libelle+'-'+l.salle.code;}
					break;
				default:
					if (l.promotion) {data = l.promotion.code;}
					break;
			}
			return data;
		}

		/** vérifie si le rowspan doit être appliqué. */
		vm.isRowspanApplied = function(city){
			if(vm.rowspan.previousCity !== city){
				vm.rowspan.previousCity = city;
				return true;
			}
			return false;
		};

		/** DONNEES PRINCIPALES DU PLANNING */

		/**
		 * Gestion des plannings
		 */
		vm.managePlanning = function(){
			if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
				planningService.getConges(vm.interval).then(function(response) {
					if(response.status === 200){
						vm.conges = response.data;
					}else{
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}else{
				vm.conges = undefined;
			}
			getDatas().then(function() {
				vm.rowspan.cities = _.groupBy(vm.contentMonths,function(v,k){
					return v.city;
				});
			}).then(function(){
				if(vm.displayPlanningImpossible.length === 1 ){
					Utils.viewToast("warn", "Le cours ne peut pas être planifié");
				}else if(vm.displayPlanningImpossible.length > 1){
					Utils.viewToast("warn", "Les cours ne peuvent pas être planifiés");
				}
			});
		};

		/**
		 * Gestion du contenu des promotions.
		 */
		function getDatas(){
			var deferred = $q.defer();
			for (var city in vm.lessonsGrouped) {
				if(!_.contains($rootScope.displayFilters.cities,city)){
					$rootScope.displayFilters.cities.push(city);
				}
				for(var displayBy in vm.lessonsGrouped[city]){
					vm.row = {
						city : city,
						displayBy : displayBy,
						lessons : undefined,
						display : false
					};
					vm.row.lessons = initDataForEachElementView();
					vm.planningImpossible = [];

					var conges = [];

					if($rootScope.data.filtersSelected.viewPlanning === 'formateur'){
						conges = manageConges(displayBy);
					}
					_.each(vm.lessonsGrouped[city][displayBy],function(lesson,key){
						var findError = _.find(vm.planningImpossible, function(planning){
							return planning.id == lesson.id;
						});
						if(isFiltered(lesson) && findError === undefined) {
							lesson.impossible = false;
							if(lesson.hasOwnProperty("otherLessonsDuringWeek")){
								delete lesson.otherLessonsDuringWeek;
							}
							if($rootScope.data.filtersSelected.viewPlanning === 'salle') {
								vm.row.city = lesson.salle.lieu.libelle;
							}

							if((moment(lesson.fin).diff(moment(lesson.debut),'weeks')+1) !== lesson.module.dureeEnSemaines) {
								lesson.module.dureeEnSemaines = (moment(lesson.fin).week() - moment(lesson.debut).week() + 1);
							}

							if(((moment(lesson.debut).week()-1) + (lesson.module.dureeEnSemaines-1)) <= (moment(lesson.fin).week()-1)){
								manageDureEnSemaines(lesson);
								var debutFormated = moment(lesson.debut).format(config.formatGeneric);
								var lessonsFiltered = vm.row.lessons;
								lessonsFiltered = _.filter(lessonsFiltered,function(l){
									return l !== undefined;
								});

								if (angular.isUndefined(vm.row.lessons[debutFormated])){
										if(lessonsFiltered && lessonsFiltered.length > 1 && moment(lessonsFiltered[lessonsFiltered.length-1])
											&& lesson.week >= (moment(lessonsFiltered[lessonsFiltered.length-1].debut).week()-1)
											&& lesson.week <= (moment(lessonsFiltered[lessonsFiltered.length-1].fin).week()-1)){
												manageLessonInSameWeek(lessonsFiltered,lessonsFiltered.length-1,lesson);
										}else{
											var impossible = impossibleCase(vm.lessonsGrouped[city][displayBy],lesson);
											addLesson(lesson,conges,impossible);
											lesson.alerts = manageAlerts(lesson);
										}
								}else{
									var impossible = impossibleCase(vm.lessonsGrouped[city][displayBy],lesson);
									if(!impossible){
										manageLessonInSameWeek(vm.row.lessons, debutFormated, lesson);
									}
								}
								manageLessonPlanifiedForSeveralWeek(lesson);
							}
						}
					});
					displayEntityFound();

					if(vm.row.display){
						vm.contentMonths.push(vm.row);
						$rootScope.displayFilters.promotions.push(displayBy);
					}
					vm.displayPlanning = vm.contentMonths.length > 0 ? true : false;
				}
			}
			deferred.resolve('success');
			return deferred.promise;
		}

		/** Gestion du cas impossible*/
		function impossibleCase(lessonsFiltered,lesson){
			var find = _.find(vm.planningImpossible, function(planning){
				return planning.id == lesson.id;
			});
			if(find){return true};
			if(lesson.module.dureeEnSemaines > 1 && find === undefined) {
				_.find(lessonsFiltered,function(l){
					if(l.id !== lesson.id
						&& (moment(l.debut).week()-1) === (moment(lesson.fin).week()-1) && l.module.dureeEnSemaines > 1
						|| ((moment(l.debut).week()-1) > (moment(lesson.debut).week()-1))
						&& (moment(l.debut).week()-1) <= (moment(lesson.fin).week()-1)
						&& (moment(l.fin).week()-1) > (moment(lesson.fin).week()-1)){
						vm.planningImpossible.push(l);
						vm.displayPlanningImpossible.push(l);
					}
				});
			}
			return false;
		}

		/** Gestion des durées en semaines. */
		function manageDureEnSemaines(lesson){
			if(moment(lesson.fin).isSame(vm.interval.dateStart,'weeks')){
				lesson.module.dureeEnSemaines = (moment(vm.interval.dateStart).week()) - (moment(lesson.fin).week()-1);
			} else if(moment(lesson.debut).isBefore(vm.interval.dateStart)){
				lesson.module.dureeEnSemaines = (moment(lesson.fin).week()) - (moment(vm.interval.dateStart).week()-1);
			}

			if(lesson.fin > vm.interval.dateEnd) {
				var dateEndLesson = moment(lesson.fin);
				while(dateEndLesson.format(config.format) >= vm.interval.dateEnd) {
					dateEndLesson.subtract(1,'weeks');
					lesson.module.dureeEnSemaines -=  1;
				}
			}
		}

		/** Gestion des congés */
		function manageConges(displayBy){
			var conges = _.filter(vm.conges,function(c){
				return displayBy === (c.formateur.nom + ' ' + c.formateur.prenom) && c.etatConge != "REFUSE"
					&& (moment(c.dateFinConge).isBefore(vm.interval.dateEnd) || moment(c.dateFinConge).isSame(vm.interval.dateEnd,'day'));
			});

			_.each(conges,function(c){
				var startWeekConge = moment(c.dateDebutConge).week()-1;
				var endWeekConge = moment(c.dateFinConge).week()-1;

				var findLesson = vm.row.lessons[startWeekConge];
				c.dureeEnSemaines = (endWeekConge - startWeekConge) + 1;
				if(angular.isUndefined(findLesson)){
					vm.row.lessons[startWeekConge] = c;
					var count = startWeekConge;
					if(c.dureeEnSemaines > 1){
						while(count != endWeekConge){
							delete vm.row.lessons[count+1];
							count++;
						}
					}
				}
			});
			return conges;
		}

		/** ajout d'un cours, gestion du cas : ajout d'un cours pendant une période de vacances */
		function addLesson(lesson,conges,impossible){
			var findConge = _.find(conges,function(conge){
				return moment(lesson.fin).isBetween(moment(conge.dateDebutConge),moment(conge.dateFinConge))
					|| moment(lesson.fin).isSame(moment(conge.dateFinConge),'day');
			});

			if(findConge === undefined){
				if(!impossible){
					if(moment(lesson.debut).isSame(vm.interval.dateStart) || moment(lesson.debut).isAfter(vm.interval.dateStart)){
						if(moment(lesson.debut).day() > 1){
							lesson.debut = moment(lesson.debut).startOf('week').add(1,'days');
						}
						vm.row.lessons[moment(lesson.debut).format(config.formatGeneric)] = lesson;
					}else{
						lesson.week = vm.weeks[0].number;
						lesson.module.dureeEnSemaines = (moment(lesson.fin).week() - vm.weeks[0].number);
						vm.row.lessons[vm.weeks[0].start] = lesson;
					}
				}
				vm.row.display = true;
			}else {
				manageLessonInSameWeek(vm.row.lessons,weekConge,lesson);
			}
		}

		/** Gestion des cours ayant lieu la même semaine */
		function manageLessonInSameWeek(lessons, date, lesson){
			if (lessons[date].hasOwnProperty("otherLessonsDuringWeek")) {
				if(!_.contains(lesson.otherLessonsDuringWeek,lesson)) {
					lessons[date].otherLessonsDuringWeek.push(lesson);
				}
			} else {
				lessons[date].otherLessonsDuringWeek = [lesson];
			}
			if(!lesson.hasOwnProperty("etatConge")){
				var longestStrategicLesson = _.max(lessons[date].otherLessonsDuringWeek, function (l) {
					if (l) {
						return moment(l.fin).diff(moment(l.debut), 'days');
					}
				});
				displayTheLessonWithTheMostDayPlanified(lessons,longestStrategicLesson,lesson,date);
			}
		}
		/** affichage du cours ayant le plus de jours planifiés lorsque plusieurs cours sont sur la même semaine. */
		function displayTheLessonWithTheMostDayPlanified(lessons,longestStrategicLesson,lesson,date){
			var diffDaysLongestStrategicLesson = moment(longestStrategicLesson.fin).diff(moment(longestStrategicLesson.debut), 'days');
			var diffDaysLesson = moment(lessons[date].fin).diff(moment(lessons[date].debut), 'days');

			if (diffDaysLesson < diffDaysLongestStrategicLesson) {
				var index = lessons[date].otherLessonsDuringWeek.indexOf(longestStrategicLesson);
				if (index > -1) {
					var arrayTemp = lessons[date].otherLessonsDuringWeek;
					arrayTemp.splice(index,1);
					arrayTemp.push(lessons[date]);
					longestStrategicLesson.otherLessonsDuringWeek = arrayTemp;
					lessons[date] = longestStrategicLesson;
				}
			}
		}

		/** Gestion des cours planifiés sur plusieurs semaines. */
		function manageLessonPlanifiedForSeveralWeek(lesson){
			if(lesson.module.dureeEnSemaines > 1) {
				var diff = ((moment(lesson.fin).week()-1) - (moment(lesson.debut).week()-1));
				var depart = lesson.debut;
				if(moment(lesson.fin).isSame(vm.interval.dateStart,'weeks')){
				} else if(moment(lesson.debut).isBefore(vm.interval.dateStart)){
					depart = vm.interval.dateStart;
				}

				diff = moment(depart).diff(lesson.fin,'weeks');

				for(var index = diff; index < 0; index ++){
					var startOfWeek = moment(lesson.fin).add(1,'weeks').startOf('week').add(1,'days').format(config.formatGeneric);
					delete vm.row.lessons[moment(startOfWeek).add(index,'weeks').format(config.formatGeneric)];
				}
			}
		}

		/** Initialisation de l'objet content avec les données temporelles. */
		function initDataForEachElementView(){
			var content = {};
			_.each(vm.weeks,function(week){
				if(week >= vm.weeks[0].number){
					content[week.start] = undefined;
				}
			});
			return content;
		}

		/** DONNEES SECONDAIRES DU PLANNING */

		/**
		 * Récupérer les semaines contenues dans l'interval.
		 * @returns {Array}
		 */
		function getWeeks(){
			vm.weeks = [];

			var dateStart = moment(vm.interval.dateStart);
			var dateEnd = moment(vm.interval.dateEnd);
			var dateEndWeekCurrent = dateStart.clone().add(4,'days');

			vm.weeks.push({
				'number':dateStart.format('WW'),
				'start':dateStart.format(config.formatGeneric),
				'end':dateEndWeekCurrent.format(config.formatGeneric)
			});

			var startDateManipulated = null;
			var endDateManipulated = null;
			var everyWeekAreSold = false;
			var countWeek = 1;
			/** tant que tous les semaines n'ont pas été dépassées, on passe à la suivante.*/
			while(!everyWeekAreSold){
				if(countWeek == 1){
					startDateManipulated = dateStart.add(1,'weeks');
					endDateManipulated = startDateManipulated.clone().add(4,'days');
				}else{
					startDateManipulated = startDateManipulated.clone().add(1,'weeks');
					endDateManipulated = startDateManipulated.clone().add(4,'days').endOf('day');
					if(!endDateManipulated.isBetween(dateStart, dateEnd)
						|| !startDateManipulated.isBetween(dateStart, dateEnd)
						|| endDateManipulated.isSame(dateEnd)){
						everyWeekAreSold = true;
					}
				}
				if(!everyWeekAreSold){
					vm.weeks.push({
						'number':startDateManipulated.format('WW'),
						'start':startDateManipulated.format(config.formatGeneric),
						'end':startDateManipulated.clone().add(4,'days').format(config.formatGeneric)
					});
					countWeek++;
				}
			}
			return vm.weeks;
		}

		vm.splitDisplayBy = function(displayBy){
			if($rootScope.data.filtersSelected.viewPlanning === 'salle'){
				displayBy = displayBy.split('-')[1];
			}
			return displayBy;
		};

		/** ALERTES */

		/** Gestion des alertes pour les salles. */
		function manageAlertsSalle(lessonCurrent, alerts, libelleAlerts){
			if(lessonCurrent.salle){
				if(lessonCurrent.salle.disponibilite){
					if(lessonCurrent.salle.capacite < lessonCurrent.nbStagiaire){
						addAlert(alerts,'capacity',lessonCurrent,libelleAlerts.capacity);
					}
					/** à la fois ce booléen doit être à vrai et aucun cours ne doit être liée à cette salle durant la période souhaitée. */
					_.find(vm.lessons, function(l){
						if(l.salle && l.salle.code===lessonCurrent.salle.code
							&& moment(l.debut).week() >= moment(lessonCurrent.debut).week()
							&& moment(l.debut).week() <= moment(lessonCurrent.fin).week()
							&& l.id !== lessonCurrent.id){
							addAlert(alerts,'occuped',l,libelleAlerts.occuped);
						}
					});
				}else{
					addAlert(alerts,'unavailability',lessonCurrent,libelleAlerts.unavailability);
				}

				if(lessonCurrent.module && lessonCurrent.salle.equipements.length > 0){
					if(lessonCurrent.module.equipements.length > 0){
						_.each(lessonCurrent.module.equipements,function(moduleEquipement){
							var equipementFound = _.find(lessonCurrent.salle.equipements,function(salleEquipement){
								return salleEquipement.id === moduleEquipement.id;
							});
							if(!equipementFound){
								addAlert(alerts,'equipements',lessonCurrent,libelleAlerts.equipements);
							}
						});
					}else{
						addAlert(alerts,'equipements',lessonCurrent,libelleAlerts.equipements);
					}
				}
			}
		}

		/** Gestion des alertes pour les compétences. */
		function manageAlertsCompetences(lessonCurrent, alerts, libelleAlerts){
			if(lessonCurrent.module){
				planningService.getExperiencesByFormateurAndModule(lessonCurrent.formateur.idFormateur,lessonCurrent.module.id).then(function(response) {
					if(response.status === 200){
						var experiencesFormateur = response.data;
						_.find($rootScope.data.parametres,function(parametre){
							if(parametre.code === 'PARAM_NB_DISP_EXP' && parametre.configs[0].valeur > experiencesFormateur){
								addAlert(alerts,'beginner',lessonCurrent,libelleAlerts.beginner);
							}
						});
					}
				});

				if(lessonCurrent.module.competences.length > 0){
					if(lessonCurrent.formateur.competences.length > 0){
						_.each(lessonCurrent.module.competences,function(moduleCompetence){
							var competenceFound = _.find(lessonCurrent.formateur.competences,function(formateurCompetence){
								return moduleCompetence.code === formateurCompetence.code && moduleCompetence.libelle === formateurCompetence.libelle;
							});
							if(!competenceFound){
								addAlert(alerts,'missingSkills',moduleCompetence,libelleAlerts.missingSkills);
							}
						});
					}else{
						addAlert(alerts,'missingSkills',moduleCompetence,libelleAlerts.missingSkills);
					}
				}
			}
		}

		/** indisponibilité du formateur. */
		function manageUnavailabilityTrainer(lessonsInTermsOfFormateurCurrent,lessonsInTermsOfFormateurCurrentWithFormateurAndSalle , response, alerts,libelleAlerts){
			if(response.status === 200){
				lessonsInTermsOfFormateurCurrent = response.data;
				var alertsUnavailableTrainers = [];
				_.each(lessonsInTermsOfFormateurCurrent,function(lessonToCompare){
					_.find(lessonsInTermsOfFormateurCurrent,function(otherLesson){
						if(lessonToCompare.module.id !== otherLesson.module.id){
							if(moment(otherLesson.debut).week() == moment(lessonToCompare.debut).week()
								|| moment(otherLesson.debut).isBetween(lessonToCompare.debut,lessonToCompare.fin)
								|| moment(otherLesson.debut).week() == moment(lessonToCompare.fin).week()) {
								var findAlert = _.find(alertsUnavailableTrainers,function(alert){
									return alert.module === otherLesson.module.libelleCourt;
								});
								if(!findAlert){
									alertsUnavailableTrainers.push({
										'date': moment(otherLesson.debut).format(config.formatGeneric),
										'module': otherLesson.module.libelleCourt
									});
									addAlert(alerts,'unavailableTrainer',otherLesson,libelleAlerts.unavailableTrainer);
								}
							}
						}
					});
					if(lessonToCompare.salle && lessonToCompare.salle.lieu){
						lessonsInTermsOfFormateurCurrentWithFormateurAndSalle.push(lessonToCompare);
					}
				});
			}
		}

		/** Gestion des alertes concernant les déplacements du formateur. */
		function manageDeplacementAlerts(lessonsInTermsOfFormateurCurrentWithFormateurAndSalle,lessonCurrent,alerts,libelleAlerts){
			if(lessonCurrent.salle && lessonCurrent.salle.lieu) {
				planningService.getDeplacementsFormateur(lessonCurrent.formateur.idFormateur).then(function (response) {
					var deplacements = response.data;
					var parameterDeplacementTolerated = _.find($rootScope.data.parametres,function(parametre){
						return parametre.code === 'PARAM_INT_VISION';
					});
					var successiveDeplacementTolerated = parameterDeplacementTolerated.configs[0].valeur;
					var successiveDeplacement = 0;
					var availableWeekEnd = 0;
					var lessonsInTermsOfFormateurCurrentWithFormateurAndSalleFiltered = _.filter(lessonsInTermsOfFormateurCurrentWithFormateurAndSalle,function(lesson){
						return lesson.promotion.code === lessonCurrent.promotion.code;
					});
					_.each(lessonsInTermsOfFormateurCurrentWithFormateurAndSalleFiltered, function (lesson, index) {
						/** on enlève 1 pour gérer la fuseau horaire. */
						if(index > 0){
							if(moment(lesson.debut).week()-1 === availableWeekEnd){
								_.each(deplacements, function (deplacement) {
									if (deplacement && deplacement.lieu && deplacement.lieu.id !== lesson.salle.lieu.id) {
										return false;
									}
								});
								successiveDeplacement += (moment(lesson.fin).week() - moment(lesson.debut).week()) + 1;
								if(successiveDeplacement >= successiveDeplacementTolerated){
									addAlert(alerts,'movement',lesson,libelleAlerts.movement);
									return true;
								}
								if(index < (lessonsInTermsOfFormateurCurrentWithFormateurAndSalleFiltered.length - 1)){
									var duringWeekLesson = (moment(lesson.fin).add(-1,'day').week() - moment(lesson.debut).add(-1,'day').week()) + 1;
									availableWeekEnd += duringWeekLesson;
								}
							}else{
								return false;
							}
						}else{
							availableWeekEnd = moment(lesson.debut).week() + (moment(lesson.fin).week() - moment(lesson.debut).week());
							successiveDeplacement = (moment(lesson.fin).week() - moment(lesson.debut).week()+1);
						}
					});
				});
			}
		}
		/** Gestion des alertes. */
		function manageAlerts(lessonCurrent){
			var alerts = [];
			var libelleAlerts = planningService.getAlertesPlanningList();
			manageAlertsSalle(lessonCurrent,alerts, libelleAlerts);
			if(lessonCurrent.formateur){
				if(lessonCurrent.formateur.externe){
					var remainingWeekForConfirmation = _.find($rootScope.data.alertes,function(alerte){
						return alerte.code === 'ALE_CONF_EXT';
					});
					if(moment(lessonCurrent.debut).diff(moment(),'weeks') < remainingWeekForConfirmation.configs[0].valeur && remainingWeekForConfirmation.configs[0].valeur > 0){
						addAlert(alerts,'externalSpeaker',lessonCurrent,libelleAlerts.externalSpeaker);
					}
				}else{
					var lessonsInTermsOfFormateurCurrent = [];
					var lessonsInTermsOfFormateurCurrentWithFormateurAndSalle = [];
					planningService.getLessonsByFormateur(lessonCurrent.formateur.idFormateur,vm.interval).then(function(response){
						manageUnavailabilityTrainer(lessonsInTermsOfFormateurCurrent,lessonsInTermsOfFormateurCurrentWithFormateurAndSalle, response, alerts,libelleAlerts);
					}).then(function(){
						manageDeplacementAlerts(lessonsInTermsOfFormateurCurrentWithFormateurAndSalle,lessonCurrent,alerts,libelleAlerts);
					});
				}
				manageAlertsCompetences(lessonCurrent, alerts, libelleAlerts);
			}
			return alerts;
		}

		/** méthode d'ajout d'une alerte. */
		function addAlert(alerts,code,lesson,description){
			alerts.push({
				code:code,
				lesson:lesson,
				description: description
			});
		}

		/** UTILS */

		/** Ouverture du menu */
		var originatorEv;
		vm.openMenu = function ($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		};

		/** SIDE NAV */

		vm.toggleRight = buildToggler('right');
		$rootScope.lessonSelected = {};
		vm.isOpenRight = function(){
			return $mdSidenav('right').isOpen();
		};

		vm.setLessonSelected = function(lesson){
			$rootScope.lessonSelected = lesson;
		};

		function buildToggler(navID) {
			return function() {
				$mdSidenav(navID)
					.toggle()
					.then(function () {
					});
			};
		}

		vm.close = function () {
			$mdSidenav('right').close()
				.then(function () {
				});
		};
	}
})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:notificationsCtrl
	* @description
	* # notificationsCtrl
	* Controller of the app
	*/

	angular
	.module('profile')
	.controller('ProfileCtrl', Profile);

	Profile.$inject = ['$rootScope'];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Profile($rootScope) {
		var vm = this;
		vm.user = $rootScope.account;
		vm.getHumanRole = function (habil) {
			switch (habil) {
				case "admin":
				return "administrateur";
				case "resp_form":
				return "reponsable formation";
				case "form":
				return "formateur";
			}
		};

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('AlertesCtrl', Alertes);

	Alertes.$inject = ['SettingsService', 'Utils'];

	function Alertes(SettingsService, Utils) {
		var vm = this;

		SettingsService.getAlerts().then(function(response) {
			if(response.status === 200){
				console.log(response);
				vm.alertes = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.save = function (alertes) {
			_.forEach(alertes, function (alerte) {
				SettingsService.updateParams(alerte).then(function (response) {
						if(response.status === 200){
							Utils.viewToast("success", "Les alertes ont bien été mises à jour !");
							SettingsService.getAlerts().then(function(response) {
								if(response.status === 200){
									console.log(response);
									vm.alertes = response.data;
								} else {
									Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
								}
							});
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					});
			});
		};

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('ApiDocCtrl', ApiDoc);

	ApiDoc.$inject = ['config'];

	function ApiDoc (config) {
		var vm = this;
		vm.isLoading = false;
		vm.url = config.apiUrl + '/v2/api-docs';
		// error management
		vm.myErrorHandler = function (data, status) {
			console.log('failed to load swagger: ' + status + '   ' + data);
		};

		vm.infos = false;
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('PisteAuditCtrl', PisteAudit);

	PisteAudit.$inject = ['SettingsService', 'Utils'];

	function PisteAudit(SettingsService, Utils) {
		var vm = this;

		vm.query = {
			order: 'date',
			reverse: false,
			limit: 10,
			page: 1
		};

		vm.hasPisteAudit = function () {
			return vm.holidays.length > 0;
		};

		SettingsService.getPisteAudit().then(function(response) {
			if(response.status === 200){
				console.log(response);
				vm.pisteAudit = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('FormateursCtrl', Formateurs);

	Formateurs.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope', '$mdDialog'];

	function Formateurs(SettingsService, Utils, $timeout, $q, $scope, $mdDialog) {
		var vm = this;

		/**
		* Autocomplete pour recherche formateur et competence
		*/
		vm.selectedFormateur = null;
		$scope.$watch(vm.searchFormateur, vm.searchFormateurChange);
		$scope.$watch(vm.searchCompetence, vm.searchCompetenceChange);

		vm.querySearch = function(query, searchType){
			var results;
			if(searchType === "formateur"){
				results = query ? vm.formateurs.filter( createFilterFormateur(query) ) : vm.formateurs;
			} else if(searchType === "competence" ){
				results = query ? vm.competences.filter( createFilterCompetence(query) ) : vm.competences;
			}
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		/**
		* Filtre la liste de formateur par le nom et le prenom
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterFormateur(query) {
			return function filterFn(formateur) {
				if(formateur !== undefined){
					return (formateur.nom.toLowerCase().indexOf(query.toLowerCase()) === 0 || formateur.prenom.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		/**
		* Filtre la liste de competence par le libelle et le libelleCourt
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterCompetence(query) {
			return function filterFn(competence) {
				if(competence !== undefined){
					return (competence.code.toLowerCase().indexOf(query.toLowerCase()) === 0 || competence.libelle.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		/**
		* Affiche la liste de competence checked pour le formateur selectionne
		* @param  formateur [Le formateur selectionne]
		*/
		vm.selectFormateur = function(formateur){
			console.log(formateur);
			vm.selectedFormateur = formateur;
			if(vm.selectedFormateur.competences !== null){
				vm.competencesFormateur = vm.selectedFormateur.competences;
				console.log(vm.competences);
			}
			SettingsService.getAffectationByFormateur(vm.selectedFormateur.idFormateur).then(function(response) {
				if(response.status === 200){
					vm.selectedFormateur.affectationsLieux = response.data;
					vm.affectationsLieux = vm.selectedFormateur.affectationsLieux;
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		/**
		* Met à jour la liste de formateur dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchFormateur [Le texte de la recherche]
		*/
		vm.searchFormateurChange = function(searchFormateur){
			if(vm.formateurs.length !== vm.formateursSave.length || vm.formateurs.length === 1){
				vm.formateurs = vm.formateursSave;
			}
			vm.formateurs = vm.formateurs.filter( createFilterFormateur(searchFormateur) );
		};

		/**
		* Met à jour la liste de compétence dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String competence [Le texte de la recherche]
		*/
		vm.selectedCompetenceChange = function(competence){
			console.log(vm.selectedFormateur);
			if(vm.selectedFormateur !== null){
				if(!angular.isDefined(_.find(vm.competencesFormateur, competence))){
					vm.competencesFormateur.push(competence);
					updateCompetencesFormateur();
				} else {
					Utils.viewToast("error", "Cette compétence est déjà associée au module sélectionné.");
				}
			} else {
				Utils.viewToast("warn", "Veuillez sélectionner un formateur.");
			}
		};

		vm.removeCompetence = function (competence) {
			if(angular.isDefined(_.find(vm.competencesFormateur, competence))){
				var index = _.indexOf(vm.competencesFormateur, competence);
				vm.competencesFormateur.splice(index, 1);
				updateCompetencesFormateur();
			}
		};

		vm.removeAffectation = function (affectation) {
			if(angular.isDefined(_.find(vm.affectationsLieux, affectation))){
				var index = _.indexOf(vm.affectationsLieux, affectation);
				vm.affectationsLieux.splice(index, 1);
				updateAffectationFormateur();
			}
		};

		/**
		* Verifie si le lieu est le premier dans la liste
		* @param  Lieu  lieu [Le lieu]
		* @return {Boolean}      [Si premier, return true]
		*/
		vm.isDefault = function(affectation){
			if(affectation.default === true){
				return true;
			}
		};

		/**
		* Enregistre les informations dans le formateur selectionne
		*/
		function updateCompetencesFormateur(){
			if(vm.selectedFormateur !== null){
				SettingsService.updateCompetenceFormateur(vm.selectedFormateur).then(function(response) {
					Utils.viewToast("success", "Succès : modification réussie!");
				})
				.catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			}
		}

		function updateAffectationFormateur(){
			if(vm.selectedFormateur !== null){
				SettingsService.updateAffectationFormateur(vm.selectedFormateur).then(function(response) {
					Utils.viewToast("success", "Succès : modification réussie!");
				})
				.catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			}
		}

		/**
		* Récupère les formateurs via le service
		*/
		SettingsService.getFormateurs().then(function(response) {
			if(response.status === 200){
				vm.formateurs = response.data;
				vm.formateursSave = vm.formateurs;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les lieux via le service
		*/
		SettingsService.getLieux().then(function(response) {
			if(response.status === 200){
				vm.lieux = response.data;
				vm.lieuxSave = vm.lieux;
				$scope.lieux = vm.lieux;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les compétences via le service
		*/
		SettingsService.getCompetences().then(function(response) {
			if(response.status === 200){
				vm.competences = response.data;
				vm.competencesSave = vm.competences;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.newAffectation = function (affectation) {
			vm.selectedAffectation = affectation;
			if(vm.selectedFormateur !== null){
				$mdDialog.show({
					clickOutsideToClose: true,
					parent: angular.element(document.body),
					escapeToClose: true,
					preserveScope: true,
					scope: $scope,
					locals : {
						lieux : vm.lieux,
						formateur : vm.selectedFormateur,
						affectation : 	vm.selectedAffectation
					},
					templateUrl: 'app/modules/settings/partial/affectation.tmpl.html',
					controller: function DialogController(SettingsService, $mdDialog, $scope, lieux, formateur, affectation)	{
						$scope.lieux = lieux;
						$scope.formateur = formateur;

						console.log($scope.formateur);
						$scope.identite = formateur.nom + " " + formateur.prenom;
						if(angular.isDefined(affectation)){
								$scope.newAffectation = affectation;
						}

						$scope.closeDialog = function() {
							delete $scope.formateur.affectationsLieux;
							$scope.newAffectation.formateur = $scope.formateur;
							console.log($scope.newAffectation.formateur);

							$scope.newAffectation.lieu = _.find($scope.lieux, $scope.newAffectation.lieu.id);
							console.log($scope.newAffectation.lieu);
							if($scope.newAffectation.default === true){
								$scope.newAffectation.preference = 0;
							}

							if(!angular.isDefined(_.find($scope.formateur.affectationsLieux, $scope.newAffectation))){
									$scope.newAffectation.id = 0;
									$scope.formateur.affectationsLieux = [];
									$scope.formateur.affectationsLieux.push($scope.newAffectation);
							}

							console.log($scope.formateur);

							SettingsService.updateAffectationFormateur($scope.formateur).then(function(response) {
								// if(response.status === 200){
									Utils.viewToast("success", "L'affectation a bien été créée !");
									// newAffectation = {};
								// } else {
								// 	Utils.viewToast("error", "Erreur :" + response.err + ". " + response.data);
								// }
							});
							$mdDialog.hide();
						};
					}
				});
			} else {
				Utils.viewToast("warn", "Veuillez sélectionner un formateur.");
			}
		};

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('HabilitationsCtrl', Habilitations);


	Habilitations.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope'];

	function Habilitations(SettingsService, Utils, $timeout, $q, $scope) {
		var vm = this;

		vm.formateurSelected = null;

		vm.formateurSelected = null;
		$scope.$watch(vm.searchFormateur, vm.searchFormateurChange);
		$scope.$watch(vm.searchModule, vm.searchModuleChange);

		vm.querySearch = function(query, searchType){
			var results;
			results = query ? vm.formateurs.filter( createFilterFormateur(query) ) : vm.formateurs;
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		/**
		* Filtre la liste de formateur par le nom et le prenom
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterFormateur(query) {
			return function filterFn(formateur) {
				if(formateur !== undefined){
					return (formateur.nom.toLowerCase().indexOf(query.toLowerCase()) === 0 || formateur.prenom.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}


		vm.getHumanRole = function (habil) {
			switch (habil) {
				case "admin":
				return "administrateur";
				case "resp_form":
				return "reponsable formation";
				case "form":
				return "formateur";
			}
		};

		vm.selectFormateur = function (formateur) {
			vm.formateurSelected = formateur;
			console.log(vm.formateurSelected);
		};

		vm.setHabilitation = function (habilitation) {
			if(vm.formateurSelected === null){
				Utils.viewToast("warn", "Veuillez selectionner un formateur.");
			} else {
				vm.formateurSelected.habilitation = habilitation;
				console.log(vm.formateurSelected);
				SettingsService.updateHabilitationFormateur(vm.formateurSelected).then(function (response) {
					if(response.status === 200){
						Utils.viewToast("success", "L'habilitation  a bien été affectée.");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}
		};

		/**
		* Met à jour la liste de formateur dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchFormateur [Le texte de la recherche]
		*/
		vm.searchFormateurChange = function(searchFormateur){
			if(vm.formateurs.length !== vm.formateursSave.length || vm.formateurs.length === 1){
				vm.formateurs = vm.formateursSave;
			}
			vm.formateurs = vm.formateurs.filter( createFilterFormateur(searchFormateur) );
		};

		SettingsService.getFormateurs().then(function(response) {
			if(response.status === 200){
				vm.formateurs = response.data;
				vm.formateursSave = vm.formateurs;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		SettingsService.getHabilitations().then(function(response) {
			if(response.status === 200){
				vm.habilitations = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('ModulesCtrl', Modules);

	Modules.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope', '$rootScope', '$mdDialog'];

	function Modules(SettingsService, Utils, $timeout, $q, $scope, $rootScope, $mdDialog) {
		var vm = this;

		$scope.$watch(vm.searchModule, vm.searchModuleChange);

		vm.querySearch = function(query, searchType){
			var results;
			if(searchType === "equipement"){
				results = query ? vm.equipements.filter( createFilterEquipement(query) ) : vm.equipements;
			} else if(searchType === "module" ){
				results = query ? vm.modules.filter( createFilterModule(query) ) : vm.modules;
			} else if(searchType === "competence" ){
				results = query ? vm.competences.filter( createFilterCompetence(query) ) : vm.competences;
			}
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		function createFilterModule(query) {
			return function filterFn(module) {
				if(module !== undefined){
					return (module.libelle.toLowerCase().indexOf(query.toLowerCase()) === 0 || module.libelleCourt.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		function createFilterEquipement(query) {
			return function filterFn(equipement) {
				if(equipement !== undefined){
					return (equipement.lot.toLowerCase().indexOf(query.toLowerCase()) === 0 || equipement.modele.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		function createFilterCompetence(query) {
			return function filterFn(competence) {
				if(competence !== undefined){
					return (competence.code.toLowerCase().indexOf(query.toLowerCase()) === 0 || competence.libelle.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		/**
		* Met à jour la liste de module dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchModule [Le texte de la recherche]
		*/
		vm.searchModuleChange = function(searchModule){
			if(vm.modules.length !== vm.modulesSave.length || vm.modules.length === 1){
				vm.modules = vm.modulesSave;
			}
			vm.modules = vm.modules.filter( createFilterModule(searchModule) );
		};

		/**
		* Met à jour la liste de compétence dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String competence [Le texte de la recherche]
		*/
		vm.selectedCompetenceChange = function(competence){
			if(!angular.isDefined(_.find(vm.competencesModule, competence))){
					vm.competencesModule.push(competence);
					updateModule();
			} else {
				Utils.viewToast("error", "Cette compétence est déjà associée au module sélectionné.");
			}
		};

		/**
		* Met à jour la liste d'équipement dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchEquipement [Le texte de la recherche]
		*/
		vm.selectedEquipementChange = function(equipement){
			if(!angular.isDefined(_.find(vm.equipementsModule, equipement))){
					vm.equipementsModule.push(equipement);
					updateModule();
			} else {
				Utils.viewToast("error", "Cet équipement est déjà associé au module sélectionné.");
			}
		};

		vm.removeCompetence = function (competence) {
			if(angular.isDefined(_.find(vm.competencesModule, competence))){
					var index = _.indexOf(vm.competencesModule, competence);
					vm.competencesModule.splice(index, 1);
					updateModule();
			}
		};

		vm.removeEquipement = function (equipement) {
			console.log("removeEquipement");
			if(angular.isDefined(_.find(vm.equipementsModule, equipement))){
					var index = _.indexOf(vm.equipementsModule, equipement);
					vm.equipementsModule.splice(index, 1);
					updateModule();
			}
		};

		vm.getModulesSpecs = function (module) {
			vm.selectedModule = module;
			vm.equipementsModule = module.equipements;
			vm.competencesModule = module.competences;
		};

		function updateModule() {
			SettingsService.updateModule(vm.selectedModule).then(function (response) {
				if(response.status === 200){
					Utils.viewToast("success", "Le module a bien été mis à jour !");
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		}

		/**
		* Récupère les modules via le service
		*/
		SettingsService.getModules().then(function(response) {
			if(response.status === 200){
				vm.modules = response.data;
				vm.modulesSave = vm.modules;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les équipements via le service
		*/
		SettingsService.getEquipements().then(function(response) {
			if(response.status === 200){
				vm.equipements = response.data;
				vm.equipementsSave = vm.equipements;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		/**
		* Récupère les compétences via le service
		*/
		SettingsService.getCompetences().then(function(response) {
			if(response.status === 200){
				vm.competences = response.data;
				vm.competencesSave = vm.competences;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.newCompetence = function (event) {
			$mdDialog.show({
				clickOutsideToClose: true,
				escapeToClose: true,
				preserveScope: true,
				scope: $scope,
				templateUrl: 'app/modules/settings/partial/competences.tmpl.html',
				controller: function DialogController(SettingsService, $mdDialog, $scope)	{
					$scope.closeDialog = function() {
						SettingsService.createCompetence($scope.newCompetence).then(function(response) {
							if(response.status === 201){
								Utils.viewToast("success", "La compétence a bien été créée !");
								SettingsService.getCompetences().then(function(response) {
									if(response.status === 200){
										vm.competences = response.data;
										vm.competencesSave = vm.competences;
									} else {
										Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
									}
								});
							} else {
								Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
							}
						});
						$mdDialog.hide();
					};
				}
			});
		};
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('ParametresCtrl', Parametres);


	Parametres.$inject = ['SettingsService', 'Utils'];

	function Parametres(SettingsService, Utils) {
		var vm = this;

		SettingsService.getParams().then(function(response) {
			if(response.status === 200){
				vm.params = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.save = function (params) {
			_.forEach(params, function (param) {
				SettingsService.updateParams(param).then(function (response) {
						if(response.status === 200){
							Utils.viewToast("success", "Le paramétrage a bien été mis à jour !");
							SettingsService.getParams().then(function(response) {
								if(response.status === 200){
									vm.params = response.data;
								} else {
									Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
								}
							});
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					});
			});
		};

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('PromotionsCtrl', Promotions);

	Promotions.$inject = ['SettingsService', 'Utils', '$timeout', '$q', '$scope'];

	function Promotions(SettingsService, Utils, $timeout, $q, $scope) {
		var vm = this;

		vm.promotionSelected = null;

		vm.promotionSelected = null;
		$scope.$watch(vm.searchPromotion, vm.searchPromotionChange);

		vm.querySearch = function(query, searchType){
			var results;
			results = query ? vm.promotions.filter( createFilterPromotion(query) ) : vm.promotions;
			var deferred = $q.defer();
			$timeout(function () { deferred.resolve( results ); }, Math.random() * 100, false);
			return deferred.promise;
		};

		/**
		* Filtre la liste de promotion par le nom et le prenom
		* @param String query [Le texte de la recherche]
		* @return Array       [le array correspondant au critère]
		*/
		function createFilterPromotion(query) {
			return function filterFn(promotion) {
				if(promotion !== undefined){
					return (promotion.code.toLowerCase().indexOf(query.toLowerCase()) === 0);
				}
			};
		}

		vm.selectPromotion = function (promotion) {
			vm.promotionSelected = promotion;
			console.log(vm.promotionSelected);
		};

		vm.setLieu = function (lieu) {
			if(vm.promotionSelected === null){
				Utils.viewToast("warn", "Veuillez selectionner une promotion.");
			} else {
				vm.promotionSelected.lieu = lieu;
				console.log(vm.promotionSelected);
				SettingsService.updatePromotion(vm.promotionSelected).then(function (response) {
					if(response.status === 200){
						Utils.viewToast("success", "La promotion a bien été mise à jour !");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}
		};

		/**
		* Met à jour la liste de promotion dans la md-list selon le filtre appliqué dans l'Autocomplete
		* @param  String searchpromotion [Le texte de la recherche]
		*/
		vm.searchPromotionChange = function(searchPromotion){
			if(vm.promotions.length !== vm.promotionsSave.length || vm.promotions.length === 1){
				vm.promotions = vm.promotionsSave;
			}
			vm.promotions = vm.promotions.filter( createFilterPromotion(searchPromotion) );
		};

		SettingsService.getPromotions().then(function(response) {
			if(response.status === 200){
				vm.promotions = response.data;
				vm.promotionsSave = vm.promotions;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		SettingsService.getLieux().then(function(response) {
			if(response.status === 200){
				vm.lieux = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('SallesCtrl', Salles);

	Salles.$inject = ['SettingsService', 'Utils'];

	function Salles(SettingsService, Utils) {
		var vm = this;

		var lieuSelected;
		vm.materielSelected = false;
		vm.contentTitle = "Equipements présents dans la salle";

		SettingsService.getLieux().then(function(response) {
			if(response.status === 200){
				vm.lieux = response.data;
			} else {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			}
		});

		vm.isDisponible = function (salle, disponible) {
			salle.disponibilite = !disponible;
			SettingsService.updateDispoSalle(salle).then(function(response) {
				if(response.status === 200){
					Utils.viewToast("success", "La salle a bien été mise à jour !");
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		vm.selectSalles = function(lieu){
			lieuSelected = lieu;
			SettingsService.getSallesByLieu(lieu).then(function(response) {
				if(response.status === 200){
					vm.salles = response.data;
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
			vm.materiels = [];
		};

		vm.selectMateriels = function(salle){
			vm.contentTitle = "Equipements présents dans la salle";
			vm.selectedSalle = salle;
			vm.materiels = salle.equipements;
			vm.isSalleSelected = true;
			vm.isMaterielSelected = false;
			vm.isNew = false;
			vm.materielSelected = {};
			showDivider(vm.materiels.length);
		};

		function showDivider(size){
			if(size > 1) {
				vm.showDivider = true;
			}
		}

		vm.selectMateriel = function(materiel){
			vm.contentTitle = "Modifier l'équipement";
			vm.isMaterielSelected = true;
			vm.materielSelected = materiel;
			vm.materielSelected.finGarantie = new Date(materiel.finGarantie);
			vm.materielSelected.dernierNettoyage = new Date(materiel.dernierNettoyage);
		};

		vm.addMateriel = function(){
			if(angular.isDefined(vm.selectedSalle)){
				vm.contentTitle = "Ajouter un équipement";
				vm.isMaterielSelected = true;
				vm.isNew = true;
				vm.materielSelected = {};
			} else {
				Utils.viewToast("warn", "Veuillez sélectionner une salle.");
			}
		};

		vm.deleteEquipement = function (equipement) {
			SettingsService.deleteEquipement(equipement).then(function (response) {
				if(response.status === 200){
					vm.selectedSalle.equipements.splice(equipement, 1);
					Utils.viewToast("success", "L'équipement a bien été supprimé !");
				} else {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				}
			});
		};

		vm.save = function(toSave, isNew){
			if(isNew === true){
				if(_.indexOf(vm.selectedSalle.equipements, toSave) === -1){
						vm.selectedSalle.equipements.push(toSave);
				}
				SettingsService.updateSalle(vm.selectedSalle).then(function(response) {
					if(response.status === 200){
						vm.selectSalles(lieuSelected);
						Utils.viewToast("success", "L'équipement a bien été ajouté !");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			} else {
				SettingsService.updateSalle(vm.selectedSalle).then(function(response) {
					if(response.status === 200){
						Utils.viewToast("success", "L'équipement a bien été modifié !");
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				});
			}
		};
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('SidenavCtrl', Sidenav);


	Sidenav.$inject = ['SettingsService', '$state', '$mdSidenav'];

	function Sidenav(SettingsService, $state, $mdSidenav) {
		var vm = this;

		vm.navigateTo = function(to) {
			$state.go('home.settings.' + to);
		};

		vm.tabMenu = SettingsService.getMenu();
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:settingsCtrl
	* @description
	* # settingsCtrl
	* Controller of the app
	*/

	angular
	.module('settings')
	.controller('SettingsCtrl', Settings);

	Settings.$inject = ['SettingsService', '$mdSidenav', 'Utils'];

	function Settings(SettingsService, $mdSidenav, Utils) {
		/*jshint validthis: true */
		var vm = this;

		vm.init = {
			pageTitle: "Navigation",
		};

		vm.toggleSidenav = function(){
			$mdSidenav('left').toggle();
		};

		vm.isOpenLeft = function(){
			return $mdSidenav('left').isOpen();
		};

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:sidenavplanningCtrl
	* @description
	* # sidenavplanningCtrl
	* Controller of the app
	*/

	angular
		.module('planning')
		.controller('SideNavPlanningCtrl', SideNavPlanning);

		SideNavPlanning.$inject = ['$mdSidenav','$log','homeService','$rootScope','$stateParams','$state','planningService','Utils'];

		function SideNavPlanning($mdSidenav,$log,homeService,$rootScope,$stateParams,$state,planningService,Utils) {

			var vm = this;
			vm.alerts = [];
			vm.salles = [];
			vm.formateurs = [];

			vm.formateurSelected = {};
			vm.salleSelected = {};
			vm.coursSelected = {};

			/** on récupère les salles et formateurs de la base de données,
			 * puis les appels suivants iront les récupérer en cache. */
			$rootScope.$watch('lessonSelected',function(newValue,oldValue){
				console.log(newValue);
				vm.formateurSelected = {};
				vm.salleSelected = {};
				vm.lessons = [];
				if(newValue.hasOwnProperty('id')){
					homeService.getFormateurs().then(function(response){
						if(response.status === 200) {
							vm.formateurs = response.data;
						}
					}).then(function(){
						homeService.getSalles().then(function(response){
							if(response.status === 200){
								vm.salles = response.data;
							}
						});
					}).then(function(){
						vm.title = newValue.module.libelleCourt;

						if(newValue.otherLessonsDuringWeek){
							_.each(newValue.otherLessonsDuringWeek,function(otherLessonsDuringWeek){
								vm.lessons.push(otherLessonsDuringWeek);
							});
							vm.lessons = _.sortBy(vm.lessons,function(lesson){
								return lesson.module.libelleCourt;
							});
						}
						vm.lessons.push(newValue);
						vm.alerts = getAlerts(newValue);
						if(newValue && newValue.formateur){
							vm.formateurSelected = newValue.formateur.idFormateur;
						}
						if(newValue && newValue.salle){
							vm.salleSelected = newValue.salle.code;
						}
					});
				}
			});

			vm.updateLesson = function(){
				var formateur = _.find(vm.formateurs,function(formateur){
					return formateur.idFormateur == vm.formateurSelected;
				});
				var salle = _.find(vm.salles,function(salle){
					return salle.code === vm.salleSelected;
				});
				var cours = _.find(vm.lessons,function(c){
					return c.id = vm.coursSelected;
				});	

				if(cours.hasOwnProperty('id')) {
					cours.salle = salle;
					cours.formateur = formateur;

					delete cours.week;
					delete cours.impossible;
					delete cours.alerts;
					delete cours.otherLessonsDuringWeek;

					planningService.updateCours(cours);
					vm.close();
					$rootScope.lessonSelected = {};

					if ($rootScope.data.filtersSelected.viewPlanning == 'salle'
						|| $rootScope.data.filtersSelected.viewPlanning == 'formateur') {
						$state.go('home.planning', {
							dateStart: $stateParams.dateStart,
							dateEnd: $stateParams.dateEnd
						}, {reload: true});
					}
				}else{
					Utils.viewToast("error", "Le cours n'a pas été sélectionné");
				}
			};

			vm.init = function(){

			};

			/** récupération des alertes pour le cours sélectionné. */
			function getAlerts(lesson){
				var alerts = [];
				if(lesson.alerts){
					_.find(lesson.alerts, function (alert) {
						if(alert.code === 'unavailableTrainer') {
							if (lesson.module.id != alert.lesson.module.id) {
								if ((((moment(alert.lesson.debut).week() - 1) >= lesson.week)
									&& (moment(alert.lesson.fin).week() - 1) <= (moment(lesson.fin).week() - 1))
									|| (moment(alert.lesson.debut).week() - 1) == (moment(lesson.fin).week() - 1)) {
									alerts.push(alert);
								}
							}
						}else {
							alerts.push(alert);
						}
					});
				}
				return alerts;
			}

			vm.close = function () {
				$mdSidenav('right').close()
					.then(function () {
					});
			};
		}
})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:timesheetCtrl
	* @description
	* # timesheetCtrl
	* Controller of the app
	*/

	angular
	.module('timesheet')
	.controller('TimesheetCtrl', Timesheet);

	Timesheet.$inject = [];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function Timesheet() {
		/*jshint validthis: true */
		var vm = this;

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:userCtrl
	* @description
	* # userCtrl
	* Controller of the app
	*/

	angular
	.module('user')
	.controller('UserCtrl', User);

	User.$inject = [];

	/*
	* recommend
	* Using function declarations
	* and bindable members up top.
	*/

	function User() {
		/*jshint validthis: true */
		var vm = this;

	}

})();

(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.controller:vacationrequestCtrl
	 * @description
	 * # vacationrequestCtrl
	 * Controller of the app
	 */

	angular
		.module('vacationrequest')
		.controller('AddDialogController', AddDialogController)
		.controller('ValidationDialogController', ValidationDialogController)
		.controller('VacationrequestCtrl', Vacationrequest);

	Vacationrequest.$inject = ['VacationrequestService', 'Utils', '_', '$mdDialog', '$scope', '$rootScope'];
	AddDialogController.$inject = ['VacationrequestService', 'Utils', '$mdDialog', '$scope'];
	ValidationDialogController.$inject = ['VacationrequestService', 'Utils', '$mdDialog', '$scope'];

	/*
	 * recommend
	 * Using function declarations
	 * and bindable members up top.
	 */

	function Vacationrequest(VacationrequestService, Utils, _, $mdDialog, $scope, $rootScope) {
		/*jshint validthis: true */
		var vm = this;

		vm.role = $rootScope.account.userRoles;

		vm.isAdmin = function () {
			return (vm.role.libelle === 'admin');
		};

		vm.query = {
			order: 'dateDebutConge',
			reverse: false,
			limit: 10,
			page: 1
		};

		vm.etatEnum = {
			ALL: {intituleUrl :'all', description: 'tout', etatConge:false, valeurEnum:'ALL'},
			ACTIF: {intituleUrl :'actif', description: 'actif', etatConge:false, valeurEnum:'ACTIF'},
			VALIDE: {intituleUrl :'valide', description: 'Validé', etatConge:true, valeurEnum:'VALIDE'},
			EN_ATTENTE: {intituleUrl :'attente', description: 'En attente', etatConge:true, valeurEnum:'EN_ATTENTE'},
			REFUSE: {intituleUrl :'refuse', description: 'Refusé', etatConge:true, valeurEnum:'REFUSE'}
		};
		vm.getDescriptionEtat = function (etat) {
			switch(etat) {
				case vm.etatEnum.ALL.valeurEnum:
					return vm.etatEnum.ALL.description;
				case vm.etatEnum.ACTIF.valeurEnum:
					return vm.etatEnum.ACTIF.description;
				case vm.etatEnum.VALIDE.valeurEnum:
					return vm.etatEnum.VALIDE.description;
				case vm.etatEnum.EN_ATTENTE.valeurEnum:
					return vm.etatEnum.EN_ATTENTE.description;
				case vm.etatEnum.REFUSE.valeurEnum:
					return vm.etatEnum.REFUSE.description;
				default:
					return 'Etat inconnu'
			}
		};


		vm.dateDuJour = new Date();
		vm.filtreDateDebut = new Date((new Date()).setHours(0, 0, 0));
		vm.filtreDateFin = new Date(new Date((new Date()).setYear((new Date()).getFullYear()+1)).setHours(23, 59, 59));
		vm.getFiltreDateDebut = function() {
			return vm.filtreDateDebut;
		};
		vm.getFiltreDateFin = function() {
			return vm.filtreDateFin;
		};

		vm.refreshHolidays = function () {
			if (vm.isAdmin()) {
				VacationrequestService.getTimeOff(
					vm.getFiltreDateDebut().toISOString(),
					vm.getFiltreDateFin().toISOString(),
					null,
					null
				).then(function(response) {
					if (response.status == 200) {
						vm.holidays = response.data;
					} else {
						vm.holidays = [];
						if (response.status == 409) {
							Utils.viewToast("warn", response.data.message);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				}).catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			} else {
				VacationrequestService.getTimeOff(
					vm.getFiltreDateDebut().toISOString(),
					vm.getFiltreDateFin().toISOString(),
					$rootScope.account.id,
					null
				).then(function(response) {
					if (response.status == 200) {
						vm.holidays = response.data;
					} else {
						vm.holidays = [];
						if (response.status == 409) {
							Utils.viewToast("warn", response.data.message);
						} else {
							Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
						}
					}
				}).catch(function(error) {
					Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
				});
			}
		};
		vm.refreshHolidays();



		vm.getHolidays = function () {
			$scope.promise = vm.holidays.get($scope.query, success).$promise;
		};

		vm.hasHolidays = function () {
			return vm.holidays.length > 0;
		};

		function success(holidays) {
			vm.holidays = holidays;
		}

		vm.newTimeOff = {
			dateStart: '',
			hourStart: 0,
			minuteStart: 0,
			getDateTimeStart: function() {
				if (vm.newTimeOff.hasOwnProperty('dateStart')
					&& vm.newTimeOff.dateStart != null
					&& vm.newTimeOff.dateStart != ''
				) {
					if (vm.newTimeOff.hasOwnProperty('hourStart')
					    && vm.newTimeOff.hasOwnProperty('minuteStart')
						&& Number.isInteger(vm.newTimeOff.hourStart)
						&& Number.isInteger(vm.newTimeOff.hourStart) > -1
						&& Number.isInteger(vm.newTimeOff.hourStart) < 24
						&& Number.isInteger(vm.newTimeOff.minuteStart)
						&& Number.isInteger(vm.newTimeOff.minuteStart) > -1
						&& Number.isInteger(vm.newTimeOff.minuteStart) < 60
					) {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateStart).year(),
								moment(vm.newTimeOff.dateStart).month(),
								moment(vm.newTimeOff.dateStart).date(),
								vm.newTimeOff.hourStart,
								vm.newTimeOff.minuteStart,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					} else {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateStart).year(),
								moment(vm.newTimeOff.dateStart).month(),
								moment(vm.newTimeOff.dateStart).date(),
								0,
								0,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					}
				} else {
					return null;
				}
			},
			dateEnd: '',
			hourEnd: 0,
			minuteEnd: 0,
			getDateTimeEnd: function() {
				if (vm.newTimeOff.hasOwnProperty('dateEnd')
					&& vm.newTimeOff.dateEnd != null
					&& vm.newTimeOff.dateEnd != '') {
					if (vm.newTimeOff.hasOwnProperty('hourEnd')
						&& vm.newTimeOff.hasOwnProperty('minuteEnd')
						&& Number.isInteger(vm.newTimeOff.hourEnd)
						&& Number.isInteger(vm.newTimeOff.hourEnd) > -1
						&& Number.isInteger(vm.newTimeOff.hourEnd) < 24
						&& Number.isInteger(vm.newTimeOff.minuteEnd)
						&& Number.isInteger(vm.newTimeOff.minuteEnd) > -1
						&& Number.isInteger(vm.newTimeOff.minuteEnd) < 60
					) {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateEnd).year(),
								moment(vm.newTimeOff.dateEnd).month(),
								moment(vm.newTimeOff.dateEnd).date(),
								vm.newTimeOff.hourEnd,
								vm.newTimeOff.minuteEnd,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					} else {
						return moment(
							new Date(
								moment(vm.newTimeOff.dateEnd).year(),
								moment(vm.newTimeOff.dateEnd).month(),
								moment(vm.newTimeOff.dateEnd).date(),
								0,
								0,
								0
							)
						).format('YYYY-MM-DDTHH:mm');
					}
				} else {
					return null;
				}
			},
			motif: '',
			formateur: {
				formateurId : $rootScope.account.id,
				nom: $rootScope.account.lastName,
				prenom: $rootScope.account.firstName,
				mail: $rootScope.account.email,
				username: $rootScope.account.login
			}
		};

		// affichage de l'écran de dialogue d'ajout d'une demande de congé
		vm.showAddVacationrequestDialog = function (event) {
			$mdDialog.show({
				clickOutsideToClose: true,
				escapeToClose: true,
				preserveScope: true,
				scope: $scope,
				templateUrl: 'app/modules/vacationrequest/addVacationrequestDialog.html',
				controller: AddDialogController(VacationrequestService, Utils, $mdDialog, $scope, vm)
			});
		};

		vm.showValidationVacationrequestDialog = function (conge, event) {
			vm.inValidationConge = conge;
			$mdDialog.show({
				clickOutsideToClose: true,
				escapeToClose: true,
				preserveScope: true,
				scope: $scope,
				templateUrl: 'app/modules/vacationrequest/validationVacationrequestDialog.html',
				controller: ValidationDialogController(VacationrequestService, Utils, $mdDialog, $scope, vm)
			});
		};

		vm.searchText = '';
		vm.filterByFormateur = function(holiday) {
			if (holiday != null && holiday.formateur != null) {
				return ((holiday.formateur.nom.toLowerCase().indexOf(vm.searchText.toLowerCase()) !== -1)
				|| (holiday.formateur.prenom.toLowerCase().indexOf(vm.searchText.toLowerCase()) !== -1));
			} else {
				return false;
			}
		};

		vm.getCongesFiltres = function() {
			if (vm.isAdmin()) {
				var conges = [];
				_.forEach(vm.holidays, function (value) {
					if (vm.filterByFormateur(value)) {
						conges.push(value);
					}
				});
				return conges;
			} else {
				return vm.holidays;
			}
		}
	}

	function AddDialogController(VacationrequestService, Utils, $mdDialog, $scope, vm)
	{
		$scope.closeDialog = function() {
			$mdDialog.hide();
		};

		$scope.cancelDialog = function() {
			$mdDialog.cancel();
		};

		$scope.validDialog = function() {
			var conge = VacationrequestService.createTimeOff (
				VacationrequestService.getJSONTimeOff (
					null,
					vm.newTimeOff.getDateTimeStart(),
					vm.newTimeOff.getDateTimeEnd(),
					vm.newTimeOff.motif,
					null,
					null,
					vm.newTimeOff.formateur
				)
			).then(function(response) {
				if (response.status == 200 || response.status == 201) {
					Utils.viewToast("success", "Succès : création du congé réussie!");
				} else {
					if (response.status == 409) {
						Utils.viewToast("warn", response.data.message);
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				}
				vm.refreshHolidays();
			}).catch(function(error) {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			});

			// vm.holidays.push(conge);
			$scope.closeDialog();
		};
	}

	function ValidationDialogController(VacationrequestService, Utils, $mdDialog, $scope, vm)
	{
		$scope.closeDialog = function() {
			$mdDialog.hide();
		};

		$scope.refuseCongeDialog = function() {
			$scope.validation(vm.etatEnum.REFUSE.valeurEnum);
			$scope.closeDialog();
		};

		$scope.validCongeDialog = function() {
			$scope.validation(vm.etatEnum.VALIDE.valeurEnum);
			$scope.closeDialog();
		};

		$scope.validation = function(newEtat) {
			VacationrequestService.updateTimeOff(
				VacationrequestService.getJSONTimeOff (
					vm.inValidationConge.idConge,
					vm.inValidationConge.dateDebutConge,
					vm.inValidationConge.dateFinConge,
					vm.inValidationConge.motif,
					newEtat,
					vm.inValidationConge.dateDemandeConge,
					vm.newTimeOff.formateur
			)).then(function(response) {
				if (response.status == 200 || response.status == 201) {
					Utils.viewToast("success", "Succès : traitement de la demande de congé réussi!");
				} else {
					if (response.status == 409) {
						Utils.viewToast("warn", response.message);
					} else {
						Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
					}
				}
				vm.refreshHolidays();
			}).catch(function(error) {
				Utils.viewToast("error", "Erreur :" + response.status + ". " + response.data);
			});
		};
	}
})();

(function () {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:homeService
	* @description
	* # homeService
	* Service of the app
	*/

	angular.module('opale')
	.factory('homeService', homeService);

	homeService.$inject = ['$http','config', 'Dao','cache', 'LoginService', 'USER_ROLES'];

		function homeService($http, config, Dao, cache, LoginService, USER_ROLES) {
			var menu = [
				{
					link: 'dashboard',
					name: 'Dashboard',
					icon: 'apps',
					tooltip: 'Tableau de bord',
					authorizedRoles: [USER_ROLES.all]
				},

				{
					link: 'timesheet',
					name: 'Timesheet',
					icon: 'assignment',
					tooltip: 'Fiche de temps',
					authorizedRoles: [USER_ROLES.all]
				},

				{
					link: 'vacationrequest',
					name: 'Vacationrequest',
					icon: 'access_time',
					tooltip: 'Congés',
					authorizedRoles: [USER_ROLES.all]
				},

				{
					link: 'planning',
					name: 'Planning',
					icon: 'event',
					tooltip: 'Planning',
					authorizedRoles: [USER_ROLES.all]
				},
				{
					link: 'settings',
					name: 'Settings',
					icon: 'settings',
					tooltip: 'Paramétrage',
					authorizedRoles: [USER_ROLES.admin]
				},

			];

			function getNotifications(){
				return Dao.crudData("get", config.apiUrl + "/notifications");
				// return Dao.crudData("get", "app/mock/notification.json");
			}

			function getFormations(){
				return Dao.crudData("get", config.apiUrl + "/formations");
			}

			function getPromotions(dateStart,dateEnd){
				return Dao.crudData("get", config.apiUrl + "/promotions/"+ dateStart + "/" + dateEnd);
			}

			function getLieux(){
				return Dao.crudData("get", config.apiUrl + "/lieux");
			}

			function getParametres(){
				return Dao.crudData("get",config.apiUrl + "/parametrages");
			}

			function getAlertes(){
				return Dao.crudData("get",config.apiUrl + "/alertes");
			}

			function getFormateurs(){
				return Dao.crudData("get", config.apiUrl + "/formateurs");
			}

			function getSalles(){
				return Dao.crudData("get", config.apiUrl + "/salles");
			}


			function syncData(){
				return Dao.crudData("get", config.apiUrl + "/etl-load");
			}

			function logout() {
				LoginService.logout();
			}

			function getMenuList() {
				return menu;
			}

			return {
				getMenuList: getMenuList,
				getNotifications: getNotifications,
				getFormations: getFormations,
				getPromotions: getPromotions,
				getLieux : getLieux,
				getSalles : getSalles,
				getFormateurs : getFormateurs,
				getAlertes : getAlertes,
				getParametres : getParametres,
				syncData: syncData,
				logout: logout
			};
		}

})();

(function() {
   'use strict';

   /**
   * @ngdoc function
   * @name app.service:loginService
   * @description
   * # loginService
   * Service of the app
   */

   angular
   .module('login')
   .factory('LoginService', Login)
   .factory('Session', Session);

   Login.$inject = ['config', 'Dao', '$http', '$state', '$rootScope', '$resource', 'authService', 'Session', '$cookies'];
   Session.$inject = ['$cookies'];

   function Login (config, Dao, $http, $state, $rootScope, $resource, authService, Session, $cookies) {
      return {
         login : login,
         getAccount : getAccount,
         isAuthorized : isAuthorized,
         logout : logout,
      };

      function login (credentials) {
         var headers = credentials ? {authorization : "Basic " + btoa(credentials.user + ":" + credentials.pass)} : {};
         //return $http.get("app/mock/userAdmin.json", {headers : headers})
         // return $http.get("app/mock/userForm.json", {headers : headers})
          return $http.get(config.apiUrl + '/user/account', {headers : headers})
         .then(function (response, status, headers, config) {
            sessionStorage.setItem("auth", btoa(credentials.user + ":" + credentials.pass));
            authService.loginConfirmed(response.data);
            return {status: response.status, data: response.data};
         }).catch(function (response, status, headers, config) {
            $rootScope.authenticationError = true;
            Session.invalidate();
            return {err: status, data: response.data};
         });
      }

      function getAccount() {
         $rootScope.loadingAccount = true;
         // var headers = {authorization : "Basic " + sessionStorage.getItem("auth")};
         if(sessionStorage.getItem("auth")){
            //$http.get("app/mock/userForm.json")
            //$http.get("app/mock/userAdmin.json")
			 $http.get(config.apiUrl + '/user/account')
            .then(function (response) {
               authService.loginConfirmed(response.data);
            });
         } else {
            authService.loginCancelled();
         }
      }

      function isAuthorized(authorizedRoles) {
         if (!angular.isArray(authorizedRoles)) {
            if (authorizedRoles === '*') {
               return true;
            }
            authorizedRoles = [authorizedRoles];
         }
         var isAuthorized = false;
         angular.forEach(authorizedRoles, function (authorizedRole) {
            var authorized = null;
            if(angular.isDefined(Session.userRoles)){
               authorized = Session.userRoles.libelle;
            }

            if (authorized === authorizedRole || authorizedRole === '*') {
               isAuthorized = true;
            }
         });
         return isAuthorized;
      }

      function logout() {
         $rootScope.authenticationError = false;
         $rootScope.authenticated = false;
         $rootScope.account = null;
         $http.get(config.apiUrl + '/logout');
         Session.invalidate();
         authService.loginCancelled();
      }

   }

   function Session ($cookies) {
      var vm = this;
      vm.create = function (data) {
         this.id = data.idFormateur;
         this.login = data.username;
         this.firstName = data.nom;
         this.lastName = data.prenom;
         this.email = data.mail;
         this.userRoles = data.habilitation;
      };
      vm.invalidate = function () {
         this.id = null;
         this.login = null;
         this.firstName = null;
         this.lastName = null;
         this.email = null;
         this.userRoles = null;
         sessionStorage.removeItem("auth");
         $cookies.remove("XSFR-TOKEN");
         $cookies.remove("JSESSIONID");
      };
      return vm;
   }

})();

(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name app.service:planningService
	 * @description
	 * # planningService
	 * Service of the app
	 */

  	angular
		.module('planning')
		.factory('planningService', Planning);
		// Inject your dependencies as .$inject = ['$http', 'someSevide'];
		// function Name ($http, someSevide) {...}

		Planning.$inject = ['$http','config','Dao', 'cache'];

		function Planning ($http,config,Dao, cache) {

			var alerts = {
				capacity: 'La capacité de la salle est insuffisante',
				occuped: 'La salle est déjà occupée pour cette période ',
				unavailability: 'La salle est indisponible',
				equipments: 'La salle ne dispose pas des équipements nécessaires pour réaliser le module ',
				externalSpeaker: 'L\'intervenant externe n\'a pas confirmé',
				unavailableTrainer: 'Le formateur n\'est pas disponible pour le module ',
				movement: 'Le formateur a réalisé trop de déplacements',
				beginner: 'Le formateur est débutant sur le module ',
				missingSkills: 'Le formateur n\'a pas les compétences requises pour dispenser le cours '
			};


			/**
			 * Use those functions
			 * to configure
			 * the application
			 */
			function getLessons(interval){
				return Dao.crudData("get", config.apiUrl + "/planning/" + interval.dateStart + "/" + interval.dateEnd);
			}

			function getExperiencesByFormateurAndModule(formateur,module){
				return Dao.crudData("get", config.apiUrl + "/formateur/cours/experiences/" + formateur + "/" + module);
			}

			function getLessonsByFormateur(idFormateur,interval){
				return Dao.crudData("get", config.apiUrl + "/formateur/cours/" + idFormateur + "/" + interval.dateStart + "/" + interval.dateEnd);
			}

			function getDeplacementsFormateur(idFormateur){
				return Dao.crudData("get", config.apiUrl + "/formateur/deplacementsExterne/" + idFormateur);
			}

			function getConges(interval){
				return Dao.crudData("get", config.apiUrl + "/conges/" + interval.dateStart + "/" + interval.dateEnd);
			}

			function getAlertesPlanningList(){
				return alerts;
			}

			function updateCours(cours){
				console.log(config.apiUrl + "/cours", cours);
				return Dao.crudData("put", config.apiUrl + "/cours", cours);
			}

			return {
				updateCours:updateCours,
				getAlertesPlanningList:getAlertesPlanningList,
				getLessons: getLessons,
				getExperiencesByFormateurAndModule: getExperiencesByFormateurAndModule,
				getLessonsByFormateur:getLessonsByFormateur,
				getDeplacementsFormateur:getDeplacementsFormateur,
				getConges: getConges
			};
		}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:notificationsService
	* @description
	* # notificationsService
	* Service of the app
	*/

	angular
	.module('profile')
	.factory('ProfileService', Profile);
	// Inject your dependencies as .$inject = ['$http', 'someSevide'];
	// function Name ($http, someSevide) {...}

	Profile.$inject = ['$http'];

	function Profile ($http) {

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:settingsService
	* @description
	* # settingsService
	* Service of the app
	*/

	angular
	.module('settings')
	.factory('SettingsService', Settings);

	Settings.$inject = ['$http', 'config', 'Dao', 'cache'];

	function Settings ($http, config, Dao, cache) {

		var menu = [
			{
				name: "Application",
				items: [
					{
						name: "Alertes",
						url: "alertes"
					},
					{
						name: "Paramètres",
						url: "params"
					},
					{
						name: "Habilitations",
						url: "habilitations"
					},
					{
						name: "Traces applicatives",
						url: "pisteaudit"
					}
				]
			},
			{
				name: "Ressources",
				items: [
					{
						name: "Formateurs",
						url: "formateurs"
					},
					{
						name: "Modules",
						url: "modules"
					},
					{
						name: "Salles",
						url: "salles"
					},
					{
						name: "Promotion",
						url: "promotions"
					},
				]
			},
		];

		var settings = {
			getMenu : getMenu,
			getAlerts : getAlerts,
			getParams: getParams,
			getLieux : getLieux,
			getPisteAudit : getPisteAudit,
			getSallesByLieu : getSallesByLieu,
			getFormateurs : getFormateurs,
			getFormations : getFormations,
			getHabilitations : getHabilitations,
			getPromotions: getPromotions,
			updateHabilitationFormateur : updateHabilitationFormateur,
			getAffectationByFormateur : getAffectationByFormateur,
			getModules : getModules,
			updateModule : updateModule,
			getEquipements : getEquipements,
			getCompetences : getCompetences,
			deleteEquipement: deleteEquipement,
			updateSalle : updateSalle,
			updatePromotion : updatePromotion,
			updateDispoSalle : updateDispoSalle,
			updateParams: updateParams,
			updateCompetenceFormateur : updateCompetenceFormateur,
			updateAffectationFormateur : updateAffectationFormateur,
			createCompetence : createCompetence
		};

		return settings;

		function getMenu() {
			return menu;
		}

		function getAlerts(){
			return Dao.crudData("get", config.apiUrl + "/alertes");
		}

		function getParams(){
			return Dao.crudData("get", config.apiUrl + "/parametrages");
		}

		function getLieux(){
			return Dao.crudData("get", config.apiUrl + "/lieux");
		}

		function getPisteAudit(){
			// return Dao.crudData("get", config.apiUrl + "/pisteaudit");
			return Dao.crudData("get", "app/mock/traces.json");
		}

		function getSallesByLieu(lieu){
			return Dao.crudData("get", config.apiUrl + "/salles/" + lieu +"/");
		}

		function getFormateurs(){
			return Dao.crudData("get", config.apiUrl + '/formateurs');
		}

		function getFormations() {
			return Dao.crudData("get", config.apiUrl + '/formations');
		}

		function getHabilitations(){
			return Dao.crudData("get", config.apiUrl + "/habilitations");
		}

		function getModules(){
			return Dao.crudData("get", config.apiUrl + '/modules');
		}

		function getPromotions(){
			return Dao.crudData("get", config.apiUrl + '/promotions');
		}

		function getAffectationByFormateur(idFormateur){
			return Dao.crudData("get", config.apiUrl + '/formateurs/' + idFormateur + '/affectations');
		}

		function updateAffectationFormateur(formateur){
			return Dao.crudData("put", config.apiUrl + "/formateurs/" + formateur.idFormateur + '/affectation', formateur);
		}


		function updateModule(module){
			return Dao.crudData("put", config.apiUrl + "/modules/" + module.id, module);
		}

		function getEquipements(){
			return Dao.crudData("get", config.apiUrl + '/equipements');
		}

		function getCompetences(){
			return Dao.crudData("get", config.apiUrl + '/competences');
		}

		function deleteEquipement(equipement){
			return Dao.crudData("delete", config.apiUrl + "/equipements/" + equipement.id);
		}

		function updatePromotion(promotion){
			return Dao.crudData("put", config.apiUrl + "/promotion", promotion);
		}

		function updateSalle(salle){
			return Dao.crudData("put", config.apiUrl + "/salle", salle);
		}

		function updateDispoSalle(salle){
			return Dao.crudData("put", config.apiUrl + "/salle/indisponible", salle);
		}

		function updateParams(parametrage){
			return Dao.crudData("put", config.apiUrl + "/parametrage/" + parametrage.code, parametrage);
		}

		function updateHabilitationFormateur(formateur){
			return Dao.crudData("put", config.apiUrl + "/formateurs/" + formateur.idFormateur + "/habilitation", formateur);
		}

		function updateCompetenceFormateur(formateur){
			return Dao.crudData("put", config.apiUrl + "/formateurs/" + formateur.idFormateur + "/competences", formateur);
		}

		function createCompetence(competence){
			return Dao.crudData("post", config.apiUrl + "/competence", competence);
		}

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:timesheetService
	* @description
	* # timesheetService
	* Service of the app
	*/

	angular
	.module('timesheet')
	.factory('TimesheetService', Timesheet);
	// Inject your dependencies as .$inject = ['$http', 'someSevide'];
	// function Name ($http, someSevide) {...}

	Timesheet.$inject = ['$http'];

	function Timesheet ($http) {

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:userService
	* @description
	* # userService
	* Service of the app
	*/

	angular
	.module('user')
	.factory('UserService', User);
	// Inject your dependencies as .$inject = ['$http', 'someSevide'];
	// function Name ($http, someSevide) {...}

	User.$inject = ['$http'];

	function User ($http) {

	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.service:vacationrequestService
	* @description
	* # vacationrequestService
	* Service of the app
	*/

	angular
	.module('vacationrequest')
	.factory('VacationrequestService', Vacationrequest);
	// Inject your dependencies as .$inject = ['$http', 'someSevide'];
	// function Name ($http, someSevide) {...}

	Vacationrequest.$inject = ['$http', 'config', 'Dao'];

	function Vacationrequest ($http, config, Dao) {

		return {
			getTimeOff: getTimeOff,
			getJSONTimeOff: getJSONTimeOff,
			createTimeOff: createTimeOff,
			updateTimeOff: updateTimeOff
		};

		function getTimeOff (dateDebut, dateFin, formateurId, etat) {
			var url = config.apiUrl + "/conges/" + dateDebut + "/" + dateFin;
			if (formateurId === null) {
				if (etat === null) {
					return Dao.crudData('get', url);
				} else {
					return $http.get(url, {cache: true, params:{'etat': etat}})
						.then(Dao.success)
						.catch(Dao.error);
				}
			} else {
				if (etat === null) {
					return $http.get(url, {cache: true, params:{'formateurId': formateurId}})
						.then(Dao.success)
						.catch(Dao.error);
				} else {
					return $http.get(url, {cache: true, params:{'formateurId': formateurId, 'etat': etat}})
						.then(Dao.success)
						.catch(Dao.error);
				}
			}
		}
		function getJSONTimeOff (
			idConge,
			dateDebutConge,
			dateEndConge,
			motif,
			etatConge,
			dateDemandeConge,
			formateur
		) {
			return {
				idConge: idConge == null ? 0:idConge,
				dateDebutConge: dateDebutConge,
				dateFinConge: dateEndConge,
				motif: motif,
				etatConge: etatConge,
				dateDemandeConge: dateDemandeConge,
				formateur:
				{
					idFormateur: formateur.formateurId,
					username: formateur.username,
					mail:formateur.mail,
					prenom:formateur.prenom,
					nom:formateur.nom,
					externe: 0,
					competences:null,
					habilitation: null,
					experiences: null
				}
			};
		}
		function createTimeOff (conge) {
			return Dao.crudData(
				'post',
				config.apiUrl + "/conge",
				conge
			);
		}

		function updateTimeOff (conge) {
			return Dao.crudData(
				'put',
				config.apiUrl + "/conge/" + conge.idConge,
				conge
			);
		}
	}

})();

(function() {
    'use strict';

    angular
        .module('opale')
        .directive('stringToNumber', directive);

    /* @ngInject */
    function directive() {
        var directive = {
            require: 'ngModel',
            restrict: 'A',
            link: linkFunc,
        };

        return directive;

        function linkFunc(scope, el, attr, ngModel) {
          ngModel.$parsers.push(function(value) {
            return '' + value;
          });
          ngModel.$formatters.push(function(value) {
            return parseFloat(value);
          });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('opale')
        .directive('access', access);

    access.$inject = ['LoginService'];

    /* @ngInject */
    function access(LoginService) {
        var directive = {
            restrict: 'A',
            link: linkFunc,
        };

        return directive;

        function linkFunc(scope, el, attr) {
          var roles = attrs.access.split(',');
          if (roles.length > 0) {
              if (LoginService.isAuthorized(roles)) {
                  element.removeClass('hide');
              } else {
                  element.addClass('hide');
              }
          }
        }
    }

})();

(function() {
  'use strict';

  angular
  .module('opale')
  .directive('ngEnter', function () {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if(event.which === 13) {
          scope.$apply(function (){
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      });
    };
  })
  .directive('usernameExist', ['$http', 'config', '$q', function ($http, config, $q) {
    return {
      require: 'ngModel',
      link : function ($scope, element, attrs, ngModel) {
        ngModel.$asyncValidators.usernameExist = function(modelValue, viewValue) {
          var def = $q.defer();

          $http.get(config.apiUrl + '/security/user?username='+ modelValue).then(function (reponse) {
            if(angular.isObject(reponse.data)){
              def.resolve();
            } else {
              def.reject();
            }
          });

          return def.promise;
        };
      }
    };
  }]);
})();

(function() {
    'use strict';

    angular
    .module('opale')
    .directive('iconPanel', iconPanel);

    /* @ngInject */
    function iconPanel() {
        var directive = {
            restrict: 'EA',
            templateUrl: '/app/modules/shared/directives/iconpanel/iconpanel.tpl.html',
            scope: {
                type: '@',
                title: '@',
                message: '@'
            },
            link: linkFunc,
            controller: Controller,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    Controller.$inject = [];

    /* @ngInject */
    function Controller() {
        var vm = this;
        vm.show = false;
        switch (vm.type) {
            case 'info':
                vm.bg = 'blue';
                vm.icon = 'info';
            break;
            case 'warn':
                vm.bg = 'orange';
                vm.icon = 'warning';
            break;
            case 'error':
                vm.bg = 'red';
                vm.icon = 'error';
            break;
            case 'success':
                vm.bg = 'green';
                vm.icon = 'done';
            break;
            default:
        }
    }
})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:loadingindicatorDirective
	* @description
	* # loadingindicatorDirective
	* Directive of the app
	*/

	angular
		.module('opale')
		.directive('loadingIndicator', loadingIndicator);

		loadingIndicator.$inject = ['$http', '$rootScope'];

		function loadingIndicator ($http, $rootScope) {

			var directive = {
				restrict: 'E',
				link: link,
				transclude: true,
				scope: {load: "=?"},
				template: '<md-progress-linear md-mode="indeterminate" ng-show="load" class="loading-page md-warn md-hue-3"></md-progress-linear>'
			};

			return directive;

			function link(scope, element, attrs) {
				scope.load = true;
				scope.isLoading = false;

				scope.isLoading = isLoading;

				scope.$watch(scope.isLoading, toggleElement);

				function toggleElement(loading) {
					if (loading) {
						scope.load = true;
					} else {
						scope.load = false;
					}
				}

				function isLoading() {
					return $http.pendingRequests.length > 0;
				}
			}
		}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.controller:sidenavplanningDirective
	* @description
	* # sidenavplanningDirective
	* Directive of the app
	*/

	angular
		.module('opale')
		.directive('sideNavPlanning', sideNavPlanning);

		function sideNavPlanning () {

			var directive = {
				link: link,
				restrict: 'EA',
				scope:{
				},
				controller: 'SideNavPlanningCtrl',
				controllerAs: 'vm',
				bindToController:true,
				templateUrl:'app/modules/shared/directives/sidenavplanning/sidenavplanning.html',
			};

			return directive;

			function link(scope, element, attrs) {

			}
		}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name app.factory:utils Factory
	* @description
	* # Utils Factory
	* Factory of the app
	*/

	angular
	.module('opale')
	.factory('Utils', Utils);


	Utils.$inject = ['atomicNotifyService'];

	function Utils(atomicNotifyService) {

		return {
			/**
			* Affiche un toast parametre
			* @param  {[String]} content [Le texte à afficher]
			*/
			viewToast : function viewToast(type, content){
				switch (type) {
					case "info":
						atomicNotifyService.custom("info", content, "info");
					break;
					case "warn":
						atomicNotifyService.custom("warning", content, "warning");
					break;
					case "error":
						atomicNotifyService.custom("error", content, "error");
					break;
					case "success":
						atomicNotifyService.custom("success", content, "done");
					break;
					case "custom":
						atomicNotifyService.custom("info", content);
					break;
					default:

				}
			},
			/**
			* Déplace un element dans une liste
			* @param  {[Array]} array          [Le tableau à modifier]
			* @param  {[type]} value          [L'element de la liste à deplacer]
			* @param  {[type]} positionChange [Le deplacement : +1, -1, +2, -3...]
			* @return {[Array]}                [Le tableau modifie]
			*/
			moveElementInArray: function moveElementInArray (array, value, positionChange) {
				var oldIndex = array.indexOf(value);
				if (oldIndex > -1){
					var newIndex = (oldIndex + positionChange);

					if (newIndex < 0){
						newIndex = 0;
					} else if (newIndex >= array.length){
						newIndex = array.length;
					}

					var arrayClone = array.slice();
					arrayClone.splice(oldIndex,1);
					arrayClone.splice(newIndex,0,value);

					return arrayClone;
				}
				return array;
			},

			isUndefinedOrNull : function isUndefinedOrNull (val) {
				return angular.isUndefined(val) || val === null;
			}
		};

	}



})();

(function() {
  'use strict';

  /**
  * @ngdoc function
  * @name app.factory:utils Factory
  * @description
  * # Utils Factory
  * Factory of the app
  */

  angular
  .module('opale')
  .factory('Dao', Dao);


  Dao.$inject = ['$http', '$cacheFactory'];

  function Dao($http, $cacheFactory) {
    return {
      crudData : crudData
    };

    /**
    * Permet de réaliser les operation du CRUD auprès du WS
    * @param  {[string]} method   [valeur possible : "get", "put", "post", "delete"]
    * @param  {[string]} url      [l'url du ws]
    * @param  {[data]} data     [les donnees à modifier, ajouter ou supprimer]
    * @param  {[string]} cacheKey [la clé du cache pour les get]
    * @return {[data]}          [les données du ws]
    */
    function crudData (method, url, data, headers) {
      switch (method) {
        case "get":
        return getData(url, headers);
        case "put":
        return setData(url, method, data);
        case "post":
        return setData(url, method, data);
        case "delete":
        return setData(url, method, data);
      }
    }

    function getData(url, headers){
      return $http.get(url, {cache: true}, {headers : headers})
      .then(doResponse)
      .catch(error);
    }

    function setData(url, method, data) {
      return $http({
        url: url,
        method: method,
        data: data
      })
      .then(doResponse)
      .catch(error);
    }

    function doResponse(response) {
      var status = response.status;
      var data;
      switch(status) {
        case 200:
        data = response.data;
        break;
        case 401:
        data = 'Action non authorisée !';
        break;
        case 403:
        data = 'Accès interdit !';
        break;
        case 404:
        data = 'La ressource demandée n\'as pas été trouvée.';
        break;
        case 500:
        data = 'Une erreur inconnue est survenue sur le serveur !';
        break;
        case 0:
        data = 'Une erreur inconnue est survenue !';
        break;
        case -1:
        data = 'Impossible de se connecter au serveur ! Vérifiez votre connexion.';
        break;
        default:
        data = 'Une erreur inconnue est survenue !';
        break;
      }
      return {status: status, data: data};
    }

    function error(error) {
      var status = error.status;
      var errorMsg;
      switch(status) {
        case 401:
        errorMsg = 'Action non authorisée !';
        break;
        case 403:
        errorMsg = 'Accès interdit !';
        break;
        case 404:
        errorMsg = 'La ressource demandée n\'as pas été trouvée.';
        break;
        case 500:
        errorMsg = 'Une erreur inconnue est survenue sur le serveur !';
        break;
        case 0:
        errorMsg = 'Une erreur inconnue est survenue !';
        break;
        case -1:
        errorMsg = 'Impossible de se connecter au serveur ! Vérifiez votre connexion.';
        break;
        default:
        errorMsg = 'Une erreur inconnue est survenue !';
        break;
      }
      return {err: status, data: errorMsg};
    }
  }

})();
