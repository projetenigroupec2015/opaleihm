(function () {
	'use strict';

	/**
	* @ngdoc configuration file
	* @name app.config:config
	* @description
	* # Config and run block
	* Configutation of the app
	*/


	angular
	.module('opale')
	.config(configure)
	.factory('XSRFInterceptor', XSRFInterceptorFactory)
	.run(runBlock);

	configure.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$mdThemingProvider', '$breadcrumbProvider', '$mdToastProvider', 'atomicNotifyProvider'];

	function configure($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $mdThemingProvider, $breadcrumbProvider, $mdToastProvider, atomicNotifyProvider) {

		$locationProvider.hashPrefix('!');

		// This is required for Browser Sync to work poperly
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$httpProvider.defaults.withCredentials = true;
		$httpProvider.interceptors.push('XSRFInterceptor');


		$urlRouterProvider.otherwise('/dashboard');

		$mdThemingProvider.theme('default')
		.primaryPalette('blue', {
			'default': '400',
			'hue-1': '100',
			'hue-2': '600',
			'hue-3': 'A100'
		})
		.accentPalette('light-blue', {
			'default': '900',
			'hue-1': '200',
			'hue-2': '700',
			'hue-3': 'A400'
		})
		.warnPalette('lime', {
			'default': '900',
			'hue-1': '200',
			'hue-2': '700',
			'hue-3': 'A400'
		});

		/**
		* Parametrage du fil d'ariane
		*/
		$breadcrumbProvider.setOptions({
			templateUrl: 'app/modules/shared/templates/breadcrumb.tpl.html'
		});

		/**
		* Paramétrage du système de notifications par toast
		*/
		atomicNotifyProvider.setDefaultDelay(5000);
		atomicNotifyProvider.useIconOnNotification(true);

		moment.locale('fr');

	}

	runBlock.$inject = ['config', 'Utils', '$rootScope', 'amMoment', 'LoginService', 'Session', 'USER_ROLES', '$q', '$timeout', '$state', '$cookies', '$location'];

	function runBlock(config, Utils, $rootScope, amMoment, LoginService, Session, USER_ROLES, $q, $timeout, $state, $cookies, $location) {
		$rootScope.$on('$stateChangeStart',	function(event, toState, toParams, fromState, fromParams, options){
			if($location.originalPath === "/login" && $rootScope.authenticated) {
				event.preventDefault();
			} else if(toState.access && toState.access.loginRequired && !$rootScope.authenticated){
				event.preventDefault();
				$rootScope.$broadcast("event:auth-loginRequired", {});
			} else if(toState.access && !LoginService.isAuthorized(toState.access.authorizedRoles)){
				event.preventDefault();
				$rootScope.$broadcast("event:auth-forbidden", {});
			}
		});


		$rootScope.$on('event:auth-loginConfirmed', function (event, data) {
			$rootScope.loadingAccount = false;
			var nextLocation = redirectOnLogin(data.habilitation.libelle);
			var delay = ($state.$current.name === "loading" ? 1000 : 0);
			$timeout(function () {
				Session.create(data);
				$rootScope.account = Session;
				$rootScope.authenticated = true;
				if(nextLocation === 'home.planning'){
					var dateStart = moment().startOf('month').startOf('week').add(1,'days').format(config.format);
					var dateEnd = moment().add(1,'months').endOf('month').endOf('week').format(config.format);
					$state.go(nextLocation, {dateStart:dateStart,dateEnd:dateEnd}, { location: "replace"});
				} else {
					$state.go(nextLocation, { location: "replace"});
				}
			}, delay);
		});

		function redirectOnLogin (habilitation) {
			if(habilitation === 'admin'){
				return ($rootScope.requestedUrl ? $rootScope.requestedUrl : "home.dashboard");
			} else {
				return ($rootScope.requestedUrl ? $rootScope.requestedUrl : "home.planning");
			}
		}

		// Call when the 401 response is returned by the server
		$rootScope.$on('event:auth-loginRequired', function (event, data) {
				console.log("login");
				Session.invalidate();
				$rootScope.authenticated = false;
				$rootScope.loadingAccount = false;
				$state.go('login', { location: "replace"});
		});

		// Call when the 403 response is returned by the server
		$rootScope.$on('auth-forbidden', function (rejection) {
			$rootScope.$evalAsync(function () {
				$state.go('error.403', { location: "replace", notify: false });
			});
		});

		// Call when the user logs out
		$rootScope.$on('event:auth-loginCancelled', function () {
			$location.path("/login").replace();
		});

		$rootScope.$on('api-connect-error', function () {
			Utils.viewToast("error", "Impossible de se connecter à l'API!");
		});

		// Get already authenticated user account
		LoginService.getAccount();
		amMoment.changeLocale('fr');
	}

	function XSRFInterceptorFactory($log, $cookies, $cacheFactory, $rootScope, $q){
		var xsrfToken = $cookies.get('XSRF-TOKEN');
		var auth = sessionStorage.getItem("auth");

		var XSRFInterceptor = {

			request: function(config) {

				$rootScope.isLoading = true;

				if (xsrfToken) {
					config.headers['X-XSRF-TOKEN'] = xsrfToken;
				}
				if(auth){
					config.headers['Authorization'] = auth;
				}

				if(config.method === 'PUT' || config.method === 'POST' || config.method === 'DELETE'){
					var $httpDefaultCache = $cacheFactory.get('$http');
					$httpDefaultCache.removeAll();
				}

				return config;
			},

			response: function(response) {
				$rootScope.isLoading = false;

				var newToken = response.headers('X-XSRF-TOKEN');

				if (newToken) {
					xsrfToken = newToken;
				}

				return response;
			},

			responseError: function(response) {
				console.log(response);
				if(response.status === -1){
					$rootScope.$broadcast('api-connect-error', {data : response});
					return response || $q.when(response);
				} else if (response.status === 401) {
					$rootScope.$broadcast('event:auth-loginRequired', {data : response});
				} else if(response.status === 403){
					$rootScope.$broadcast('event:auth-loginRequired', {data : response});
				} else {
					return response;
				}
			}
		};

		return XSRFInterceptor;
	}

})();
