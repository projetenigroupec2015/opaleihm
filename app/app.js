(function() {
	'use strict';

	/**
	 * @ngdoc index
	 * @name app
	 * @description
	 * # app
	 *
	 * Main modules of the application.
	 */

	angular.module('opale', [
		'ngResource',
		'ngAria',
		'ngMaterial',
		'ngMdIcons',
		'ngCookies',
		'ngAnimate',
		'ngMessages',
		'ngSanitize',
		'ui.router',
		'ncy-angular-breadcrumb',
		'angularMoment',
		'atomic-notify',
		'home',
		'config',
		'timesheet',
		'vacationrequest',
		'profile',
		'settings',
		'planning',
		'user',
		'login',
		'loading',
		'http-auth-interceptor',
		'md.data.table'
	]);

})();
