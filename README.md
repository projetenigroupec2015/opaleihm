# OPALE IHM #
Généré à partir de ANGM: http://newaeonweb.com.br/generator-angm/

## Démarrage
Fonctionne parfaitement avec NodeJS en version 4.4.5 et la dernière version de NPM. => /!\ Erreur possible avec la version 6.2.1 de NodeJS (latest)

> NOTE: Les actions dans le Terminal sont possibles à partir de Webstorm. Pour cela, il suffit d'ouvrir l'onglet Terminal situé dans le bas de l'écran : https://www.jetbrains.com/help/webstorm/11.0/working-with-embedded-local-terminal.html

### Vérifier la version de NodeJS et de NPM

Ouvrir le terminal cmd, et taper:

```bash
$ node --version && npm --version
```
La version de NodeJS doit être 4.4.5 (LTS) et NPM >=2.15.5. Si ce n'est pas le cas, reinstaller NodeJS: https://nodejs.org/en/download/
```

### Installer bower grunt et karma

Ouvrir le terminal, et taper:

```bash
$ npm install --global bower grunt-cli karma grunt-karma
```

Le paramètre '--global' permet de rendre les installations de ces composants NPM disponibles à l'ensemble des projets pouvant les nécessiter. Ainsi, si vous souhaitez n'installer ces éléments que pour le projet en cours, il suffit d'enlever le paramètre.

### Initialiser NPM et bower

Ouvrir le terminal, et taper:

```bash
$ npm install -g bower-npm-resolver
$ npm install && bower install
```


## Lancer l'application
Dans Webstorm, ouvrir l'onglet Grunt et exécuter la tâche :
```bash
dev 'injector:dev', 'concurrent'
```
L'application se lance dans le navigateur par défaut. Si ce n'est pas le cas, elle est disponible à l'adresse : `http://localhost:4000`.
Le livereload est automatiquement lancé en même temps que l'application sur le port 32729. Vous pouvez faire des modifications et les voir apparaître à l'écran instantannément. 

> NOTE: Il se peut que l'application manque parfois de réactivité après un certain temps de développement, ce n'est pas la qualité du développement mais livereload qui est en cause. Il suffit de redémarrer le serveur en allant dans l'onglet 'Run' et en cliquant sur "Rerun 'dev'" ou en faisant Ctrl+F11.