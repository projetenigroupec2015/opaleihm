// Grunt tasks

module.exports = function (grunt) {
	"use strict";

	// Project configuration.
	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
		banner: '/*!\n' +
		'* <%= pkg.name %> - v<%= pkg.version %> - MIT LICENSE <%= grunt.template.today("yyyy-mm-dd") %>. \n' +
		'* @author <%= pkg.author %>\n' +
		'*/\n',

		clean: {
			dist: ['src']
		},

		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			gruntfile: {
				src: 'Gruntfile.js'
			},
			app: {
				src: ['app/modules/**/*.js']
			}
		},

		exec: {
			bowerInstaller: 'bower-installer'
		},

		concat: {
			options: {
				banner: '<%= banner %>',
				stripBanners: false
			},

			base: {
				src: [
					// Angular Project Dependencies,
					'app/app.js',
					'app/app.config.js',
					'app/modules/**/*Module.js',
					'app/modules/**/*Route.js',
					'app/modules/**/*Ctrl.js',
					'app/modules/**/*Service.js',
					'app/modules/**/*Directive.js',
					'app/modules/**/*Utils.js'
				],
				dest: 'app/assets/js/<%= pkg.name %>-appbundle.js'
			}
		},

		cssmin: {
			minify: {
				expand: true,
				cwd: 'styles',
				src: ['*.css', '!*.min.css'],
				dest: 'styles',
				ext: '.min.css'
			}
		},

		uglify: {
			options: {
				banner: '<%= banner %>',
				report: 'min'
			},
			base: {
				src: ['<%= concat.base.dest %>'],
				dest: 'app/assets/js/<%= pkg.name %>-angscript.min.js'
			},
			basePlugin: {
				src: [ 'src/plugins/**/*.js' ],
				dest: 'app/assets/js/plugins/',
				expand: true,
				flatten: true,
				ext: '.min.js'
			}
		},

		connect: {
			server: {
				options: {
					keepalive: true,
					port: 4000,
					base: '.',
					hostname: 'localhost',
					debug: true,
					livereload: true,
					open: true
				}
			}
		},

		less: {
			development: {
				options:{
					paths:["app/**/assets/**/"]
				},
				files: {"app/assets/css/style.css": "app/assets/less/style.less"}
			},
			production: {
				options: {
					paths: ["app/assets/**/"],
					cleancss: true
				},
				files: {"app/assets/css/style.css": "app/assets/less/style.less"}
			}
		},

		concurrent: {
			tasks: ['connect', 'less', 'watch'],
			options: {
				logConcurrentOutput: true
			}
		},

		watch: {
			app: {
				files: '<%= jshint.app.src %>',
				tasks: ['jshint:app'],
				options: {
					livereload: true
				}
			},
			html: {
				files: ['index.html','**/*.html','**/*.css', '**/*.less'],
				options: {
					livereload: true
				}
			},
			less:{
				files: ['app/**/assets/less/*.less'],
				tasks: ['less']
			}
		},
		injector: {
			options: {},
			dev: {
				files: {
					'index.html': [
						'bower.json',
						'app/app.js',
						'app/app.config.js',
						'app/**/*Module.js',
						'app/**/*Route.js',
						'app/**/*Ctrl.js',
						'app/**/*Service.js',
						'app/**/*Directive.js',
						'app/**/*Factory.js',
						'app/modules/**/*Utils.js',
						'app/assets/css/**/*.css',
					]
				}
			},
			production: {
				files: {
					'index.html': [
						'app/assets/css/**/*.css',
						'app/assets/js/*.js'
					]

				}
			}
		},
		ngtemplates: {
			app: {
				src: 'app/modules/**/*.html',
				dest: 'app/assets/js/templates.js',
				options: {
					module: '<%= pkg.name %>',
					root: 'app/',
					standAlone: false
				}
			}
		},
		compress: {
			main: {
				options: {
					archive: 'front_opale.zip'
				},
				files: [
					{src: ['app/assets/css/**/*'], dest: '', filter: 'isFile'}, // includes files in path
					{src: ['app/assets/js/*'], dest: '', filter: 'isFile'},
					{src: ['app/assets/fonts/**/*'], dest: '', filter: 'isFile'},
					{src: ['app/assets/images/*'], dest: '', filter: 'isFile'},
					{src: ['src/bower_components/**/*'], dest: '', filter: 'isFile'},
					{src: ['*.html'], dest: '', filter: 'isFile'},
				]
			}
		}
	});

	require('time-grunt')(grunt);
	require('load-grunt-tasks')(grunt);

	// Making grunt default to force in order not to break the project if something fail.
	grunt.option('force', true);
	// Register grunt tasks
	grunt.registerTask("build", [
		"jshint",
		"exec",
		"concat",
		"ngtemplates",
		"injector:production",
		"concurrent",
		"clean"
	]);

	// Development task(s).
	grunt.registerTask('dev', ['injector:dev','concurrent']);

};
